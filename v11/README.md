# IDMarketplace

id marketplace


# Technical Ref:
# Requirements:
- Odoo11.0
- pip3 install json2html

## Getting Endpoint:
Ex:
```python
""" self = sale.order """
master_id = self.env['marketplace.shop']._search_marketplace(shop_key=self.tokopedia_shop_id)
endpoint = master_id.get_endpoints('order.ack')
url = endpoint.with_context(marketplace_data=dict(order_id=order_id))._fetch_url()
```


## Run Endpoint:
Ex:
```python
""" self: env of sale.order """
# master_id = self.env['marketplace.shop']._search_marketplace(shop_key=self.tokopedia_shop_id)
master_id = self.env['marketplace.master'].browse(anyid**)
endpoint = master_id.get_endpoints('order.ack')
# url = endpoint.with_context(marketplace_data=dict(order_id=order_id))._fetch_url()
response = endpoint.with_context(marketplace_data=dict(order_id=order_id),post_data={data**},extra_headers={**})._run_outgoing() # if post request
```


## CREATING ORDER FROM RESPONSE (TOKOPEDIA)
ex:
```python
order, errors = self.env['sale.order'].create(dict_of_response)
```