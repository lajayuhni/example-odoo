# -*- coding: utf-8 -*-
import json
import logging
import werkzeug.utils

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class MarketplacePrint(http.Controller):
    @http.route('/marketplace/order/print/<int:order_id>/<int:print_count>',  type='http', auth='user')
    """
    MarketplacePrint To load print order from tokopedia endpoint
    """
    def index(self, order_id, print_count, **kw):

        Order = request.env['sale.order']

        order = Order.browse(order_id)

        marketplace_data = dict(order_id=order.tokopedia_order_id, fs_id=order.marketplace_id.fs_id, print_count=print_count)
        
        r = order.marketplace_id.with_context(marketplace_data=marketplace_data).get_endpoints('order.print.label')._run_outgoing()
        r.raise_for_status()
        if r.ok:
            # if http ok but response error
            try:
                res = r.json()
                
                if r.get('header').get('error_code'):
                    raise UserError(r.get('header').get('messages'))
            except Exception as e:
                
                pass

            return request.make_response(r.text)