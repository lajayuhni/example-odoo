from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class MarketplaceToken(models.Model):
    _name = 'marketplace.token'
    _descriptipon = 'Marketplace Token'

    # no need to identify by marketplace master
    # just use key
    # causing by possible if 1 key to multiple accounts
    # marketplace_id = fields.Many2one('marketplace.master', string="Marketplace Master", required=True)

    key = fields.Char(string="Key API")
    token = fields.Char(string="token")
    expire_on = fields.Datetime()
    # expiry_raw = fields.Char("Raw Expire Data")
    raw = fields.Text()

    @api.model
    def _get_active_token(self, key):
        return self.sudo().search([('key','=',key),('expire_on','>',fields.Datetime.now())])

