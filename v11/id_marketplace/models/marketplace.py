from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
from odoo.api import Environment, SUPERUSER_ID
from datetime import datetime,timedelta
from odoo.osv import expression
import requests
import logging
_logger = logging.getLogger(__name__)


class MarketplaceProvider(models.Model):
    _name = 'marketplace.provider'
    _description = 'Marketplace Provider'

    code = fields.Char("Code", required=True)
    name = fields.Char("Name", required=True)

    default_endpoint_ids = fields.Binary()
    _sql_constraints = [
        ('code_unique', 'unique (code)', 'Code must be unique'),
    ]



class MarketplaceMaster(models.Model):
    _name = 'marketplace.master'
    _description = 'Marketplace'

    name = fields.Char(string="Name", required=True)
    code = fields.Char(string="Code", required=True)

    active = fields.Boolean(default=True)
    
    auto_confirm = fields.Boolean(default=True)
    reject_if_confirm_fail = fields.Boolean(default=False, help="When Order notification comes, then if auto_confirm and confirming failed (maybe caused by stock not available) will send reject signal")
    base_endpoint_url = fields.Char(required=True)
    base_webhook_url = fields.Char("Base Webhook URL", required=False, help="Set if webhook service outside base url of odoo")
    endpoint_ids = fields.One2many('marketplace.endpoint', 'marketplace_id', string="Enpoints", required=True)
    incoming_webhook_ids = fields.One2many('marketplace.endpoint', 'marketplace_id', string="Webhooks", compute="_compute_endpoint_ids")
    outgoing_endpoint_ids = fields.One2many('marketplace.endpoint', 'marketplace_id', string="Webhooks", compute="_compute_endpoint_ids")
    
    client_id = fields.Char(ghelp="Secret ID", roups="base.group_system,id_marketplace.manager")
    client_secret = fields.Char(help="Secret key", groups="base.group_system,id_marketplace.group_marketplace_manager")
    fs_id = fields.Char(groups="base.group_system,id_marketplace.manager")
    
    company_id = fields.Many2one('res.company')
    shop_ids = fields.One2many('marketplace.shop', 'marketplace_id', string="Shops")

    latest_sync_time = fields.Datetime()
    latest_sync_user_id = fields.Many2one('res.users', string="Latest Sync User")
    response = fields.Text(string="Last Response Sync")

    provider_id = fields.Many2one('marketplace.provider', required=True, help="Marketplace")



    tokopedia_product_count = fields.Integer(compute="_compute_related_product")
    # tokopedia_product_ids = fields.One2many('product.product', compute="_compute_related_product")

    product_key_field_id = fields.Many2one('ir.model.fields',
        domain=[('model','=','product.product')], 
        string="Product Key Field", 
        help="Field in system that equal with Unique code in marketplace", 
        required=True,
        default=lambda self:self.env.ref("product.field_product_product_default_code").id)


    def _from_date(self, date=None):
        if not date:
            now =  datetime.combine(fields.date.today(), datetime.min.time())
            time = (now+ timedelta(days=-2))
        else:
            time = datetime.combine(date, datetime.min.time())
        return int(time.timestamp())
    
    def _to_date(self, date=None):
        if not date:
            now =  datetime.combine(fields.date.today(), datetime.max.time())
            time = now
        else:
            time = datetime.combine(date, datetime.max.time())
        return int(time.timestamp())

    def product_key_field_name(self):
        self.ensure_one()
        return self.product_key_field_id.name
    
    def sync_down_product(self):
        """
        sync_down_product Using tokopedia v2

        :return: [description]
        :rtype: [type]
        """
        self.ensure_one()
        view = self.env.ref('id_marketplace.marketplace_product_sync_wizard_header_form_view')
        context = dict(self.env.context or {})
        WizHead = self.env['marketplace.product.sync.wizard.header'].create({})

        res = {
            'name': "%s - %s" % (_('Sync Product'), self.name),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'form',
            'res_model': 'marketplace.product.sync.wizard.header',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new',
            'res_id':WizHead.id,
            # 'domain':[('id','in',records.ids)]
        }
        return res

    def _compute_related_product(self):
        for rec in self:
            res = {}
            for shop in rec.shop_ids:
                res.update({
                    'tokopedia_product_count':res.get('tokopedia_product_count', 0) + shop.tokopedia_product_count,
                    # 'tokopedia_product_ids':res.get('tokopedia_product_ids', self.env['product.product']) + shop.tokopedia_product_ids,
                    })
            rec.update(res)



    _sql_constraints = [
        ('code_unique', 'unique (code)', 'Code must be unique'),
    ]
    TOKOPEDIA = lambda self:self.env.ref('id_marketplace.provider_tokopedia')
    SHOPEE = lambda self:self.env.ref('id_marketplace.provider_tokopedia')

    def register_webhook(self):
        return getattr(self, '_register_webhook_%s' % self.provider_id.name.lower())()

    def _register_webhook_tokopedia(self):
        self.ensure_one()
        res = False
        # sending request
        # tokopedia register webhook key == 'webhook.register.register'
        endpoint = self.get_endpoints('webhook.register.register')
        # self.send_request(endpoint)

        # post data from marketplace.master incoming_webhook_ids
        post_data = {}
        for in_endpoint in self.incoming_webhook_ids:
            post_data.update({in_endpoint.key:in_endpoint._fetch_url()})
        

        _logger.info('Run endpoint %s' % (endpoint.key,))
        req = endpoint.with_context(post_data=post_data)._run_outgoing()
        if req.ok:
            res = req.json()
        else:
            # if raise status
            raise_errors = req.raise_for_status()
            if raise_errors:
                raise ValueError(raise_errors)
            else:
                res = res.json()
        return res


    @api.model
    def create(self, val):
        res = super(MarketplaceMaster, self).create(val)
        if not val.get('endpoint_ids'):
            res.set_default_endpoint_ids()

        return res

    def write(self, vals):
        outgoings = vals.get('outgoing_endpoint_ids')
        if outgoings:
            vals.update({'endpoint_ids':outgoings})
        res = super(MarketplaceMaster, self).write(vals)
        
        return res

    @api.onchange('provider_id')
    def _onchange_provider_id(self):
        # if self.provider_id:
        self.set_default_endpoint_ids()

    def set_default_endpoint_ids(self):
        self.ensure_one()

        if self.provider_id.default_endpoint_ids:
            endpoints = eval(self.provider_id.default_endpoint_ids)
            if type(endpoints)==list:
                remap_endpoints = []
                for endpoint in endpoints:
                    if endpoint.get('key') not in self.endpoint_ids.mapped('key'):
                        remap_endpoints.append((0,0,endpoint))
                
                self.endpoint_ids = remap_endpoints
            else:
                raise ValueError("Failed to init default endpoints!Please check default value!")

    def action_open_outgoing_endpoint_form(self):
        self.ensure_one()
        form = self.env.ref('id_marketplace.marketplace_endpoint_outgoing_form_view')
        context = dict(self.env.context or {})
        context.update({'default_type':'outgoing','default_marketplace_id':self.id})
        res = {
            'name': "%s - %s" % (_('Register Outgoing Endpoint'), self.name),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'form',
            'res_model': 'marketplace.endpoint',
            'view_id': form.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new'
        }
        return res

    def action_open_incoming_webhook_form(self):
        self.ensure_one()
        form = self.env.ref('id_marketplace.marketplace_endpoint_incoming_form_view')
        context = dict(self.env.context or {})
        context.update({'default_type':'incoming','default_marketplace_id':self.id})
        res = {
            'name': "%s - %s" % (_('Register Incoming Webhook'), self.name),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'form',
            'res_model': 'marketplace.endpoint',
            'view_id': form.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new'
        }
        return res


    @api.depends('endpoint_ids.type')
    def _compute_endpoint_ids(self):
        self = self.sudo()
        for rec in self:
            rec.incoming_webhook_ids = rec.endpoint_ids.filtered(lambda r:r.type=='incoming')
            rec.outgoing_endpoint_ids = rec.endpoint_ids.filtered(lambda r:r.type=='outgoing')


    @api.returns('marketplace.endpoint')
    def get_endpoints(self, key):
        self.ensure_one()
        self = self.sudo()
        res = False
        return self.endpoint_ids.filtered(lambda r:r.key==key)

    def _tokopedia_encode_bearer(self):
        import base64
        self = self.sudo()
        data = '%s:%s' % (self.client_id, self.client_secret,)

        return base64.b64encode(bytes(data, 'utf-8')).decode('utf-8')


    def encode_bearer(self):
        if self.provider_id == TOKOPEDIA:
            return self._tokopedia_encode_bearer()

    def send_request(self, endpoint, headers={}, data={}):
        self.ensure_one()
        self = self.sudo()
        token = self.get_token()
        return getattr(requests, method)(url=endpoint._fetch_url(), data=data, headers=headers)

    def _register_new_token(self, data):
        """
        _register_new_token Register rereshed token to model
        """
        return self.env['marketplace.token'].sudo().create(data)


    
    def _get_token_tokopedia(self):
        """
        _get_token_tokopedia active token, if no active token then will ask new one. https://developer.tokopedia.com/openapi/guide/#/authentication?id=authentication
        """
        self = self.sudo()
        endpoint = self.get_endpoints('auth')
        headers = {
            'Authorization': 'Basic %s' % (self._tokopedia_encode_bearer()),
            'Content-Length':'0',
            'user-Agent':'PostmanRuntime/7.17.1'
        }
        

        url = endpoint._fetch_url()
        r = requests.post(url=url,headers=headers)
        r.raise_for_status() # get exception if not success

        if r.ok:
            # if success
            responses = r.json()
            if responses.get('error'):
                raise ValidationError(responses.get('error_description'))
            active_token = responses.get('access_token')
            # FIXME IF ACCESS DENIED

            lifetime = int(responses.get('expires_in'))/60 #in minutes
            self._register_new_token({
                'key':self.fs_id,
                'token':responses.get('access_token'),
                'expire_on':datetime.now()+timedelta(minutes=lifetime),
            })
            return active_token

        else:
            raise ValidationError("Get tokopedia token failed. Issue has been reported")

    def _get_token_shopee(self):
        pass

    def _get_active_token(self):
        """
        _get_active_token Get Existing Active token wich has noted on db.
        """
        return self.env['marketplace.token'].sudo()._get_active_token(key=self.fs_id)


    
    def get_token(self):
        self.ensure_one()
        self = self.sudo()
        active_token = self._get_active_token() # FIXME: Should check on new models, if saved active token exist take from it
        if active_token:
            return active_token.token
        return getattr(self, "_get_token_%s" % (self.provider_id.name.lower(),))()

    def _tokopedia_shop_map(self,data):
        return {
            'key_id':data.get('shop_id'),
            'url':data.get('shop_url'),
            'data':str(data),
            'name':data.get('shop_name'),
            'marketplace_status_code':data.get('status'),
        }

    def btn_sync_shop(self):
        """
        btn_sync_shop Button Sync Shop
        """

        self.ensure_one()
        # master_id = self.env['marketplace.shop']._search_marketplace(shop_key=self.tokopedia_shop_id)
        endpoint = self.get_endpoints('shop.info')
        url = endpoint._fetch_url()
        response = endpoint._run_outgoing() # if post request
        response.raise_for_status()
        if response.ok:
            # print(response.json)
            self.sync_shop_parse_response_from_tokopedia(response.json())

        

    def sync_tokopedia_shop(self,data):
        self.ensure_one()
        errors = []
        res = False
        
        
        # if called from @api.model
        # search by sku
        key = data.get('shop_id')
        shop = self.shop_ids.filtered(lambda r:r.key_id==key)
        updated = {'response':str(data)}
        

        if len(shop) == 1:
            data = self._tokopedia_shop_map(data)
            shop.write(data)
            res = True
        elif len(shop)>1:
            errors += ["Found %s Shop on sherlogic with shop_id=%s, Name=%s" % (len(p), key, data.get('shop_name'),)]
            res = False
        elif len(shop)==0:
            # ceate new
            self.write({'shop_ids':[(0,0,self._tokopedia_shop_map(data))]})
            res = True
        return errors,res
    

    def sync_shop_parse_response_from_tokopedia(self, response):
        self.ensure_one()
        datas = response.get('data')
        errors = response.get('error_message',[])
        
        if datas and len(datas):
            self.write({'response':str(datas)})
            for data in datas:
                err,res = self.sync_tokopedia_shop(data)
                errors += err
        else:
            if not len(errors):
                raise Warning("No Data")

        if len(errors):
            raise UserError(",\n".join(errors))
        return True

    



class MarketplaceShop(models.Model):
    _name = 'marketplace.shop'
    _description = 'Marketplace Shop'

    
    marketplace_id = fields.Many2one('marketplace.master', required=True)
    
    key_id = fields.Integer(string="Key ID", help="Primary key on Marketplace", required=True)
    name = fields.Char(string="Shop Name", help="Name on Marketplace", required=True)
    url = fields.Char(string="Shop Url", help="Marketplace URL", required=True)
    # fetch_url = fields.Char(compute="_compute_fetch_url")
    
    marketplace_status_code = fields.Char(help="Marketplace Status code")

    data = fields.Text(help="Latest response from marketplace when do a Sync")

    warehouse_id = fields.Many2one('stock.warehouse', "Default Warehouse", required=False, help="Warehouse")

    error_info = fields.Html(compute="_compute_error_info")

    tokopedia_product_count = fields.Integer(compute="_compute_related_product")
    tokopedia_product_ids = fields.One2many('product.product', compute="_compute_related_product")
    active = fields.Boolean(default=True)

    skip_tokopedia_fullfill_by = fields.Boolean(default=True, help="Skipp any order request if fullbillby==1")

    provider_id = fields.Many2one(related="marketplace_id.provider_id", readonly=True)

    partner_id = fields.Many2one('res.partner', string="Customer", required=True, help="Customer that will use by the shop transaction")

    _rec_name = 'name'

    def action_view_product_wizard(self):
        self.ensure_one()
        tree = self.env.ref('id_marketplace.marketplace_product_sync_wizard_tree_view')
        context = dict(self.env.context or {})
        endpoint = self.marketplace_id.get_endpoints('v1.product.all')
        # context.update({}) #uncomment if need append context
        res = {
            'name': "%s - %s" % (_('Sync Product'), self.name),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'tree',
            'res_model': 'marketplace.product.sync.wizard',
            'view_id': tree.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new',
            'domain':[('id','in',records.ids)]
        }
        return res


    def _compute_related_product(self):
        for rec in self:
            qq = """SELECT id as counter FROM product_product WHERE tokopedia_product_id > 0 AND tokopedia_product_id is NOT NULL"""
            self.env.cr.execute(qq)
            res = self.env.cr.fetchall()
            pids = [r[0] for r in res]
            rec.update({
                'tokopedia_product_count':len(res),
                'tokopedia_product_ids':self.env['product.product'].browse(pids),
            })




    def _compute_error_info(self):
        def badge(msg,level='danger'):
            return '<span class="badge badge-%s">%s</span>' % (level,msg)
        for rec in self:
            tags = []
            complete = False
            if not rec.warehouse_id.id:
                tags.append(badge("Warheouse Empty!"))

            rec.error_info = "".join(tags)

    @api.model
    def _search_shop_key(self, shop_key):
        self = self.sudo()
        return self.search([('key_id','=',shop_key)])

    @api.model
    def _search_marketplace(self, shop_key):
        return self._search_shop_key(shop_key).marketplace_id

    