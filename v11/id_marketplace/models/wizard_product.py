from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
from odoo.osv import expression
import requests

import logging
_logger = logging.getLogger(__name__)

class MarketplaceProductSyncWizardHeader(models.TransientModel):
    _name = 'marketplace.product.sync.wizard.header'
    _description = 'Marketplace Product Sync Wizard Header'

    line_ids = fields.One2many('marketplace.product.sync.wizard', 'header_id')
    page = fields.Integer(default=1)
    size = fields.Integer(default=20)
    shop_id = fields.Many2one('marketplace.shop')

    def _fetch_down_product(self):
        WizHead = self
        Wiz = self.env['marketplace.product.sync.wizard']
        endpoint = self.shop_id.marketplace_id.get_endpoints('v1.product.all')
        # url = endpoint._fetch_url()
        def trying_get_data(endpoint):
            try:
                return endpoint.with_context(marketplace_data=dict(per_page=self.size,start=((self.page*self.size)-self.size+1),shop_id=self.shop_id.key_id))._run_outgoing() # if post request
            except:
                # try again untill success
                return trying_get_data(endpoint)
            # return endpoint.with_context(marketplace_data=dict(per_page=self.size,start=((self.page*self.size)-self.size+1),shop_id=self.shop_id.key_id))._run_outgoing() # if post request

        response = trying_get_data(endpoint)
        # context.update({}) #uncomment if need append context
        data = response.json()
        
        records = Wiz

        def search_product(dd):
            P = self.env['product.product']
            product_key_field_name = self.shop_id.marketplace_id.product_key_field_name()

            domain = [('tokopedia_product_id','=',dd.get('id'))]
            if dd.get('sku'):
                sku_domain = [(product_key_field_name,'=', dd.get('sku'))]
                domain = expression.OR([domain,sku_domain])
            return P.search(domain)

        data = data.get('data')
        if data:
            shop = data.get('shop')
            for d in data.get('products'):
                
                product_id = search_product(d)
                new_r = {
                    'header_id':WizHead.id,
                    'name':d.get('name'),
                    'price':d.get('price').replace('.','').replace('Rp',''),
                    'key_id':d.get('id'),
                    'shop_id':shop.get('id'),
                    'code':d.get('sku'),
                    'description':d.get('desc'),
                    'data':d,
                    'product_id':product_id[0].id if product_id else False
                }

                records += Wiz.new(new_r)
        self.line_ids = [(5,0),]
        self.line_ids = records

    @api.onchange('shop_id','page','zize')
    def onchange_page_size(self):
        if self.shop_id and self.page and self.size:
            self._fetch_down_product()

    

    def _validate_product(self):
        empty_products = self.line_ids.filtered(lambda r:r.product_id.id==False)
        if len(empty_products):
            raise UserError("Please define product for:\n%s" % ("\n".join(empty_products.mapped(lambda r:"[%s] %s" % (r.code, r.name,))),))

    def _clear_product(self):
        products = self.line_ids.mapped('product_id')
        qpp = """UPDATE product_product set 
            tokopedia_product_id=0, 
            tokopedia_category_odoo_id=NULL, 
            tokopedia_name=NULL, 
            tokopedia_desc=NULL, 
            tokopedia_response=NULL, 
            tokopedia_price=0,
            tokopedia_status=NULL,
            tokopedia_stock=0,
            tokopedia_category_id=0 
            WHERE id in %s"""
        qpt = """UPDATE product_product set 
            tokopedia_product_id=0, 
            tokopedia_category_odoo_id=NULL, 
            tokopedia_name=NULL, 
            tokopedia_desc=NULL, 
            tokopedia_response=NULL, 
            tokopedia_price=0,
            tokopedia_status=NULL,
            tokopedia_stock=0,
            tokopedia_category_id=0 
            WHERE id in %s"""

        self.env.cr.execute(qpp, (tuple(products.ids),))
        self.env.cr.execute(qpt, (tuple(products.mapped('product_tmpl_id').ids),))

    def submit(self):
        self._validate_product()
        self._clear_product()
        self.line_ids.update_selected_product()

class MarketplaceProductSyncWizard(models.TransientModel):
    _name = 'marketplace.product.sync.wizard'
    _description = 'Marketplace Product Sync Wizard'

    header_id = fields.Many2one('marketplace.product.sync.wizard.header')
    
    key_id = fields.Integer(required=True, help="Key", string="Key")

    code = fields.Char('SKU', required=True)
    name = fields.Char(string="Name", required=True, help="Name on Marketplace")
    description = fields.Char(string="Description", required=False)
    price = fields.Monetary(string="Price", required=True)
    shop_id = fields.Integer(string="Shop ID", required=False)
    currency_id = fields.Many2one('res.currency', 
        default=lambda self:self.env.user.company_id.currency_id.id, 
        compute=lambda self:self.env.user.company_id.currency_id.id)
    product_id = fields.Many2one('product.product', "Product", help="Related Product", required=False)
    data = fields.Binary()

    def _tokopedia_data_map(self):
        self.ensure_one()
        data = eval(self.data)
        return {
            'tokopedia_shop_id':self.shop_id,
            'tokopedia_product_id':self.key_id,
            'tokopedia_name':self.name,
            'tokopedia_desc':self.description,
            'tokopedia_response':str(data),
            'tokopedia_price':self.price,
            'tokopedia_status':'active',
            'tokopedia_stock':data.get('stock'),
            'tokopedia_category_id':data.get('category_id'),
            
        }

    def update_selected_product(self):
        # fix me should dynamic from marketplace_id == tokopedia | shopeee
        # return getattr(self, "_get_token_%s" % (self.provider_id.name.lower(),))()
        # return getattr(self, '_update_selected_product')
        shops = self.env['marketplace.shop'].search([]) # find all shops, to fatest filtering/more efficient
        for rec in self:
            shop = shops.filtered(lambda r:r.key_id==rec.shop_id)
            if not len(shop):
                raise ValueError("Shop ID %s not registered in system" % (str(rec.shop_id),))

            marketplace = shop.marketplace_id.provider_id
            if marketplace == self.env.ref('id_marketplace.provider_tokopedia'):
                # if tokopedia
                rec.product_id.product_tmpl_id.write(rec._tokopedia_data_map())
            
    

    @api.constrains('product_id','shop_id','sku','product_id')
    def constrains_product_id(self):
        errors = []
        for rec in self:
            if rec.product_id.id:
                duplicate = self.search([('id','!=',rec.id), ('header_id','=',rec.header_id.id), ('shop_id','=',rec.shop_id), ('product_id','=',rec.product_id.id)])
                if len(duplicate):
                    errors.append("Product \"%s\" already assigned to \"%s\"" % (rec.product_id.display_name, duplicate.name,))

        if len(errors):
            raise ValidationError("\n".join(errors))