# -*- coding: utf-8 -*-

import logging

from odoo import _, api, fields, models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class TokopediaCategory(models.Model):
	_name = 'tokopedia.category'
	_description = 'Tokopedia Category'

	tokopedia_name = fields.Char(string='Name',required=True)
	tokopedia_id = fields.Char(string='Tokopedia Id',required=True)
	parent_id = fields.Many2one('tokopedia.category', string='Parent')
	parent_tokopedia_categ_id = fields.Char(string='Parent Tokopedia',compute='_get_parent_tokopedia_categ_id')
	child_ids = fields.One2many('tokopedia.category', 'parent_id', string='Child')

	_sql_constraints = [
		('tokopedia_id_unique', 'unique(tokopedia_id)',
		 'Tokopedia category must be unique!')
	]
	_rec_name = 'tokopedia_name'

	def search_key(self, tokopedia_id):
		"""
		search_key Find by tokopedia_id

		:param tokopedia_id: Tokopedia ID
		:type tokopedia_id: integer
		:return: env['tokopedia.category']
		"""
		return self.search([('tokopedia_id','=',tokopedia_id)])

	@api.model
	def create(self, vals):
		
		tokopedia_id = vals.get('tokopedia_id')
		search = self.search_key(tokopedia_id)
		if tokopedia_id and search:
			
			childs = vals.get('child_ids')
			# if len(childs):
			# 	# delete child , so not updated
			# 	del vals['child_ids']

			
			return search.write(vals)
		else:
			# print('--->create')
			return super(TokopediaCategory, self).create(vals)


	@api.multi
	def name_get(self):
		res = []
		for rec in self:
			name = "[%s] %s" % (rec.tokopedia_id, rec.tokopedia_name, )
			res.append((rec.id, name))

		return res

	@api.depends('parent_id')
	def _get_parent_tokopedia_categ_id(self):
		for rec in self:
			res = False
			if rec.parent_id:
				res = rec.parent_id.tokopedia_id
			rec.parent_tokopedia_categ_id = res

	@api.model
	def sync_categories(self,categories):
		"""
		sync_categories from tokopedia responses

		:param categories: list from json
		:type categories: list
		"""
		for categ in categories:
			data = {}
			for item, v in categ.items():
				fname = 'tokopedia_'+item
				if self._fields.get(fname):
					data.update({fname:v})
				if item == 'child' and v:
					child_ids = self._prepare_category_values(v)
					child = []
					for rec in child_ids:
						child.append((0,False,rec))
					data.update({'child_ids':child})
			categor = self.create(data)

	def _prepare_category_values(self, categories):
		vals = []
		for categ in categories:
			data = {}
			for item, v in categ.items():
				fname = 'tokopedia_'+item
				if self._fields.get(fname):
					data.update({fname:v})
				if item == 'child' and v:
					child_ids = self._prepare_category_values(v)
					child = []
					for rec in child_ids:
						child.append((0,False,rec))
					data.update({'child_ids':child})
			vals.append(data)
		return vals


	@api.model
	def sync_from_tokopedia_for_all_categories(self, response):
		errors = []
		headers = response.get('header') 
		if headers.get('error_code') != 0:
			if headers.get('messages'):
				errors.append(headers.get('messages'))
			if headers.get('reason'):
				errors.append(headers.get('reason'))
		else:
			datas = response.get('data').get('categories')
			if len(datas):
				self.sync_categories(datas)
			else:
				raise Warning("No Data")

		if len(errors):
			raise UserError(",\n".join(errors))
		return errors

	@api.model
	def download_tokopedia_category(self):
		"""
		download_tokopedia_category Will Download All Category in Tokopedia
		because category not related to store, so just download it from any account that provider==tokopedia
		"""

		# find account where provider_id == tokopedia
		tokopedias = self.env['marketplace.master'].search([('provider_id','=',self.env.ref('id_marketplace.provider_tokopedia').id)])
		for tokopedia in tokopedias:
			response = tokopedia.get_endpoints('category.getall')._run_outgoing()
			if response.ok:
				self.sync_from_tokopedia_for_all_categories(response=response.json())
				break # stop
			else:
				_logger.error("Sync category with marketplace: %s failed with message: %s" % (tokopedia.name, response.text,))
				_logger.warning('Try Next account if exist')
		