from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError
import json
import requests
import base64

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    TOKOPEDIA_ORDER_STATUS = [
        ('0','Seller cancel order'),
        ('2','Order Reject Replaced'),
        ('3','Order Reject Due Empty Stock'),
        ('4','Order Reject Approval'),
        ('5','Order Canceled by Fraud'),
        ('10','Order rejected by seller'),
        ('11','Order Pending Replacement'),
        ('100','Pending order'),
        ('103','Wait for payment confirmation from third party'),
        ('200','Payment confirmation'),
        ('220','Payment verified, order ready to process'),
        ('221','Waiting for partner approval'),
        ('400','Seller accept order'),
        ('450','Waiting for pickup'),
        ('500','Order shipment'),
        ('501','Status changed to waiting resi have no input'),
        ('520','Invalid shipment reference number (AWB)'),
        ('530','Requested by user to correct invalid entry of shipment reference number'),
        ('540','Delivered to Pickup Point'),
        ('550','Return to Seller'),
        ('600','Order delivered'),
        ('601','Buyer open a case to finish an order'),
        ('690','Fraud Review'),
        ('691','Suspected Fraud'),
        ('695','Post Fraud Review'),
        ('698','Finish Fraud Review'),
        ('699','Order invalid or shipping more than 25 days and payment more than 5 days'),
        ('700','Order finished'),
        ('701','Order assumed as finished but the product not arrived yet to the buyer'),
    ]

    marketplace_order = fields.Boolean(string='Marketplace Order', default=False, track_visibility="onchange")

    tokopedia_order_id = fields.Integer(string='Tokopedia Order ID',default=False, track_visibility="onchange")
    tokopedia_invoice_num = fields.Char(string='Tokopedia Invoice Number', track_visibility="onchange")
    tokopedia_response = fields.Binary(string='Tokopedia Response')
    tokopedia_printed_count = fields.Integer(string='Print Count')
    tokopedia_shop_id = fields.Integer(string="Tokopedia Shop ID")


    shopee_order_no = fields.Char(string="Shopee Order No#")
    shopee_order_id = fields.Integer(strong="Shopee Order ID")

    #https://developer.tokopedia.com/openapi/guide/#/statuscode?id=order-status-codes
    # just use -1 as default because -1 not used by tokopedia default
    tokopedia_order_status = fields.Selection(TOKOPEDIA_ORDER_STATUS, string="Tokopedia Order Status") 

    webhook_raiseerror = fields.Boolean(default=False, track_visibility="onchange")

    marketplace_id = fields.Many2one('marketplace.master')


    tokopedia_download_status = fields.Selection([('-','-'),('to_confirm','to be confirm'),('to_reject','To Reject')], default="-", required=True)

    @api.model
    def _get_tokopedia_order_status(self, key=None, caption=None):
        """
        _get_tokopedia_order_status get tokopedia statuses / status by key / key by caption

        :param key: key such as "200", defaults to None
        :type key: String
        :param caption: "Waiting for pickup", defaults to None
        :type caption: String, optional
        :return: A list of statuses or Char related Status if key/caption defined
        :rtype: List | Dict
        """
        if not key and not caption:
            # return all
            return self.TOKOPEDIA_ORDER_STATUS
        else:
            if key and not caption:
                return self.TOKOPEDIA_ORDER_STATUS.get(key)
            elif not key and caption:
                for k,v in self.TOKOPEDIA_ORDER_STATUS.items():
                    if v==caption:
                        return self.TOKOPEDIA_ORDER_STATUS.get(k)
                        # break loop

    @api.constrains('tokopedia_order_id')
    def constrains_tokopedia_order_id(self):
        """
        constrains_tokopedia_order_id check tokopedia_order_id should not be duplicated with existing active order (Order not in canceled status)

        :raises ValidationError: Order already exist
        """
        for rec in self.filtered(lambda r:r.tokopedia_order_id):
            # find another with same tokopedia_order_id
            domain = [('tokopedia_order_id','=',rec.tokopedia_order_id),
                ('state','!=','cancel'),
                ('id','!=',rec.id)
            ]

            order = self.search(domain)

            if len(order):
                raise ValidationError("Duplicate tokopedia order %s and %s. With Invoice num: %s" % (rec.name, rec.name, rec.tokopedia_invoice_num,))
            




    @api.model
    def decode_tokopedia_order_status(self, code):
        return dict(self.TOKOPEDIA_ORDER_STATUS).get(str(code), False)


    @api.model
    def open_marketplace_order_download_wizard(self):
        """
        open_marketplace_order_download_wizard Show POP Up Wizard to new order listed on marketplace

        :return: ir.actions.act_window
        :rtype: Dict
        """
        form = self.env.ref('id_marketplace.marketplace_order_header_wizard_form_view')
        context = dict(self.env.context or {})
        # context.update({}) #uncomment if need append context
        res = {
            'name': "%s" % (_('Fetch Marketplace Order')),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'form',
            'res_model': 'marketplace.order.header.wizard',
            'view_id': form.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'current'
        }
        return res
    
    @api.model
    def _get_marketplace_order_default_warehouse(self, shop_id):
        # marketplace order get from shop_id.warehouse_id
        # return self.env['marketplace.shop']._search_shop_key(shop_id).warehouse_id
        return self.env['marketplace.shop']._search_shop_key(shop_id).warehouse_id
        
    @api.model
    def _prepare_tokopedia_sale_order_value(self, response, partner_id):
        """
        _prepare_tokopedia_sale_order_value convert response from webhooknotification into sale.order (https://developer.tokopedia.com/openapi/guide/#/webhook/ordernotification)

        :param response: Response from tokopedia
        :type response: Dict
        :param partner_id: partner object
        :type partner_id: env['res.partner']
        :return: [env['sale.order'] , errors]
        :rtype: list
        """

        errors = []
        Product = self.env['product.product']
        vals = {
                'tokopedia_response':str(response),
                'partner_id':partner_id.id,
                # 'freight_service':'land',
                'marketplace_order':True,
                'state':'sent',
                # tokopedia webhook notification use invoice_num as key, but in get order all using invoice_num key
                'client_order_ref':response.get('invoice_ref_num') or response.get('invoice_num'),
                }
        tmp_obj = self.env['sale.order'].new(vals)
        tmp_obj._onchange_eval('partner_id','1',{})
        
        
        for item,value in response.items():
            if item in ('invoice_num','invoice_ref_num'): # on getallorder invoice_ref_num, in webhook use invoice_num
                item = 'invoice_num'
            fname = 'tokopedia_'+item
            # if field found then fill it
            field = self._fields.get(fname)
            if field:
                if field.type in ('selection','char'):
                    value = str(value)
                tmp_obj.update({fname:value})
            if item == 'products':
                order_lines = []
                for product in value:
                    product_id = Product.search([('tokopedia_product_id','=',product.get('id'))])
                    if product_id:
                        order_lines.append((0,False,{
                            'product_id':product_id.id,
                            'name':product_id.display_name,
                            'product_uom_qty':product.get('quantity'),
                            'product_uom': product_id.uom_id.id,
                            'price_unit':product.get('price')
                        }))
                    else:
                        errors.append("Product [%s] %s" % (product.get('sku'),product.get('name')))
                # vals.update({'order_line':order_lines})
                tmp_obj.order_line = order_lines
        shop = self.env['marketplace.shop']._search_shop_key(response.get('shop_id'))
        tmp_obj.warehouse_id = shop.warehouse_id.id
        tmp_obj.marketplace_id = shop.marketplace_id.id
        
        order_vals = tmp_obj._convert_to_write(dict(tmp_obj._cache))

        return [order_vals,errors]

    def _notify_marketplace_order_notification_error(self, message):
        # FIXME: need feature
        pass

    
    def _mark_as_waiting_confirm(self):
        """
        _mark_as_waiting_confirm mark tokopedia_download_status as "to_confirm"
        :rtype: None
        """

        self.ensure_one()

        self = self.filtered(lambda r:r.tokopedia_download_status=='-')

        self.tokopedia_download_status = 'to_confirm'


    def _mark_as_to_reject(self):
        """
        _mark_as_waiting_confirm mark tokopedia_download_status as "to_reject"
        :rtype: None
        """

        self.ensure_one()

        self = self.filtered(lambda r:r.tokopedia_download_status=='-')

        self.tokopedia_download_status = 'to_reject'
        
        

    @api.model
    def create_tokopedia_sale_order(self, response):
        errors = []
        Sale = self.env['sale.order']
        partner_id = self.env.ref('id_marketplace.tokopedia_partner')
        if not response.get('shop_id') or not response.get('order_id') or not response.get('products'):
            raise ValueError("In Order creating tokopedia order response must contains shop_id, order_id and products")
        
        shop = self.env['marketplace.shop']._search_shop_key(response.get('shop_id'))
        if not shop:
            return [0, ['Shop %s unidentified' % (response.get('shop_id'),)]]
        vals, errors = self._prepare_tokopedia_sale_order_value(response, partner_id)
        if len(errors):
            return [0,errors]
        else:
            # encode response
            tokopedia_response = vals.get('tokopedia_response')
            if tokopedia_response:
                vals.update({'tokopedia_response':tokopedia_response.encode('utf-8')})
            so_id = Sale.create(vals)

            # not created yet but validate from product existance


            def accept_order(order_id):
                order_id._mark_as_waiting_confirm()
            

            # FIXME: to remove feature
            # def reject_order(order_id):
            #     self._mark_as_to_reject(order_id)
            
            # self._tokopedia_reject_order(response.get('order_id'))

            
            
            so_id = so_id.with_context(marketplace_order=so_id.marketplace_order)
            # if auto confirm
            if shop.marketplace_id.auto_confirm:
                # need to fix 
                # FIXME: need to try and except, because when confirm not successfull need to notify the users
                so_id.action_confirm()
                if so_id.state not in ('draft','cancel'):
                    # if auto confirm, then if confirm success will send signal accept
                    accept_order(so_id)
                            
            # except Exception as M:
            #     self._notify_marketplace_order_notification_error(M)
            #     so_id.webhook_raiseerror = True
            #     # return [so_id, []]
            
            #always send signal accept
            
            return [so_id,errors]


    @api.model
    def sync_from_tokopedia_for_order_notification(self, response):
        errors = []
        if response.get('order_id') and response.get('invoice_num') and response.get('products'):
            success, res  = self.create_tokopedia_sale_order(response)
            errors += res
            if success:
                return success
        else:
            raise Warning("No valid Data")

        if len(errors):
            raise UserError(",\n".join(errors))
        return errors
        
    @api.model
    def has_proceed(self,sale):
        return any(sale.mapped('picking_ids').filtered(lambda r: r.state == 'done'))

    @api.model
    def create_activity_for_followers(self,data,followers):
        Activity = self.env['mail.activity']
        user_ids = self.env['res.users'].search([('partner_id','in',followers.ids)])
        if user_ids:
            for user in user_ids:
                data.update({
                    'user_id':user.id
                })
                activity_id = Activity.create(data)

    @api.model
    def cancel_order_tokopedia(self, response, sale_ids=None):
        errors = []
        Sale = self.env['sale.order']
        ActivityType = self.env['mail.activity.type']
        
        if not self:
            if sale_ids:
                self = sale_ids
            else:
                self = Sale.search([('tokopedia_order_id','=',value)])
        if not len(self):
            raise ValidationError("Order not Found!")
        
        for sale in self:
            
            if sale.picking_ids:
                if not self.has_proceed(sale):
                    sale.mapped('picking_ids').action_cancel()
                    sale.action_cancel()
                else:
                    data = {
                        'activity_type_id': self.env.ref('mail.mail_activity_data_todo').id,
                        'summary': 'Tokopedia Cancellation',
                        'date_deadline': fields.Date.today()
                    }
                    for picking in sale.picking_ids.filtered(lambda r: r.user_handler_id):
                        data.update({
                            'res_id':picking.id,
                            'res_model_id':self.env.ref('stock.model_stock_picking').id,
                        })
                        self.create_activity_for_followers(data,picking.message_partner_ids)
                    data.update({
                        'res_id':sale.id,
                        'res_model_id':self.env.ref('sale_stock.model_sale_order').id,
                    })
                    self.create_activity_for_followers(data,sale.message_partner_ids)

            
            # why should check sale.tokopedia_order_status != 0??
            #    because cancel tokopedia can be called on pre action that already update the status
            if sale.tokopedia_order_status != '0' :
                sale.tokopedia_order_status = '0'
        if len(errors):
            return [0,errors]
        else:
            if self.state == 'sent':
                self.action_cancel()
            return [1,errors]

    @api.model
    def tokopedia_update_order_status(self, response):
        errors = []
        # -1 means nothing
        # dont use 0 as default
        # 
        order = self.search([('tokopedia_order_id','=',int(response.get('order_id',-1)))])
        if not order:
            raise ValueError("Order Not Found!")
        
        if order and response.get('product_details'):
            # if response.order_status == 0 then cancel
            order_status = response.get('order_status', -1)
            order.tokopedia_order_status = str(order_status)

            if int(order_status) == 0:
                success, res = order.cancel_order_tokopedia(response)
                errors += res

        else:
            raise Warning("No valid Data")

        if len(errors):
            raise UserError(",\n".join(errors))
        return errors

    # @api.model
    # def sync_from_tokopedia_for_order_cancellation(self, response):
    #     errors = []
    #     if response.get('order_id') and response.get('product_details'):
    #         success, res = self.cancel_order_tokopedia(response)
    #         errors += res
    #     else:
    #         raise Warning("No valid Data")

    #     if len(errors):
    #         raise UserError(",\n".join(errors))
    #     return errors

    @api.model
    def _tokopedia_accept_order(self, order_id=None):
        """
        _tokopedia_accept_order action to accept order

        :param order_id: a order id on tokopedia side
        :type order_id: integer
        :raises ValidationError: Error on trying to sent accept request to tokopedia
        :return: True if success
        :rtype: Boolean
        """
        
        if len(self) == 0:
            if not order_id:
                raise ValueError(_("Error to run accept order. Self/Order id not defined well!"))
            if type(order_id)==int:
                self = self.search([('tokopedia_order_id','=',order_id)])
            else:
                self = order_id
        else:
            order_id = self.tokopedia_order_id

        self = self.filtered(lambda r:r.tokopedia_download_status=='to_confirm') # only can confirming with status "to confirm" 
        
        master_id = self.env['marketplace.shop']._search_marketplace(shop_key=self.tokopedia_shop_id)
        if not master_id:
            raise ValueError("Shop ID %s unknown!" % (self.tokopedia_shop_id))
        endpoint = master_id.get_endpoints('order.ack')
        if not endpoint:
            raise UserError("Please define endpoint with key order.ack for accepting order!")
        # url = endpoint.url.format(**{'order_id':order_id,'fs_id':endpoint.fs_id}) # FIXME need to endpoint class
        url = endpoint.with_context(marketplace_data=dict(order_id=order_id))._fetch_url()
        # headers = {
        #     'Authorization': 'Bearer %s' % (master_id.get_token())
        #     }
        # r = requests.post(url=url,headers=headers)
        r = endpoint.with_context(post_data={},marketplace_data={'order_id':order_id})._run_outgoing()
        if r.ok:
            return True
        else:
            raise ValidationError("Failed to accept order %s" % (self.tokopedia_invoice_num,))
        

    @api.model
    def _tokopedia_reject_order(self,order_id):
        master_id = self.env['marketplace.master'].search([('code','=','TKP')])
        endpoint = master_id.get_endpoints('order.nack')

        if not endpoint:
            raise UserError("Please define endpoint with key order.ack for rejecting order!")
        url = endpoint.url.format(**{'order_id':order_id,'fs_id':endpoint.fs_id})
        data_json = json.dumps({
                "reason_code": 1,
                "reason": "out of stock",
                "shop_close_end_date": "17/05/2017",
                "shop_close_note": "Maaf Pak, shop saya tutup untuk liburan"
            })
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % (master_id.get_token())
            }
        # r = requests.post(url=url,headers=headers)

    @api.model
    def tokopedia_label_print(self,order_ids):
        sale_id = self.browse(order_ids)
        # data = {
        #     'sale_id':order_ids,
        #     'order_id': sale_id.tokopedia_order_id,
        #     'fs_id': 0,
        #     'print_count': sale_id.tokopedia_printed_count
        # }
        # return self.env.ref('id_marketplace.action_tokopedia_print_label_report').report_action(self, data=data, config=False)
        
        return {
			'type': 'ir.actions.act_url',
			'url': '/marketplace/order/print/%s/%s' % (sale_id.id,self.tokopedia_printed_count),
			'target': 'new',
			'res_id': self.id,
		}