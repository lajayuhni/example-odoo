import requests

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

import os


class MarketPlaceEndpoint(models.Model):
    _name = 'marketplace.endpoint'
    _description = 'Marketplace Endpoint'

    key = fields.Char(required=True)
    url = fields.Char(required=True)
    
    marketplace_id = fields.Many2one('marketplace.master', required=True)
    fs_id = fields.Char(related="marketplace_id.fs_id", readonly=True)

    type = fields.Selection([('outgoing','Outgoing Endpoint'), ('incoming','Incoming Webhook')], required=True, string="Type", help="Is an Outgoing Endpoint / Webhook Endpoint")

    method = fields.Selection([('get','GET'), ('post','POST'), ('put','PUT')], string="Http Method")

    base_endpoint_url =  fields.Char(related="marketplace_id.base_endpoint_url", readonly=True)
    base_webhook_url =  fields.Char(related="marketplace_id.base_webhook_url", readonly=True)


    _rec_name = 'key'


    # FIXME: SINCE MUST HAVE MULTI MASTER THEN THIS NO LONGER NEED
    _sql_constraints = [
        ('name_unique', 'unique (key,marketplace_id)', 'Key must be unique'),
    ]

    def close_window(self):
        return {'type': 'ir.actions.act_window_close'}

    def _fetch_context_params(self):
        ctx = self._context.get('request_params')
        
        return ctx

    def _run_outgoing(self, token=None):
        """
        _run_outgoing run POST request

        :param token: Auth Token, defaults to None
        :type token: Char, optional
        :raises ValueError: Not acceptable value
        :return: request response
        :rtype: object request
        """
        self.ensure_one()
        self = self.sudo()
        if self.type!='outgoing':
            raise ValueError("Only can run endpoint in outgoing type!")
        
        if not token:
            token = self.marketplace_id.get_token()
        
        # BUILD HEADEERS
        headers = {
            'Authorization': 'Bearer %s' % (token,)
        }
        extra_headers = self._context.get('extra_headers',{})
        headers.update(extra_headers)

        # DEBUGGING PURPOSE
        # FIXME
        # try:
        #     import http.client as http_client
        # except ImportError:
        #     # Python 2
        #     import httplib as http_client
        # http_client.HTTPConnection.debuglevel = 1
        # logging.basicConfig()
        # logging.getLogger().setLevel(logging.DEBUG)
        # requests_log = logging.getLogger("requests.packages.urllib3")
        # requests_log.setLevel(logging.DEBUG)
        # requests_log.propagate = True

        data = self._context.get('%s_data' % (self.method, ))
        params = self._fetch_context_params()
        r = getattr(requests, self.method)(headers=headers, data=data, url=self._fetch_url(), params=params)
        
        # _logger.debug('Response Header: %s' % r.headers)
        # _logger.debug('Response Body: %s' % r.headers)
        
        r.raise_for_status() #try get exception

        if r.status_code == requests.codes.ok:
            return r
    


    def _default_context_marketplace_data(self):
        """
        _default_context_marketplace_data Build default data from context
        So we can pass data to run on endpoint just by define the context with key "marketplace_data"


        :return: Data Dict
        :rtype: Dictionary
        """
        marketplace_data = self._context.get('marketplace_data',{})
        marketplace_data.update({
            'fs_id':self.marketplace_id.fs_id,
            'base_endpoint_url':self.base_endpoint_url,
        })

        return marketplace_data


    
    def _fetch_url(self):
        """
        _fetch_url fetch url to marketplace endpoint cofiguration.

        :return: String of URL
        :rtype: String
        """
        if 'localhost' in self.marketplace_id.base_endpoint_url and self.key=='auth':
            
            return "%s%s" % (self.marketplace_id.base_endpoint_url,'/' if self.marketplace_id.base_endpoint_url[-1]!='/' else '')+'token?grant_type=client_credentials'
        url = self.url
        # marketplace_id = self
        marketplace_data = self._default_context_marketplace_data()
        for i,v in marketplace_data.items():
            url = url.replace("{"+i+"}", str(v))

        base_url = self.base_endpoint_url
        if self.type=='incoming':
            base_url = self.base_webhook_url
            if not base_url:
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')


        if 'https://' in url:
            return url
        
        return base_url+url