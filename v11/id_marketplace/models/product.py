from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
import logging
_logger = logging.getLogger(__name__)

class MarketplaceProductMixin(models.AbstractModel):
    _name = 'marketplace.product.mixin'
    _description = 'Marketplace Product Mixin'

    PRODUCT_STATUS = [('active','Active'),('best','Best (Featured Product)'),('inactive','Inactive (Warehouse)')]
    AVAILBILITY_STATUS = [('UNLIMITED','UNLIMITED'),('LIMITED','LIMITED'),('EMPTY','EMPTY')]
    UPLOAD_STATUS = [('PROCESS','PROCESS'),('DONE','DONE')]

    tokopedia_product_id = fields.Integer()
    tokopedia_name = fields.Char()
    tokopedia_desc = fields.Text()
    tokopedia_response = fields.Text()
    tokopedia_price = fields.Float()
    tokopedia_status = fields.Selection(PRODUCT_STATUS,default='active')
    tokopedia_availbility_status = fields.Selection(AVAILBILITY_STATUS,default='UNLIMITED',string='Tokopedia Availbility')
    tokopedia_stock = fields.Integer()
    tokopedia_category_id = fields.Integer()
    category_id = fields.Many2one('tokopedia.category', string='Category',store=True)
    tokopedia_shop_ids = fields.Many2many('marketplace.shop', string='Tokopedia Shop')
    tokopedia_upload_status_id = fields.Integer(string='Tokopedia Upload ID')
    tokopedia_upload_status_raw = fields.Text()
    tokopedia_upload_status = fields.Selection(UPLOAD_STATUS,default='PROCESS')
    
    @api.onchange('tokopedia_category_id')
    def _onchange_tokopedia_category_id(self):
        self.category_id = self.env['tokopedia.category'].search([('tokopedia_id','=',self.tokopedia_category_id)])

    @api.model
    def _search_by_code(self, code):
        # FIXME: field name should dynamic from ir.config_parameter
        return self.search([('default_code','=', code)])

    @api.model
    def _search_by_tokopedia_product_id(self, code):
        return self.search([('tokopedia_product_id','=', code)])

    @api.model
    def _search_tokopedia(self, code):
        if type(code)==int:
            return self._search_by_tokopedia_product_id(code)
        elif type(code)==str:
            return self._search_by_code(code)
        
        return self

        
    @api.model
    def _search_by_marketplace_key(self, provider, code):
        """
        search environtment to execute _search** method
        :self
        : provider string: eg(tokopedia, shopee)
        :return: object
        """
        return getattr(self, '_search_%s' % provider)(code)


    def sync_tokopedia_product(self,data):
        errors = []
        res = False
        # if called from @api.model
        # search by sku
        sku = data.get('sku')
        pid = data.get('product_id')
        p = self._search_by_marketplace_key('tokopedia', int(pid))
        if not p:
            p = self._search_by_marketplace_key('tokopedia', str(sku))
        
        if len(p) == 1:
            updated = {'tokopedia_response':str(data)}
            for item,v in data.items():
                fname = 'tokopedia_'+item
                if self._fields.get(fname):
                    updated.update({fname:v})
            p.write(updated)
            res = True
        elif len(p)>1:
            errors += ["Found %s product on sherlogic with SKU=%s, Name=%s" % (len(p), sku, data.get('name'),)]
            res = False
        elif len(p)==0:
            # create new?
            # FIXME: should discuss if need auto create if not found?
            # i think not
            errors += ["No Product Found  with SKU=%s, Name=%s" % (sku,data.get('name'))]
            res = False
        else:
            _logger.warning('Product tokopedia with id:%s,name:%s already exist as %s ' % (data.get('id'),data.get('name'),))
        return errors,res
    
    
    @api.model
    def sync_from_tokopedia_for_all_product(self, response):
        """
        https://developer.tokopedia.com/openapi/guide/#/product/getproduct?id=get-all-products-v2
        """
        datas = response.get('data')
        errors = []
        if len(datas):
            for data in datas:
                err,res = self.sync_tokopedia_product(data)
                errors += err
        else:
            raise Warning("No Data")

        if len(errors):
            raise ValidationError(",\n".join(errors))
        return True

    """
    @return: dict()
    https://developer.tokopedia.com/openapi/guide/#/product/createproduct?id=create-products-v2
    """
    def _prepare_product_tokopedia(self):
        self.ensure_one()
        tokopedia = self.env.ref('id_marketplace.tokopedia').shop_id
        
    # _sql_constraints = [
    #     ('tokopedia_product_id_unique', 'unique (tokopedia_product_id)', 'Tokopedia Product ID must be unique'),
    #     ('tokopedia_product_id_possitive', 'CHECK (tokopedia_product_id > 0)', 'Tokopedia Product ID must be positive integer'),
    # ]
    @api.constrains('tokopedia_product_id')
    def _constrains_tokopedia_product_id_unique(self):
        query = """SELECT tokopedia_product_id, COUNT(id) as total FROM product_product WHERE tokopedia_product_id in %s group by tokopedia_product_id having count(id)>1;"""

        tokopedia_product_ids = self.filtered(lambda r:r.tokopedia_product_id>0).mapped(lambda r:r.tokopedia_product_id)
        if len(tokopedia_product_ids):
            self.env.cr.execute(query, (tuple(tokopedia_product_ids),))
            res = self.env.cr.fetchall()
            if len(res):
                tokopedia_product_ids = [x[0] for x in res]
                query = "SELECT CONCAT(%s, id, ', tokopedia_product_id: ', tokopedia_product_id, ', code: ', CASE WHEN default_code IS NULL THEN barcode ELSE default_code END) AS code from "+self._table+" where tokopedia_product_id in %s;"
                self.env.cr.execute(query, (self._table+":  ", tuple(tokopedia_product_ids),))
                res2 = self.env.cr.fetchall()
                raise ValidationError("Duplicated on:\n %s\n\nExecuted result: %s" % (res2,res,))


class ProductTemplate(models.Model):
    _name = 'product.template'
    _inherit = ['product.template', 'mail.thread', 'mail.activity.mixin', 'marketplace.product.mixin']

    @api.onchange('name')
    def _onchange_tokopedia_name(self):
        self.tokopedia_name = self.name

    @api.onchange('description_sale')
    def _onchange_tokopedia_desc(self):
        self.tokopedia_desc = self.description_sale

    @api.onchange('list_price')
    def _onchange_tokopedia_price(self):
        self.tokopedia_price = self.list_price

    def _validate_creating_product(self):
        if not self.tokopedia_name:
            raise UserError('Please fill name of product!')
        if not self.tokopedia_desc:
            raise UserError('Please fill description of product!')
        if self.tokopedia_price <= 0.0:
            raise UserError('Please fill price more than 0.00!')
        if self.tokopedia_stock <= 0.0:
            raise UserError('Please fill stock more than 0.00!')
        if not self.tokopedia_category_id:
            raise UserError('Please select category of product!')
        if not self.tokopedia_shop_ids:
            raise UserError('Please select shop of product!')

    def _prepare_product_value(self):
        return {
                "products":[
                    {
                        "name":self.tokopedia_name,
                        "condition":"NEW",
                        "description":self.tokopedia_desc,
                        "sku":"TST21",
                        "price":self.tokopedia_price,
                        "status":self.tokopedia_availbility_status,
                        "stock":self.tokopedia_stock,
                        "min_order":1,
                        "category_id":self.tokopedia_category_id,
                        "price_currency":"IDR",
                        "weight":200,
                        "weight_unit":"GR",
                        "is_free_return":False,
                        "is_must_insurance":False,
                        "etalase":{
                            "id":1402956
                        },
                        "pictures":[
                            {
                            "file_path":"https://ecs7.tokopedia.net/img/cache/700/product-1/2017/9/27/5510391/5510391_9968635e-a6f4-446a-84d0-ff3a98a5d4a2.jpg"
                            }
                        ],
                        "wholesale":[
                            {
                            "min_qty":2,
                            "price":9500
                            },
                            {
                            "min_qty":3,
                            "price":9000
                            }
                        ],
                        "preorder":{
                            "is_active":True,
                            "duration":5,
                            "time_unit":"DAY"
                        },
                        "videos": [
                            {
                                "source": "youtube",
                                "url": "3T9DAOQIUDo"
                            }
                        ]
                    }
                ]
                }

    def tokopedia_response_data(self, response, key):
        data = response.get('data')
        if data and data.get(key):
            return data.get(key)
            
    # @api.model
    def tokopedia_create_product(self):
        self._validate_creating_product()
        
        master_id = self.env['marketplace.master'].search([('code','=','TKD')])
        if not master_id:
            raise ValueError("No marketplace with code %s!" % (master_id))
        endpoint = master_id.get_endpoints('product.create')
        if not endpoint:
            raise UserError("Please define endpoint with key product.create for creating product!")
        # FIXME Set upload status id per shop
        for shop in self.tokopedia_shop_ids:
            r = endpoint.with_context(post_data=self._prepare_product_value(),marketplace_data={'shop_id':shop.id})._run_outgoing()
            if r.ok:
                self.tokopedia_upload_status_id = int(self.tokopedia_response_data(r.json(), 'upload_id')) 
                return True
            else:
                raise ValidationError("Failed to create product %s" % (self.name,))

    def tokopedia_check_upload_status(self):
        master_id = self.env['marketplace.master'].search([('code','=','TKD')])
        if not master_id:
            raise ValueError("No marketplace with code %s!" % (master_id))
        endpoint = master_id.get_endpoints('product.upload.status')
        if not endpoint:
            raise UserError("Please define endpoint with key product.upload.status for check upload status!")
        # FIXME Set upload status per shop
        for shop in self.tokopedia_shop_ids:
            r = endpoint.with_context(post_data={},marketplace_data={'upload_id':self.tokopedia_upload_status_id,'shop_id':shop.id})._run_outgoing()
            if r.ok:
                upload_data = self.tokopedia_response_data(r.json(), 'upload_data')
                if upload_data:
                    for data in upload_data:
                        if data.get('upload_id') == self.tokopedia_upload_status_id:
                            self.tokopedia_upload_status = data.get('status')
                return True
            else:
                raise ValidationError("Failed to create product %s" % (self.name,))

class ProductProduct(models.Model):
    _name = 'product.product'
    _inherit = ['product.product', 'mail.thread', 'mail.activity.mixin', 'marketplace.product.mixin']

    tokopedia_category_odoo_id = fields.Many2one('tokopedia.category', required=False)
    # tokopedia_id = fields.Integer()
    tokopedia_product_id = fields.Integer(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_name = fields.Char(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_desc = fields.Text(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_response = fields.Text(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_price = fields.Float(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_status = fields.Char(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_stock = fields.Integer(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")
    tokopedia_category_id = fields.Integer(compute="_compute_tokopedia_product_id", store=True, inverse="_inveserse_product_tokopedia")

    def _inveserse_product_tokopedia(self):
        if self.env.user.has_group('product.group_product_variant'):
            # if can manage product variant
            # then will not impact to product template
            return True
        else:
            # if product variant not managed,, will be update on product template too
            fields_tokopedia = list(set([field for fname,field in dict(self._fields).items() if 'tokopedia_' in fname and fname not in ('tokopedia_category_odoo_id')]))
            data = {ff.name:getattr(self, ff.name) for ff in fields_tokopedia if ff}
            self.product_tmpl_id.write(data)

    @api.depends('product_tmpl_id.tokopedia_product_id')
    def _compute_tokopedia_product_id(self):
        for rec in self:
            if len(rec.product_tmpl_id.product_variant_ids)==1 and rec.product_tmpl_id.tokopedia_product_id:
                # only if product template not has any variants / only 1 variant
                rec.update(dict(
                    tokopedia_product_id=rec.product_tmpl_id.tokopedia_product_id,
                    tokopedia_name=rec.product_tmpl_id.tokopedia_name,
                    tokopedia_desc=rec.product_tmpl_id.tokopedia_desc,
                    tokopedia_category_id=rec.product_tmpl_id.tokopedia_category_id,
                ))