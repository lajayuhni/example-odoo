from json2html import *
from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
from odoo.tools.config import config

import logging
_logger = logging.getLogger(__name__)


class MarketplaceOrderHeaderWizard(models.TransientModel):
    """
    MarketplaceOrderHeaderWizard to guide dowload order record from available marketplace_masters
    """

    _name = 'marketplace.order.header.wizard'
    _description = 'Marketplace Order Header Wizard'

    marketplace_id = fields.Many2one('marketplace.master', string="Marketplace", required=True)
    shop_id = fields.Many2one('marketplace.shop', string="Shop", required=True)

    line_ids = fields.One2many('marketplace.order.wizard', 'header_id', string="Orders")

    page = fields.Integer(default=1)
    per_page = fields.Integer(default=30)

    tokopedia_order_status = fields.Selection(selection=lambda self:self._tokopedia_order_status_selection(), string="Order Status", default='220')

    skip_tokopedia_fullfill_by = fields.Boolean(related="shop_id.skip_tokopedia_fullfill_by")

    
    def _tokopedia_order_status_selection(self):
        all_status = self.env['sale.order']._get_tokopedia_order_status()
        whitelists = ['220','221','400']

        res = []
        for status in all_status:
            if status[0] in whitelists:
                # res.update({status[0]:status[1]})
                res.append(status)

        return res

    @api.model
    def create(self, vals):
        return super(MarketplaceOrderHeaderWizard, self).create(vals)


    def _accept_order_to_confirm(self):
        orders = self.line_ids.mapped('order_id').filtered(lambda r:r.id and r.tokopedia_download_status=='to_confirm')

        for o in orders:
            o._tokopedia_accept_order()
            

    def confirm(self):
        """
        confirm Confirm Button on POP Up Wizard
        When confirm button clicked then will convert each lines into new Sale.order object

        :raises ValidationError: Lines contains Error (Product / Order Errors)
        :return: ir.actions.act_window / Tree / Kanban to Order List if success
        :rtype: Dict
        """
        self.ensure_one()
        if not self.user_has_groups('id_marketplace.group_sale_marketplace_user'):
            raise ValidationError("You're not authorized to perform this action!\nConfirm Marketplace Order")
        # only process where order not already defined
        # if config.options.get('staging')!='development':
        #     # not filtering order id when in development for dev purpose
        #     self = self.filtered(lambda r:r.order_id.id==False)

        errors = self.line_ids._constrains_line_errors() # will return None if no issues
        if errors != None and type(errors)==dict and not self.env.context.get('yes_action_confirmed'):
            # return wizard yes/no question
            return errors
        
        self.line_ids._create_order()

        if self.marketplace_id.auto_confirm:
            action = self.env['ir.actions.act_window'].for_xml_id('sale', 'action_orders')
            
        else:
            action = self.env['ir.actions.act_window'].for_xml_id('sale', 'action_quotations')
        action.update({'domain':[('id','in',self.line_ids.mapped('order_id').ids)]})

        self._accept_order_to_confirm()
        return action

    def _get_response_tokopedia(self):
        """
        _get_response_tokopedia sending request to tokopedia to get list of unprocessed order

        :raises ValueError: Response format wrong
        :raises ValidationError: No New Order
        :return: Dict of order data from response
        :rtype: Dict / Json Parse
        """
        res = {}
        # get new order only

        from_date = self.marketplace_id._from_date()
        to_date = self.marketplace_id._to_date()
        page = self.page
        marketplace_data = dict(from_date=from_date,to_date=to_date, page=page, per_page=self.per_page, status=self.tokopedia_order_status)
        r = self.marketplace_id.with_context(marketplace_data=marketplace_data).get_endpoints('order.get.new')._run_outgoing()
        r.raise_for_status()
        if r.ok:
            res = r.json()
            header = res.get('header')
            # if http status 200 but error response failed
            if not res.get('data') and header.get('error_code'):
                raise ValueError("Response Failed(%s): %s" % (header.get('error_code'), "\n".join([header.get('messages'), header.get('reason')])))
            res = res.get('data') #only return data
            if not res:
                raise ValidationError("No Data")

        return res

    def _get_response(self):

        return getattr(self, '_get_response_%s' % self.marketplace_id.provider_id.name.lower())()

    def _prepare_line_shopee(self, data):
        # FIXME
        return {}

    def _prepare_line_tokopedia(self, data):
        """
        _prepare_line_tokopedia build data for new sale order line
        :rtype: Dict
        """
        Order = self.env['sale.order']
        res = {
            'marketplace_key_id':data.get('order_id'),
            'ref':data.get('invoice_ref_num'),
            'raw_response':str(data),
            'status':Order.decode_tokopedia_order_status(data.get('order_status'),),
            'order_id':Order.search([('tokopedia_order_id','=',data.get('order_id'))], limit=1).id,
            'fullfill_by_tokopedia': True if data.get('fulfill_by') else False,

        }
        return res

    def _prepare_line(self, data):
        """
        _prepare_line prepare data based on marketplace settings

        :param data: dict
        :type data: dict
        """

        return getattr(self, "_prepare_line_%s" % self.marketplace_id.provider_id.name.lower())(data)



    def render_response_data(self, datas):
        lines = [(5,0)]
        
        for d in datas:
            l = self._prepare_line(d)
            l.update({'header_id':self.id})
            lines.append((0, 0, l))
        self.line_ids = lines


    def fetch_record(self):
        data = self._get_response()
        self.render_response_data(data)
        
        self.line_ids._compute_json_html_table()
        # return True

    @api.onchange('shop_id','page','per_page','tokopedia_order_status')
    def _onchange_shop(self):
        if self.shop_id and self.tokopedia_order_status and self.page and self.per_page:
            self.fetch_record()
            return
        self.line_ids = [(5,0)]



class MarketplaceOrderWizard(models.TransientModel):
    """
    MarketplaceOrderWizard Sales records of each request
    """
    _name = 'marketplace.order.wizard'
    _description = 'Marketplace Order Wizard'

    header_id = fields.Many2one('marketplace.order.header.wizard', required=True, string="Header")
    marketplace_key_id = fields.Char(string="Marketplcae Order ID", required=True)
    marketplace_id = fields.Many2one('marketplace.master', related="header_id.marketplace_id",readonly=True)
    ref = fields.Char(string="Ref", required=True)
    status = fields.Char()
    raw_response = fields.Text(required=True)
    json_html_table = fields.Html(compute="_compute_json_html_table")

    buyer_info = fields.Html(compute="_compute_json_html_table")
    recipient_info = fields.Html(compute="_compute_json_html_table")
    product_info = fields.Html(compute="_compute_json_html_table")
    product_errors = fields.Text(compute="_compute_json_html_table")

    any_errors = fields.Boolean(compute="_compute_json_html_table")

    order_id = fields.Many2one('sale.order', string="Order")

    fullfill_by_tokopedia = fields.Boolean(store=True, help="Order Will process/handled by Tokopedia")

    @api.model
    def create(self, vals):
        return super(MarketplaceOrderWizard, self).create(vals)

    def _prepare_order(self, partner_id=None):
        if not partner_id:
            partner_id = self.env.ref('id_marketplace.tokopedia_partner')
        return self.env['sale.order']._prepare_tokopedia_sale_order_value(response=eval(self.raw_response), partner_id=partner_id)


    def _when_any_product_unmatch(self):    
        form = self.env.ref('yes_no_wizard.view')
        context = dict(self.env.context or {})
        context.update({
            'yes_no_messages':"Some order has an issue: %s<br/><b>Click YES</b> if only confirm valid order or <b>Click Cancel</b> to discard it" % (", ".join(list(set(self.mapped(lambda r:r.ref))))),
            "no_action":False,
            'yes_action':"""
self.env['%s'].browse(%s).with_context(yes_action_confirmed=1).confirm()
""" % ('marketplace.order.header.wizard', self.mapped('header_id').id,) #should be only 1 header
            
        }) #uncomment if need append context


        res = {
            'name': "%s" % (_("Some product not synced already")),
            'view_type': 'form',# REMOVE IF USING ODOO>=13
            'view_mode': 'form',
            'res_model': 'yes.no.wizard',
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new',
        }
        return res
            


    def _constrains_line_errors(self):
        
        line_with_errors = self.filtered(lambda r:r.product_errors)
        if len(line_with_errors):
            return line_with_errors._when_any_product_unmatch()



    def _filter_only_not_created(self):
        return self.filtered(lambda r:r.order_id.id==False)

    def _filter_to_process(self):
        self = self._filter_only_not_created()

        # if skip
        # self.mapped('header_id') should only return 1 record cause in 1 frame of ui
        if self.mapped('header_id').shop_id.skip_tokopedia_fullfill_by:
            # then return only fullfill_by_tokopedia==False
            return self.filtered(lambda r:not r.fullfill_by_tokopedia)
        return self


    def _create_order(self):
        

        partner_id = self.env.ref('id_marketplace.tokopedia_partner')
        Order = self.env['sale.order']
        
        orders_to_process = self._filter_to_process()
        for rec in orders_to_process:
            raw_response = eval(rec.raw_response) if type(rec.raw_response) != dict else rec.raw_response
            order, error = Order.with_context(default_marketplace_id=rec.header_id.marketplace_id.id).create_tokopedia_sale_order(raw_response)
            
            
            if order:
                rec.update({'order_id':order.id})
                # Order += order

        
            
            


    @api.depends('raw_response','header_id')
    def _compute_json_html_table(self):
        recipient_info_list = [
            'district',
            'city',
        ]
        Product = self.env['product.product']
        for rec in self:
            raw_response = eval(rec.raw_response)
            rec.json_html_table = json2html.convert(json=raw_response, table_attributes="class=\"table\"")
            
            recipient_info = raw_response.get('recipient').copy()
            recipient_info['address'] = {}
            
            for r,v in raw_response.get('recipient')['address'].items():
                if r in recipient_info_list  :
                    recipient_info['address'].update({r:v})
            
            rec.recipient_info = json2html.convert(json=recipient_info, table_attributes="class=\"table\"")
            # rec.buyer_info = json2html.convert(json=raw_response.get('buyer'), table_attributes="class=\"table\"")
            # rec.buyer_info = rec.recipient_info # tokopedia not provide buyer informaition,, just id of customer -> so will hide it
            
            rec.product_info = json2html.convert(json=raw_response.get('products'), table_attributes="class=\"table\"")
            product_errors = []
            for p in raw_response.get('products'):
                # search product
                pp = Product._search_by_marketplace_key(rec.marketplace_id.provider_id.name.lower(), int(p.get('id')))
                if not len(pp):
                    product_errors.append("Product %s on %s not found!" % ("[%s] %s" % (p.get('sku', '-'), p.get('name')), rec.ref, ))

            product_errors = "\n".join(product_errors) if len(product_errors) else False
            rec.product_errors = product_errors
            if product_errors or (rec.order_id.id and rec.order_id.state not in ('cancel')):
                rec.any_errors = True
            
    

    @api.onchange('raw_response')
    def _onchange_raw_response(self):
        self._compute_json_html_table()