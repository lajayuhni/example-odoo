{
    'name': 'ID Marketplace',
    'summary': """
        ID Marketplace
        """,
    'version': '0.0.1',
    'category': 'Sales',
    'author': 'La Jayuhni Yarsyah',
    'description': """
        ID Marketplace Manage
    """,
    'depends': [
        'base_setup','contacts', 'sale_management','stock','sale_stock', 'extra_panel_button', 'yes_no_wizard', 
    ],
    'demo':[
        'demo/marketplace.xml',
    ],
    'data': [
        'data/res.partner.csv',
        'data/marketplace_master.xml',
        'data/tokopedia_enpoints.xml',
        'data/ir_actions.xml',
        'security/security.xml',
        'views/marketplace_master.xml',
        'views/marketplace_token.xml',
        'views/tokopedia_category.xml',
        'views/sale_order_views.xml',
        'views/marketplace_endpoint.xml',
        'views/marketplace_shop.xml',
        'views/product.xml',
        'views/wizard_product.xml',
        'views/wizard_order.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'post_init_hook': 'post_init',
}