from odoo.exceptions import UserError
from .common import MarketPlaceCommonTest
import logging
_logger = logging.getLogger(__name__)

from odoo.tests.common import TransactionCase
from odoo.tests.common import post_install

# @post_install(True)
class TestTokopediaMaster(MarketPlaceCommonTest):

    def setUp(self):
        super(TestTokopediaMaster, self).setUp()

    def test_default_endpoint_on_master_create(self):
        self._setUpMarketplaceMaster()
        self.assertTrue(len(self.tokopedia.outgoing_endpoint_ids)>0, "Endpoint should be created automatically")
    
    def test_sync_shop_ok(self):
        self._setUpMarketplaceMaster()
        response = {
            "data": [
                {
                    "shop_id": 479979,
                    "user_id": 5511917,
                    "shop_name": "SHERLOCK TOOLS",
                    "logo": "https://ecs7.tokopedia.net/img/cache/215-square/shops-1/2018/1/11/479979/479979_d08f6bd9-94c3-46f8-a41c-46b35048c15c.jpg",
                    "shop_url": "https://staging.tokopedia.com/aqua",
                    "is_open": 1,
                    "status": 1,
                    "date_shop_created": "2017-07-20",
                    "domain": "aqua",
                    "admin_id": [
                        5511917,
                        5512258,
                        8968866
                    ],
                    "reason": ""
                },
                {
                    "shop_id": 479742,
                    "user_id": 5511411,
                    "shop_name": "Fincook Store",
                    "logo": "https://ecs7.tokopedia.net/img/cache/215-square/shops-1/2017/6/20/479742/479742_7dd7ec7a-8d55-48ff-9060-c880c5e2f338.png",
                    "shop_url": "https://staging.tokopedia.com/fincook",
                    "is_open": 1,
                    "status": 1,
                    "date_shop_created": "2017-05-22",
                    "domain": "fincook",
                    "admin_id": [
                        5511411
                    ],
                    "reason": ""
                },
                {
                    "shop_id": 479573,
                    "user_id": 5510391,
                    "shop_name": "I`nti.Cosmetic",
                    "logo": "https://ecs7.tokopedia.net/img/cache/215-square/shops-1/2017/3/22/5510391/5510391_a10f37fe-3517-4955-8121-19c973380ffb.jpg",
                    "shop_url": "https://staging.tokopedia.com/icl",
                    "is_open": 1,
                    "status": 1,
                    "date_shop_created": "2017-03-22",
                    "domain": "icl",
                    "admin_id": [
                        5510391
                    ],
                    "reason": ""
                }
            ],
            "status": "200 Ok",
            "error_message": []
        }
        # record = self.env.ref('id_marketplace.tokopedia')
        record = self.tokopedia
        
        record.sync_shop_parse_response_from_tokopedia(response)
        self.assertTrue(len(record.shop_ids)>1)

    def test_sync_shop_exist(self):
        """
        test_sync_shop_exist Test Sync Exising shop
        """
        self._setUpMarketplaceMaster()
        response = {
            "data": [
                {
                    "shop_id": 7762490,
                    "user_id": 5511917,
                    "shop_name": "SHERLOCK TOOLS - UPDATED", # shop name be subfixed "UPDATED"
                    "logo": "https://ecs7.tokopedia.net/img/cache/215-square/shops-1/2018/1/11/479979/479979_d08f6bd9-94c3-46f8-a41c-46b35048c15c.jpg",
                    "shop_url": "https://staging.tokopedia.com/aqua",
                    "is_open": 1,
                    "status": 1,
                    "date_shop_created": "2017-07-20",
                    "domain": "aqua",
                    "admin_id": [
                        5511917,
                        5512258,
                        8968866
                    ],
                    "reason": ""
                },
            ],
            "status": "200 Ok",
            "error_message": []
        }
        # record = self.env.ref('id_marketplace.tokopedia')
        record = self.tokopedia
        
        record.sync_shop_parse_response_from_tokopedia(response)
        self.assertTrue('UPDATED' in record.shop_ids.name, 'Shop Name not updated when trying to sync exsiting shop name')
        self.assertTrue(len(record.shop_ids)==1, 'Shop should still 1. No New shop will created')

    def test_btn_sync_shop(self):
        self._setUpMarketplaceMaster()
        self.tokopedia.shop_ids.unlink()
        self.assertEqual(len(self.tokopedia.shop_ids),0, "Shop Ids must None")
        self.tokopedia.btn_sync_shop()
        self.assertTrue(len(self.tokopedia.shop_ids)>0, "Failed to Create shop. Created shop=%s" % len(self.tokopedia.shop_ids))


    def test_sync_shop_failed(self):
        self._setUpMarketplaceMaster()
        response = {
            "data": None,
            "status": "404 Ok",
            "error_message": ['error message']
        }
        #env['marketplace.master']
        record = self.tokopedia
        
        with self.assertRaises(Exception):
            record.sync_shop_parse_response_from_tokopedia(response)
        
        self.assertTrue(len(record.shop_ids)==1, "Download new shop failed. Shop still 1.")


    def test_register_webhook_404(self):
        """
        test_register_webhook Failed criteria: Wrong address, response 404 in request.post()
        """
        self._setUpMarketplaceMaster()
        with self.assertRaises(Exception):
            self.tokopedia.outgoing_endpoint_ids.filtered(lambda r:r.key=='webhook.register.register').write({'url':'/notfoundpage'})
            self.tokopedia.register_webhook()

    
    def test_register_webhook_200(self):
        """
        test_register_webhook Failed criteria: Wrong address, response 404 in request.post()
        """
        self._setUpMarketplaceMaster()
        # with not self.assertRaises(Exception):
        self.tokopedia.register_webhook()