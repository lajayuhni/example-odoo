from odoo.exceptions import UserError,ValidationError
import logging
_logger = logging.getLogger(__name__)

from .common import MarketPlaceCommonTest


class TestProductAPI(MarketPlaceCommonTest):

    def setUp(self):
        super(TestProductAPI, self).setUp()

        self._setupProduct()
    
    def test_01(self):
        """
        test_01 test if product not exist
        should raise ValidationError
        """
        PP = self.env['product.product']
        # RESPONSE IN V2
        responses = {
            "data": [
                {
                    "product_id": 34,
                    "name": "Kemeja Pria",
                    "sku": "BF10",
                    "shop_id": 7762490,
                    "shop_name": "I`nti.Cosmetic",
                    "category_id": 1805,
                    "desc": "A shirt is a cloth garment for the upper body. Originally an undergarment worn exclusively by men, it has become, in American English, a catch-all term for a broad variety of upper-body garments and undergarments.",
                    "stock": 100,
                    "price": 10000,
                    "status": "Active"
                },
                {
                    "product_id": 14286600,
                    "name": "STABILO Paket Ballpoint Premium Bionic Rollerball - Multicolor",
                    "sku": "",
                    "shop_id": 7762490,
                    "shop_name": "I`nti.Cosmetic",
                    "category_id": 1774,
                    "desc": "Paket\n pulpen premium membuat kegiatan menulis kamu bisa lebih berwarna kenyamanan yang maksimal- memiliki 4 warna Paket\n pulpen premium membuat kegiatan menulis kamu bisa lebih berwarna kenyamanan yang maksimal- memiliki 4 warna",
                    "stock": 11,
                    "price": 75000,
                    "status": "Active"
                }
            ]
        }

        # v1
        # responses = {
        #     "header": {
        #         "process_time": 0.038081586,
        #         "messages": "Your request has been processed successfully"
        #     },
        #     "data": {
        #         "total_data": 245,
        #         "shop": {
        #             "id": 7762490,
        #             "name": "SHERLOCK TOOLS",
        #             "uri": "https://www.tokopedia.com/sherlocktools",
        #             "location": "DKI Jakarta"
        #         },
        #         "products": [
        #             {
        #                 "id": 1112601267,
        #                 "name": "Sarung Tangan Karet Cuci Piring Latex Dotted Synott size L",
        #                 "childs": None,
        #                 "url": "https://www.tokopedia.com/sherlocktools/sarung-tangan-karet-cuci-piring-latex-dotted-synott-size-l?whid=0",
        #                 "image_url": "https://ecs7-p.tokopedia.net/img/cache/200-square/product-1/2020/8/20/88901272/88901272_aead11fd-f9b0-4094-b7b9-47faabe5e6e6_1280_1280",
        #                 "image_url_700": "https://ecs7-p.tokopedia.net/img/cache/700/product-1/2020/8/20/88901272/88901272_aead11fd-f9b0-4094-b7b9-47faabe5e6e6_1280_1280",
        #                 "price": "Rp9.500",
        #                 "shop": {
        #                     "id": 7762490,
        #                     "name": "SHERLOCK TOOLS",
        #                     "url": "https://www.tokopedia.com/sherlocktools",
        #                     "is_gold": True,
        #                     "location": "Jakarta Timur",
        #                     "city": "Jakarta Timur",
        #                     "reputation": "https://inbox.tokopedia.com/reputation/v1/badge/shop/7762490",
        #                     "clover": "https://clover.tokopedia.com/badges/merchant/v1?shop_id=7762490",
        #                     "is_official": True,
        #                     "is_power_badge": True
        #                 },
        #                 "wholesale_price": [],
        #                 "courier_count": 6,
        #                 "condition": 1,
        #                 "category_id": 984,
        #                 "category_name": "Rumah Tangga",
        #                 "category_breadcrumb": "rumah-tangga/kebersihan/sarung-tangan-karet",
        #                 "department_id": 3768,
        #                 "labels": [
        #                     {
        #                         "title": "Cashback",
        #                         "color": "#42b549"
        #                     }
        #                 ],
        #                 "badges": [
        #                     {
        #                         "title": "Official Store",
        #                         "image_url": "https://ecs7-p.tokopedia.net/img/official_store_badge.png",
        #                         "show": True
        #                     }
        #                 ],
        #                 "is_featured": 0,
        #                 "rating": 5,
        #                 "count_review": 302,
        #                 "original_price": "",
        #                 "discount_expired": "",
        #                 "discount_percentage": 0,
        #                 "sku": "8991677001129",
        #                 "stock": 4083,
        #                 "status": 0,
        #                 "is_preorder": False
        #             },
        #             {
        #                 "id": 1029211019,
        #                 "name": "MASKER KAIN RESPIRATOR SHERLOCK with N99 CARBON FILTER - BIRU NAVY",
        #                 "childs": None,
        #                 "url": "https://www.tokopedia.com/sherlocktools/masker-kain-respirator-sherlock-with-n99-carbon-filter-biru-navy?whid=0",
        #                 "image_url": "https://ecs7-p.tokopedia.net/img/cache/200-square/product-1/2020/7/25/88901272/88901272_f5764159-4847-4bb2-a440-58589edefaaf_1200_1200",
        #                 "image_url_700": "https://ecs7-p.tokopedia.net/img/cache/700/product-1/2020/7/25/88901272/88901272_f5764159-4847-4bb2-a440-58589edefaaf_1200_1200",
        #                 "price": "Rp43.900",
        #                 "shop": {
        #                     "id": 7762490,
        #                     "name": "SHERLOCK TOOLS",
        #                     "url": "https://www.tokopedia.com/sherlocktools",
        #                     "is_gold": True,
        #                     "location": "Makassar",
        #                     "city": "Makassar",
        #                     "reputation": "https://inbox.tokopedia.com/reputation/v1/badge/shop/7762490",
        #                     "clover": "https://clover.tokopedia.com/badges/merchant/v1?shop_id=7762490",
        #                     "is_official": True,
        #                     "is_power_badge": True
        #                 },
        #                 "wholesale_price": [
        #                     {
        #                         "quantity_min": 20,
        #                         "quantity_max": 10000,
        #                         "price": 38400
        #                     }
        #                 ],
        #                 "courier_count": 6,
        #                 "condition": 1,
        #                 "category_id": 63,
        #                 "category_name": "Otomotif",
        #                 "category_breadcrumb": "otomotif/aksesoris-pengendara-motor/masker-buff",
        #                 "department_id": 1320,
        #                 "labels": [
        #                     {
        #                         "title": "Cashback",
        #                         "color": "#42b549"
        #                     },
        #                     {
        #                         "title": "Grosir",
        #                         "color": "#ffffff"
        #                     },
        #                     {
        #                         "title": "Dilayani Tokopedia",
        #                         "color": "#ffffff"
        #                     }
        #                 ],
        #                 "badges": [
        #                     {
        #                         "title": "Official Store",
        #                         "image_url": "https://ecs7-p.tokopedia.net/img/official_store_badge.png",
        #                         "show": True
        #                     }
        #                 ],
        #                 "is_featured": 0,
        #                 "rating": 5,
        #                 "count_review": 220,
        #                 "original_price": "",
        #                 "discount_expired": "",
        #                 "discount_percentage": 0,
        #                 "sku": "8991677008951",
        #                 "stock": 28480,
        #                 "status": 0,
        #                 "is_preorder": False
        #             }
        #         ]
        #     }
        # }


        with self.assertRaises(ValidationError):
            PP.sync_from_tokopedia_for_all_product(responses)

    def test_02(self):
        PP = self.env['product.product']
        responses = {
            "data": [
                {
                    "product_id": 34,
                    "name": "Kemeja Pria",
                    "sku": "x123",
                    "shop_id": 7762490,
                    "shop_name": "I`nti.Cosmetic",
                    "category_id": 1805,
                    "desc": "A shirt is a cloth garment for the upper body. Originally an undergarment worn exclusively by men, it has become, in American English, a catch-all term for a broad variety of upper-body garments and undergarments.",
                    "stock": 100,
                    "price": 10000,
                    "status": "Active"
                },
                {
                    "product_id": 14286600,
                    "name": "STABILO Paket Ballpoint Premium Bionic Rollerball - Multicolor",
                    "sku": "x124",
                    "shop_id": 7762490,
                    "shop_name": "I`nti.Cosmetic",
                    "category_id": 1774,
                    "desc": "Paket\n pulpen premium membuat kegiatan menulis kamu bisa lebih berwarna kenyamanan yang maksimal- memiliki 4 warna Paket\n pulpen premium membuat kegiatan menulis kamu bisa lebih berwarna kenyamanan yang maksimal- memiliki 4 warna",
                    "stock": 11,
                    "price": 75000,
                    "status": "Active"
                }
            ]
        }
        try:
            PP.sync_from_tokopedia_for_all_product(responses)
        except Exception as m:
            self.fail('Failed with message: %s' % (m,))
        
