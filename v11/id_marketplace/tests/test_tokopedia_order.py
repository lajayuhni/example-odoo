from odoo import api, fields, models, _
from odoo.exceptions import UserError,ValidationError
import logging
_logger = logging.getLogger(__name__)

from odoo.addons.id_marketplace.tests.common import *
from .common import MarketPlaceCommonTest

class TestSaleOrder(MarketPlaceCommonTest):
    
    def setUp(self):
        super(TestSaleOrder, self).setUp()
        
        self._setUpMarketplaceMaster()
        self._setupProduct()
    
    def test_order_01(self):
        # self._build_test_product()
        SO = self.env['sale.order']
        responses = {
            "fs_id": FSID,
            "order_id": 91110,
            "accept_partial": False,
            "invoice_num": "INV/20200101/XVII/V/12345678",
            "products": [{
                "id": 9199,
                "name": "Product A",
                "quantity": 1,
                "notes": "",
                "weight": 0.01,
                "total_weight": 0.01,
                "price": 10000,
                "total_price": 10000,
                "currency": "Rp",
                "sku": "x123"
            }],
            "customer": {
                "id": 5000000,
                "name": "Buyer Name",
                "phone": "088812345678",
                "email": "buyer@tokopedia.com"
            },
            "recipient": {
                "name": "Receiver Name",
                "phone": "081187654321",
                "address": {
                    "address_full": "Jl. Dr. Satrio, Tokopedia Tower",
                    "district": "Karet Semanggi",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "postal_code": "12950",
                    "district_id":2000,
                    "city_id":123,
                    "province_id":10,
                    "geo": "-6.2211974,106.8172307"
                }
            },
            "shop_id": SHOPID,
            "warehouse_id": 800000,
            "shop_name": "Shop Name",
            "payment_id": 11520000,
            "logistics": {
                "shipping_id": 1,
                "district_id": 2000,
                "city_id": 123,
                "geo": "-6.2211974,106.8172307",
                "shipping_agency": "JNE",
                "service_type": "Reguler"
            },
            "amt": {
                "ttl_product_price": 10000,
                "shipping_cost": 9000,
                "insurance_cost": 0,
                "ttl_amount": 19000,
                "voucher_amount": 0,
                "toppoints_amount": 0
            },
            "dropshipper_info": {
                "name": "",
                "phone": ""
            },
            "voucher_info": {
                "voucher_type": 0,
                "voucher_code": ""
            },
            "device_type": "default_v3",
            "create_time": 1588942139,
            "order_status": 220,
            "custom_fields": {},
            "accept_partial": False
        }

        # try:
        SO.sync_from_tokopedia_for_order_notification(responses)

        #     # self.assertTrue(len(categories)>1)
        # except Exception as m:
        #     self.fail('Failed with message: %s' % (m,))


    def test_order_02_update_status(self):
        """
        test_order_02 Test Webhook Order Update Status to waiting for pickup
        """
        SO = self.env['sale.order']
        # if order status == 0 then it's a cancel request
        self._setUpTokopediaOrder()
        responses = {
                "order_status":450,
                "fs_id":FSID,
                "shop_id":SHOPID,
                "order_id":123,
                "product_details": [
                    {
                        "id":9199,
                        "name":"Product Name",
                        "notes":"",
                        "currency":"Rp.",
                        "weight":0.1,
                        "total_weight":0.2,
                        "price":10000,
                        "total_price":20000,
                        "quantity":2,
                        "sku":"x123"
                    }
                ]
            }
        # try:
        SO.tokopedia_update_order_status(responses)
        self.assertEqual(self.tokopedia_order.tokopedia_order_status,'450', 'SO Tokopedia Status should be updato 450 (Waiting for pickup)')
        # SO.sync_from_tokopedia_for_order_cancellation(responses)
        # categories = self.env['tokopedia.category'].search([])
        # self.assertTrue(len(categories)>1)

    def test_order_03_canceling_order(self):
        """
        test_order_03_canceling_order Test Webhook Order Update Status to cancel, so odoo states should be cancel too
        """
        SO = self.env['sale.order']
        self._setUpTokopediaOrder()
        # if order status == 0 then it's a cancel request
        
        responses = {
                "order_status":0,
                "fs_id":FSID,
                "shop_id":SHOPID,
                "order_id":123,
                "product_details": [
                    {
                        "id":9199,
                        "name":"Product Name",
                        "notes":"",
                        "currency":"Rp.",
                        "weight":0.1,
                        "total_weight":0.2,
                        "price":10000,
                        "total_price":20000,
                        "quantity":2,
                        "sku":"x123"
                    }
                ]
            }
        # try:
        SO.tokopedia_update_order_status(responses)
        self.assertEqual(self.tokopedia_order.tokopedia_order_status,'0', 'SO Tokopedia Status should be updato 450 (Waiting for pickup)')
        self.assertEqual(self.tokopedia_order.state,'cancel', 'SO State should be canceled')
        # SO.sync_from_tokopedia_for_order_cancellation(responses)
        # categories = self.env['tokopedia.category'].search([])
        # self.assertTrue(len(categories)>1)


    def test_04_test_get_all_order(self):
        """
        test_04_test_get_all_order get all order
        """

        Wizard = self.env['marketplace.order.header.wizard']

        wiz_rec = Wizard.create({
            'marketplace_id':self.tokopedia.id,
            'shop_id':self.tokopedia.shop_ids.id,
        })

        wiz_rec._onchange_shop()

        self.assertTrue(len(wiz_rec.line_ids)>0, 'Not getting response in order to get all order from tokopedia')

        # start confirm
        window = wiz_rec.confirm()
        orders = self.env['sale.order'].search(window.get('domain'))
        self.assertEqual(len(orders), len(wiz_rec.line_ids))


    def test_04_test_get_all_order_02(self):
        """
        test_04_test_get_all_order_02 get all order , then manually confirming each generated order
        """

        Wizard = self.env['marketplace.order.header.wizard']

        wiz_rec = Wizard.new({
            'marketplace_id':self.tokopedia.id,
            'shop_id':self.tokopedia.shop_ids.id,
        })

        wiz_rec._onchange_shop()

        self.assertTrue(len(wiz_rec.line_ids)>0, 'Not getting response in order to get all order from tokopedia')

        # start confirm
        confirm = wiz_rec.confirm()
        
        orders = self.env['sale.order'].search(confirm.get('domain'))
        
        orders.action_confirm()
        self.assertTrue(all(orders.mapped(lambda r:r.state not in ('draft','cancel','sent'))), "All orders should confirmed well!")



    def test_05_test_get_all_order_product_not_exist(self):
        """
        test_05_test_get_all_order_product_not_exist get all order
        """

        # deleting product tokoepdia = 1264538726
        self.env['product.product'].search([('tokopedia_product_id','=',1264538726)]) \
            .unlink()


        Wizard = self.env['marketplace.order.header.wizard']

        wiz_rec = Wizard.new({
            'marketplace_id':self.tokopedia.id,
            'shop_id':self.tokopedia.shop_ids.id,
        })
        wiz_rec.fetch_record()


        # start confirm
        
        with self.assertRaises(ValidationError):
            wiz_rec.confirm()

