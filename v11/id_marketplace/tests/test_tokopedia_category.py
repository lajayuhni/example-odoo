from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

from odoo.tests.common import TransactionCase


class TestTokopediaCategory(TransactionCase):
    
    def test_category_01(self):
        TC = self.env['tokopedia.category']
        responses = {
    "header": {
        "process_time": 0.184012578,
        "messages": "",
        "reason": "",
        "error_code": 0
    },
    "data": {
        "categories": [
            {
                "name": "Lainnya",
                "id": "902",
                "child": None
            },
            {
                "name": "Fashion Wanita",
                "id": "1758",
                "child": [
                    {
                        "name": "Atasan",
                        "id": "1768",
                        "child": [
                            {
                                "name": "Polo Shirt",
                                "id": "1774"
                            },
                            {
                                "name": "Blouse",
                                "id": "1771"
                            },
                            {
                                "name": "Kaos",
                                "id": "1769"
                            },
                            {
                                "name": "Kemeja",
                                "id": "1770"
                            },
                            {
                                "name": "Tank Top",
                                "id": "1772"
                            },
                            {
                                "name": "Crop Top",
                                "id": "1773"
                            }
                        ]
                    },
                    {
                        "name": "Celana",
                        "id": "1775",
                        "child": [
                            {
                                "name": "Legging",
                                "id": "1781"
                            },
                            {
                                "name": "Celana Crop",
                                "id": "1779"
                            },
                            {
                                "name": "Hot Pants",
                                "id": "1776"
                            },
                            {
                                "name": "Celana Jeans",
                                "id": "1778"
                            },
                            {
                                "name": "Celana Panjang",
                                "id": "1780"
                            },
                            {
                                "name": "Celana Pendek",
                                "id": "1777"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}

        try:
            TC.sync_from_tokopedia_for_all_categories(responses)
            categories = self.env['tokopedia.category'].search([])
            self.assertTrue(len(categories)>1)
        except Exception as m:
            self.fail('Failed with message: %s' % (m,))

    def test_category_02(self):
        TC = self.env['tokopedia.category']
        responses = {
                        "header": {
                            "process_time": 0.374549464,
                            "messages": "Data not found",
                            "reason": "Unknown Resource",
                            "error_code": 404
                        },
                        "data": {
                            "categories": [
                                {
                                    "name": "fashion-anak / Tas Anak / Tas Backpack Anak",
                                    "id": "1979"
                                },
                                {
                                    "name": "fashion-anak / Tas Anak / Tas Selempang Anak",
                                    "id": "1978"
                                },
                                {
                                    "name": "fashion-anak / Tas Anak / Tas Koper Anak",
                                    "id": "1980"
                                }
                            ]
                        }
                    }

        with self.assertRaises(UserError):
            TC.sync_from_tokopedia_for_all_categories(responses)

        