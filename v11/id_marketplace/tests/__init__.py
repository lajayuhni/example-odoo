from . import test_tokopedia_master
from . import test_endpoint
from . import test_tokopedia_product
from . import test_tokopedia_category
from . import test_tokopedia_order
