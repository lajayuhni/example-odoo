from odoo.tests import TransactionCase

SHOPID = 7762490
FSID = 13521
BASE_ENDPOINT = 'http://localhost:8000/tokopedia'
CLIENTID = 'd023a9eb1a264547a8eca722e2eef220'
CLIENTSECRET = 'aa81ab78dc8d4179bfc243037fadb942'

class MarketPlaceCommonTest(TransactionCase):
    def _setUpWarehouse(self):
        self.warehouse_stock = self.env['stock.warehouse'].create({
            'name': 'Stock.',
            'code': 'STK',
        })
    
    def _setUpMarketplaceMaster(self):
        self._setUpWarehouse()
        # check if any shop id with same key for testing purpose
        shops = self.env['marketplace.shop'].search([('key_id','=',SHOPID)]).write({'active':False})

        
        self.tokopedia = self.env['marketplace.master'].create({
            'name':"Tokopedia 1",
            'code':"TK1",
            'active':1,
            'endpoint_ids':[],
            'client_id':CLIENTID,
            'client_secret':CLIENTSECRET,
            'fs_id':FSID,
            'product_key_field_id':self.env.ref('product.field_product_product_default_code').id,
            'base_endpoint_url':BASE_ENDPOINT,
            'company_id':1,
            'auto_confirm':False,
            'shop_ids':[(0,0,{
                'key_id':SHOPID,
                'name':"SHERLOCK TOOLS",
                'url':'https://www.tokopedia.com/sherlocktools',
                'warehouse_id':self.warehouse_stock.id

            })],
            'provider_id':self.env.ref('id_marketplace.provider_tokopedia').id,
        })

        self.assertTrue(len(self.tokopedia.outgoing_endpoint_ids)>0, 'Outgoing Endpoint should be automatically defined when creating new master based on Marketplace.Provider default.')

    def _setupProduct(self):
        self.product1 = self.env['product.template'].create({
            'name':"Product A",
            'tokopedia_name':"Product A",
            'tokopedia_desc':"XXXXX XXXX XXX" ,
            'tokopedia_response':{},
            'tokopedia_price':200000,
            'tokopedia_status':"1",
            'tokopedia_stock':100,
            'tokopedia_product_id':9199,
            'tokopedia_category_id':1,

            'type':'product',
            'barcode':'x123',
            'default_code':'x123',
            # 'hs_code':'x123',
            'lst_price':100,
            # 'purchase_line_warn':'no-message',
            # 'sale_line_warn':'no-message',
            'tracking':'none',
            'purchase_ok':1,
            'sale_ok':1,
            'company_id':1,
        })

        self.product2 = self.env['product.template'].create({
            'name':"Product B",
            'tokopedia_name':"Product B",
            'tokopedia_desc':"XXXXX XXXX XXX" ,
            'tokopedia_response':{},
            'tokopedia_price':200000,
            'tokopedia_status':"1",
            'tokopedia_stock':100,
            'tokopedia_category_id':1,
            'tokopedia_product_id':9198,
            'tokopedia_category_id':1,

            'type':'product',
            'barcode':'x124',
            'default_code':'x124',
            # 'hs_code':'x124', # no depends on delivery
            'lst_price':100,
            # 'purchase_line_warn':'no-message',
            # 'sale_line_warn':'no-message',
            'tracking':'none',
            'purchase_ok':1,
            'sale_ok':1,
            'company_id':1,
        })

    def _setUpTokopediaOrder(self, order_id=123):
        responses = {
            "fs_id": FSID,
            "order_id": order_id,
            "accept_partial": False,
            "invoice_num": "INV/20200101/XVII/V/12345678",
            "products": [{
                "id": 9199,
                "name": "Product A",
                "quantity": 1,
                "notes": "",
                "weight": 0.01,
                "total_weight": 0.01,
                "price": 10000,
                "total_price": 10000,
                "currency": "Rp",
                "sku": "x123"
            }],
            "customer": {
                "id": 5000000,
                "name": "Buyer Name",
                "phone": "088812345678",
                "email": "buyer@tokopedia.com"
            },
            "recipient": {
                "name": "Receiver Name",
                "phone": "081187654321",
                "address": {
                    "address_full": "Jl. Dr. Satrio, Tokopedia Tower",
                    "district": "Karet Semanggi",
                    "city": "Jakarta Selatan",
                    "province": "DKI Jakarta",
                    "country": "Indonesia",
                    "postal_code": "12950",
                    "district_id":2000,
                    "city_id":123,
                    "province_id":10,
                    "geo": "-6.2211974,106.8172307"
                }
            },
            "shop_id": SHOPID,
            "warehouse_id": 800000,
            "shop_name": "Shop Name",
            "payment_id": 11520000,
            "logistics": {
                "shipping_id": 1,
                "district_id": 2000,
                "city_id": 123,
                "geo": "-6.2211974,106.8172307",
                "shipping_agency": "JNE",
                "service_type": "Reguler"
            },
            "amt": {
                "ttl_product_price": 10000,
                "shipping_cost": 9000,
                "insurance_cost": 0,
                "ttl_amount": 19000,
                "voucher_amount": 0,
                "toppoints_amount": 0
            },
            "dropshipper_info": {
                "name": "",
                "phone": ""
            },
            "voucher_info": {
                "voucher_type": 0,
                "voucher_code": ""
            },
            "device_type": "default_v3",
            "create_time": 1588942139,
            "order_status": 220,
            "custom_fields": {},
            "accept_partial": False
        }

        
        self.tokopedia_order,errors = self.env['sale.order'].create_tokopedia_sale_order(responses)

