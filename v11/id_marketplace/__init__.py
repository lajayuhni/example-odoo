from odoo import api, SUPERUSER_ID

from . import models,controllers

def post_init(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    cr.execute("ALTER TABLE product_product DROP CONSTRAINT IF EXISTS tokopedia_product_id_unique")
    cr.execute("ALTER TABLE product_product DROP CONSTRAINT IF EXISTS tokopedia_product_id_possitive")