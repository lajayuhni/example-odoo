from odoo import fields,models,api,_
from odoo.exceptions import UserError

class PreorderPurchaseLine(models.Model):
    _name = 'preorder.purchase.line'
    _description = 'Purchase Preorder Line'

    preorder_id = fields.Many2one('purchase.preorder', required=True)
    preorder_line_id = fields.Many2one('preorder.line', required=True)

    product_id = fields.Many2one('product.product', string="SKU", required=True)
    product_category_id = fields.Many2one('product.category', required=False, related="preorder_line_id.product_category_id", readonly=True)
    product_qty = fields.Float(digits='product uom', required=True)
    unit_price = fields.Float()
    expected_date = fields.Date(compute="_compute_expected_date", inverse=lambda self:True, store=True)
    purchase_line_id = fields.Many2one('purchase.order.line', string="Related Purchase Item")

    @api.onchange('preorder_line_id','preorder_id','product_id')
    def _onchange_product_id(self):
        for this in self:
            categ_id = this.preorder_line_id.product_category_id.id
            this.expected_date = this.preorder_line_id.expected_date
            return {
                'domain': {'product_id': [('categ_id','=',categ_id)]}
                }

    def unlink(self):
        for this in self:
            if this.purchase_line_id.order_id.state != 'cancel':
                raise UserError(_('Purchase %s not cancel, you must cancel it first.')%(this.purchase_line_id.order_id.name))
        return super(PreorderPurchaseLine, self).unlink()

    @api.onchange('preorder_line_id')
    def _onchange_preorder_line_id(self):
        for this in self:
            if this.preorder_line_id:
                this.expected_date = this.preorder_line_id.expected_date if this.preorder_line_id.expected_date else this.expected_date

    def _prepare_po_line(self,order_id):
        return {
            'order_id': order_id.id,
            'product_id': self.product_id.id,
            'name': self.product_id.name_get()[0][1],
            'price_unit': self.product_id.lst_price,
            'product_qty': self.product_qty,
            'product_uom': self.product_id.uom_po_id.id,
            'date_planned':self.expected_date
        }