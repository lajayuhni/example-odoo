from odoo import fields,models,api,_

class PreorderLine(models.Model):
    _name = 'preorder.line'
    _description = 'Preorder Line'

    name = fields.Char(string='Description', required=True)
    preorder_id = fields.Many2one('purchase.preorder', required=True, ondelete="cascade", onupdate="cascade")
    product_category_id = fields.Many2one('product.category', required=True)
    preorder_qty = fields.Float(digits='product uom', required=True)
    partner_id = fields.Many2one('res.partner',related='preorder_id.partner_id', readonly=True)
    product_capacity = fields.Float(compute='_compute_product_capacity', store=True)
    ordered_qty = fields.Float(compute='_compute_ordered_qty', store=True)
    expected_date = fields.Date(compute="_compute_expected_date", inverse=lambda self:True, store=True)
    of_design = fields.Integer(string='# of Design')

    line_ids = fields.One2many('preorder.purchase.line','preorder_line_id')

    @api.depends('preorder_id')
    def _compute_expected_date(self):
        for rec in self:
            rec.expected_date = rec.preorder_id.expected_date

    @api.onchange('preorder_id')
    def onchange_preorder(self):
        self.expected_date = self.preorder_id.expected_date
    
    @api.depends('partner_id','product_category_id')
    def _compute_product_capacity(self):
        for this in self:
            product_capacity = 0
            if this.partner_id and this.product_category_id:
                product_capacity = sum(this.partner_id.vendor_capacity.filtered(lambda x: x.product_category.id == this.product_category_id.id).mapped('capacity'))
            this.product_capacity = product_capacity

    def name_get(self):
        res = []
        for subl in self:
            name = (subl.product_category_id.name or '') + " " + (str(subl.preorder_qty) if subl.preorder_qty else '') + " " + (str(subl.ordered_qty) if subl.ordered_qty else '') + " " + (str(subl.expected_date) or "")
            res.append((subl.id, name))
        return res

    @api.depends('line_ids.product_qty')
    def _compute_ordered_qty(self):
        for this in self:
            ordered_qty = 0
            if this.line_ids:
                for line in this.line_ids.filtered(lambda x: x.purchase_line_id):
                    if line.purchase_line_id.order_id.state != 'cancel':
                        ordered_qty += line.product_qty
            this.ordered_qty = ordered_qty
            

                