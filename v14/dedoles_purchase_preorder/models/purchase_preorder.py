import logging

from odoo import fields,models,api,_
from datetime import datetime
from odoo.exceptions import UserError
from odoo.tools.misc import formatLang, format_date, get_lang
from odoo.tools import format_datetime,format_date

_logger = logging.getLogger(__name__)

class PurchasePreorder(models.Model):
    _name = 'purchase.preorder'
    _description = 'Purchase Preorder'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'write.tracker']

    name = fields.Char(default=_('New'), required=True, readonly=True, states={'draft':[('readonly',False)]})
    note = fields.Text()
    partner_id = fields.Many2one('res.partner',readonly=True,states={'draft': [('readonly', False)],'send': [('readonly', False)]}, required=True, domain=[('is_vendor','=',True)])
    total_production_capacity = fields.Integer('Total Production Capacity (Monthly)',related='partner_id.total_capacity', required=True)
    max_preordered_capacity = fields.Integer('Max Preordered Capacity',compute="_compute_max_preordered_capcity",store=True)
    date = fields.Date('Preorder Date',default=lambda self: fields.Date.context_today(self),
        readonly=True,
        states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, required=True)
    state = fields.Selection([
        ('draft','Draft'),
        ('sent','Sent to Vendor'),
        ('confirmed','Confirmed'),
        ('partially','Partially Ordered'),
        ('done','Fully Ordered'),
        ('cancelled','Cancelled')
    ],default='draft', required=True)
    currency_id = fields.Many2one('res.currency',related='partner_id.currency_id', store=True)
    expected_date = fields.Date(readonly=True,states={'draft': [('readonly', False)],'send': [('readonly', False)]}, required=True)
    preorder_line_ids = fields.One2many('preorder.line','preorder_id',
        readonly=True,
        states={'draft': [('readonly', False)],'sent': [('readonly', False)]})
    preorder_purchase_line_ids = fields.One2many('preorder.purchase.line','preorder_id')
    purchase_ids = fields.Many2many('purchase.order','purchase_order_and_preorder_rel','preorder_id','purchase_id',compute="_compute_purchase_ids")
    user_id = fields.Many2one(
        'res.users', string='Purchase Representative', index=True,
        default=lambda self: self.env.user, check_company=True)
    purchases_count = fields.Integer(compute="_compute_purchases")

    context_messages = fields.Html(string="Messages", help="Helper field to show message", compute="_compute_context_messages")

    @api.depends_context('context_messages')
    def _compute_context_messages(self):
        self.update({
            'context_messages':self._context.get('context_messages')
        })

    @api.depends('purchase_ids')
    def _compute_purchases(self):
        for rec in self:
            rec.purchases_count = len(rec.purchase_ids.filtered(lambda r:r.state!='cancel'))


    def action_view_purchases(self):
        action = self.env['ir.actions.act_window']._for_xml_id('purchase.purchase_rfq')
        action.update({'domain':[('id','in',self.mapped('purchase_ids').ids)]})
        return action

    @api.depends('preorder_purchase_line_ids')
    def _compute_purchase_ids(self):
        for this in self:
            purchase_line_ids = this.preorder_purchase_line_ids.mapped('purchase_line_id')
            purchase_ids = purchase_line_ids.filtered(lambda x: x.order_id.state != 'cancel').mapped('order_id').ids
            this.purchase_ids = [(6,0,purchase_ids)]

    def unlink(self):
        for this in self:
            po_not_cancel = this.purchase_ids.filtered(lambda x: x.state != 'cancel')
            if po_not_cancel and this.state in ['partially','done']:
                raise UserError(_('Any purchase not cancel, you must cancel it first.'))
        return super(PurchasePreorder, self).unlink()                    


    @api.depends('preorder_line_ids.preorder_qty')
    def _compute_max_preordered_capcity(self):
        for this in self:
            # this.max_preordered_capacity = 0
            # count from grouped preorder_line->grouped by month
            group_format = '%Y%m'
            values = None
            if this.preorder_line_ids:
                month_periods = this.preorder_line_ids.mapped(lambda r:r.expected_date.strftime(group_format)) # important to group years also
                values = []
                for month in month_periods:
                    values += [sum(this.preorder_line_ids.filtered(lambda r:r.expected_date.strftime(group_format) == month).mapped('preorder_qty'))]

            if not values:
                values = [0]
            values.sort(reverse=True)
            this.max_preordered_capacity = values[0]

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            values['name'] = self.env['ir.sequence'].next_by_code('purchase.preorder') or _('New')
        res = super(PurchasePreorder, self).create(values)
        return res

    def button_cancel(self):
        for this in self:
            po_not_cancel = this.purchase_ids.filtered(lambda x: x.state != 'cancel')
            if po_not_cancel:
                raise UserError (_("You cannot cancel this preorder, since there are active RfQs/POs linked to it.\nPlease cancel them first"))
            this.state = 'cancelled'

    def _validate_state_to_write(self, vals, fields=[]):
        # blacklist fields that can't be edited
        if not fields:
            fields = [
                'name',
                'partner_id',
                'expected_date',
                'date',
                'preorder_line_ids',
            ]
        notwriteable = []
        for rec in self:
            for f,v in vals.items():
                if f in fields:
                    field_info = dict(rec._fields).get(f)
                    readonly = field_info.readonly #basically editable
                    states = field_info.states
                    if states:
                        for state,domain in states.items():
                            # ('readonly',Bool)
                            if state == rec.state:
                                for d in domain:
                                    # d -> tuple
                                    if d[0]=='readonly':
                                        readonly = d[1]
                                        break # break if found
                                

                    if readonly:
                        notwriteable += [field_info.string]

        if len(notwriteable):
            raise UserError("Not allowed to modify:\n\n%s" % ("\n".join(notwriteable)))

    def write(self,vals):
        self._validate_state_to_write(vals)
        return super(PurchasePreorder, self.with_context(no_create_track=1)).write(vals)

    def _validate_to_send(self):
        self.ensure_one()
        if not self.preorder_line_ids:
            raise UserError(_("Order Line is Empty. Couldn't sending empty order!%s" % self.name))
    
    def _action_send(self):
        for this in self:
            this._validate_to_send()
            ir_model_data = self.env['ir.model.data']
            try:
                compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
            except ValueError:
                compose_form_id = False
            # Template
            template = self.env.ref('dedoles_purchase_preorder.email_template_purchase_preorder', raise_if_not_found=False)
            lang = False
            if template:
                lang = template._render_lang(this.ids)[this.id]
            if not lang:
                lang = get_lang(self.env).code
            ctx = dict(
                default_model='purchase.preorder',
                default_res_id=this.id,
                default_res_model='purchase.preorder',
                default_use_template=bool(template),
                default_template_id=template and template.id or False,
                default_composition_mode='comment',
                # custom_layout="mail.mail_notification_paynow",
                # model_description=self.with_context(lang=lang).type_name,
                force_email=True
            )
            return {
                'name': _('Compose Email Purchase Preorder'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'mail.compose.message',
                'views': [(compose_form_id, 'form')],
                'view_id': compose_form_id,
                'target': 'new',
                'context': ctx,
            }

    def button_send(self):
        return self._action_send()

    def validate_each_lines(self):
        """
        validate_each_lines Validate
        """
        self.ensure_one()
        notvalid = self.env['preorder.line']
        for line in self.preorder_line_ids:
            if line.product_capacity < line.preorder_qty:
                notvalid += line
        
        msgs = _("For the following categories, you are <b class='text-danger'>ordering more than the available monthly production capacity</b>. Please, review your preorder lines or, if this is correct, add a note with explanation<br/><br/><ul><li>%s</li></ul>") % (
            "</li><li>".join(notvalid.mapped(lambda r:r.product_category_id.name)),
        )

        form = self.env.ref('dedoles_purchase_preorder.purchase_preorder_ctx_message_form_view')
        context = dict(self.env.context or {})
        context.update(dict(
            context_messages=msgs,
            button_next_step="_update_to_confirm"
        )) #uncomment if need append context
        res = {
            'name': "%s - %s" % (_('Warning: Monthly Capacity Reached On Lines - '), self.display_name),
            'view_mode': 'form',
            'res_model': 'purchase.preorder',
            'view_id': form.id,
            'res_id':self.id,
            'type': 'ir.actions.act_window',
            'context': context,
            'target': 'new'
        }
        return res


    def button_next_step(self):
        self.ensure_one()
        action = self._context.get('button_next_step')
        return getattr(self, action)()
        

    def _validate_capacity(self):
        self.ensure_one()
        if self.total_production_capacity < self.max_preordered_capacity:
            # raise Warning(_("You are ordering more total quantities than the vendor’s total monthly production capacity. Please, review your preorder or, if this is correct, add a note with explanation"))
            form = self.env.ref('dedoles_purchase_preorder.purchase_preorder_ctx_message_form_view')
            context = dict(self.env.context or {})
            context.update(dict(
                context_messages="You are <b class='text-danger'>ordering more total quantities than the vendor’s total monthly production capacity</b>. Please, review your preorder or, if this is correct, add a note with explanation.",
                button_next_step="validate_each_lines"
            )) #uncomment if need append context
            res = {
                'name': "%s - %s" % (_('Warning: Monthly Capacity Reached - '), self.display_name),
                'view_mode': 'form',
                'res_model': 'purchase.preorder',
                'view_id': form.id,
                'res_id':self.id,
                'type': 'ir.actions.act_window',
                'context': context,
                'target': 'new'
            }
            return res
        return True


    def _update_to_confirm(self):
        self.state = 'confirmed'

    def _action_confirm(self):
        for this in self:
            valid = this._validate_capacity()
            if type(valid)!=bool:
                return valid
            
            # else no problem occure
            this._update_to_confirm()

    def button_confirm(self):
        return self._action_confirm()

    def _action_create_po(self):
        # Create Header PO
        purchase_obj = self.env['purchase.order']
        line_obj = self.env['purchase.order.line']
        needed_create_po = self.preorder_purchase_line_ids.filtered(lambda x: not x.purchase_line_id)
        if not needed_create_po:
            raise UserError(_("No Item can be converted into PO!"))
        purchase_id = purchase_obj
        if needed_create_po:
            new_purchase_id = purchase_obj.new({
                'partner_id' : self.partner_id.id,
            })
            new_purchase_id.onchange_partner_id()
            new_purchase_id.onchange_partner_id_warning()
            new_data_purchase_id = new_purchase_id._convert_to_write({name: new_purchase_id[name] for name in new_purchase_id._cache})
            purchase_id = purchase_obj.create(new_data_purchase_id)

            for lines in self.preorder_purchase_line_ids.filtered(lambda x: not x.purchase_line_id):
                line_data = lines._prepare_po_line(purchase_id)
                line_new = line_obj.new(line_data)
                line_new.onchange_product_id()
                line_new.onchange_product_id_warning()
                line_new.update({'product_qty': lines.product_qty})
                new_data_line_id = line_new._convert_to_write({name: line_new[name] for name in line_new._cache})
                line_id = line_obj.create(new_data_line_id)
                lines.purchase_line_id = line_id
        
            
        qty_ordered = sum(self.preorder_purchase_line_ids.filtered(lambda r:r.purchase_line_id.state!='cancel').mapped('product_qty'))
        preorder_qty = sum(self.preorder_line_ids.mapped('preorder_qty'))
        if qty_ordered>0:
            state = 'partially'
            if qty_ordered >= preorder_qty:
                state = 'done'
        
        self.state = state
        return purchase_id

    def button_create_draft_po(self):
        return self._action_create_po()
    
    def _action_reset_draft(self):
        self.state = 'draft'
        self.preorder_purchase_line_ids.unlink()

    def button_reset_to_draft(self):
        return self._action_reset_draft()
