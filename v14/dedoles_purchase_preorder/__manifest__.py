# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Purchase - Dedoles',
    'version': '0.1',
    'category': 'Custom',
    'summary': 'Purchase Preorder Management for Dedoles',
    'description': """
            https://docs.google.com/document/d/1qx6nyEiA9CKjA-KOXyYnUt_gYHWor1v2W1PSF5i6tMM/edit#
            Creating Pre Order per product category
            Creating Draft PO from Preorder Data
            Build on: Odoo v 14.0
    """,
    'website': 'https://www.portcities.net',
    'author':'Portcities Ltd.',
    'images': [],
    'depends': [
        'base',
        'web_tour',
        'purchase',
        'h26_product_management',
        'write_tracker',
        ],
    'data': [
        'data/ir_seq.xml',
        'reports/report_purchase_preorder.xml',
        'data/mail_template_data.xml',
        'security/ir.model.access.csv',
        'views/purchase_preorder_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True
}
