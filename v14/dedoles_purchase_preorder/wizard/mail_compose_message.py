from odoo import fields,models,api,_

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    def action_send_mail(self):
        if self._context.get('active_model') == 'purchase.preorder':
            preorder_id = self.env[self._context.get('active_model')].browse(self._context.get('active_id'))
            preorder_id.state = 'sent'
        return super(MailComposer, self).action_send_mail()