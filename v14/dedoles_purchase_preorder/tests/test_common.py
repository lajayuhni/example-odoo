from odoo.tests.common import Form
from odoo.tests import SingleTransactionCase, SavepointCase, tagged
from odoo import fields
from datetime import timedelta
from odoo.api import SUPERUSER_ID

class TestCommon(SingleTransactionCase):

    @classmethod
    def setupUser(cls):
        
        purchase_user = cls.env.ref('purchase.group_purchase_user')
        purchase_manager = cls.env.ref('purchase.group_purchase_manager')
        Users = cls.env['res.users'].with_context({'no_reset_password': True, 'mail_create_nosubscribe': True})
        
        cls.purchase_user = Users.create({
            'name': 'TestUser purchase_user',
            'login': 'purchase_user',
            'email': 'purchase_userp@example.com',
            'notification_type': 'inbox',
            'groups_id': [(6, 0, [purchase_user.id])],
            })
            
        cls.purchase_manager = Users.create({
            'name': 'TestUser purchase_manager',
            'login': 'purchase_manager',
            'email': 'purchase_managerp@example.com',
            'notification_type': 'inbox',
            'groups_id': [(6, 0, [purchase_manager.id])],
            })

    @classmethod
    def setupVendor(cls):
        vendor = cls.env['res.partner']

        vendor_capacity_ids = [(0,0,{
                'product_category':cls.env.ref('product.product_category_4').id,
                'capacity':10000,
            }),
            (0,0,{
                'product_category':cls.env.ref('product.product_category_5').id,
                'capacity':30000,
            })]

        cls.vendors = vendor.create({
            'name':"Vendor Testing",
            'vendor_capacity':vendor_capacity_ids,
            'is_vendor':True,
        })

    
    def setUpPreorder(self):
        
        # self = self.with_user(user)
        Preorder = self.env['purchase.preorder']
        FormPreorder = Form(Preorder)
        FormPreorder.partner_id = self.vendors
        FormPreorder.expected_date = fields.Date.today() + timedelta(days=10)
        self.Preorders = FormPreorder.save()
        return self.Preorders

    def setupPreorderLine(self, preorder, qty=5000):
        with Form(preorder) as record:
            with record.preorder_line_ids.new() as line:
                line.product_category_id = preorder.env.ref('product.product_category_4')
                line.name = "Cotton socks of 3 sizes"
                
                line.preorder_qty = qty


    def setupPreorderPurchaseLine(self, preorder, preorderline, products):
        
        with Form(preorder) as rec:
            for prod in products:
                with rec.preorder_purchase_line_ids.new() as pline:
                    pline.preorder_line_id = preorderline
                    
                    pline.product_id = prod
                    
                    pline.product_qty = 1000 # 1000 of each product


    
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    