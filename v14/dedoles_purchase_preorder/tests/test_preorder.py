from .test_common import TestCommon as Common
from odoo import exceptions
from odoo.tests.common import Form,users
from odoo.api import SUPERUSER_ID
import logging
_logger = logging.getLogger(__name__)

class TestPreorder(Common):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.setupUser()
        cls.setupVendor()

    def _get_last_preorder (self):
        return self.env['purchase.preorder'].search([], limit=1, order="id desc")


    @users('purchase_user')
    def test_000_001_confirming_order_overload_capacity(self):
        """
        test_000_001_confirming_order
        - Create Preorder
        - Qty More than partner capacity
        - Should return window
        """
        self.setUpPreorder()
        # set order line more than capacity
        # capacity in common was set to 10,000 (category 4) and 30,000 (category 5)
        self.setupPreorderLine(self.Preorders, 2000000)

        order = self._get_last_preorder()

        confirm  = order._action_confirm()
        self.assertEqual(order.state, 'draft') # state still in draft
        self.assertTrue(type(confirm)==dict and confirm.get('type')=='ir.actions.act_window')
        
        wiz_context = confirm.get('context')
        next_action = wiz_context.get('button_next_step')
        next_wiz = getattr(order, next_action)() # clicking "OK"

        self.assertEqual(order.state, 'draft') # state still in draft
        self.assertTrue(type(next_wiz)==dict and next_wiz.get('type')=='ir.actions.act_window')
        wiz_context = next_wiz.get('context')
        self.assertTrue("categories" in wiz_context.get('context_messages')) #contains message "categories"

        wiz_context = next_wiz.get('context')
        next_action = wiz_context.get('button_next_step')
        next_wiz = getattr(order, next_action)() # clicking "OK" in 2nd pop up
        self.assertEqual(order.state, 'confirmed')
        

        order.unlink()


    @users('purchase_user')
    def test_001(self):
        
        self.setUpPreorder()
        self.assertTrue(self.Preorders.name, "Name not defined")
        self.assertTrue(self.Preorders.partner_id.id, "Partner should define")
        self.assertTrue(self.Preorders.currency_id==self.Preorders.partner_id.currency_id, "Preorder Currency not match")
        
        self.assertTrue(self.Preorders.total_production_capacity==self.Preorders.partner_id.total_capacity, "Production Capacity should match with vendor")


        
        self.assertEqual(self.Preorders.state, 'draft')
        

    def _get_web_context(self, records, add_web=True, **values):
        """ Helper to generate composer context. Will make tests a bit less
        verbose.

        :param add_web: add web context, generally making noise especially in
          mass mail mode (active_id/ids both present in context)
        """
        base_context = {
            'default_model': records._name,
        }
        if len(records) == 1:
            base_context['default_composition_mode'] = 'comment'
            base_context['default_res_id'] = records.id
        else:
            base_context['default_composition_mode'] = 'mass_mail'
            base_context['active_ids'] = records.ids
        if add_web:
            base_context['active_model'] = records._name
            base_context['active_id'] = records[0].id
        if values:
            base_context.update(**values)
        return base_context

    @users('purchase_user')
    def test_002_send_error(self):
        # clikcing send button with empty lines
        self.Preorders = self._get_last_preorder()
        with self.assertRaises(exceptions.UserError):
            
            wizard_sending_mail = self.Preorders._action_send()
            # should raise error cause preorder line is empty
        
    @users('purchase_user')
    def test_003_send_button(self):
        self.Preorders = self._get_last_preorder() #should created 1 after test_001 run
        self.setupPreorderLine(self.Preorders)

        self.assertEqual(len(self.Preorders.preorder_line_ids), 1)
        self.assertEqual(sum(self.Preorders.preorder_line_ids.mapped('preorder_qty')),5000)

        _logger.info('validating "max_preordered_capacity"')
        self.assertEqual(self.Preorders.max_preordered_capacity, 5000)

        self.assertEqual(self.Preorders.preorder_line_ids.name, 'Cotton socks of 3 sizes')
        _logger.info('Checking name passed')
        form_arch = Form(self.Preorders)._view.get('arch')

         
        self.assertTrue('button_send' in form_arch)
        

        mailmessage = self.env['mail.message'].search_count([])
        wizard_sending_mail = self.Preorders._action_send()
        self.assertEqual(type(wizard_sending_mail), dict) # ensure return ir.actions.act_window
        self.assertEqual(wizard_sending_mail.get('type'), 'ir.actions.act_window')
        self.assertEqual(wizard_sending_mail.get('res_model'), 'mail.compose.message') # ensure mail compose message form

        web_ctx = self._get_web_context(self.Preorders, add_web=True)
        newctx = {}
        newctx.update(web_ctx)
        newctx.update(wizard_sending_mail.get('context'))

        

        composer_form = Form(self.env['mail.compose.message'].with_context(newctx))
        composed_mail = composer_form.save()
        
        composed_mail.action_send_mail()

        newmailmessage = self.env['mail.message'].search_count([])


        self.assertEqual(self.Preorders.state,'sent')

    @users('purchase_user')
    def test_004_confirm_button(self):
        record = self._get_last_preorder()

        # in this case
        # capacity monthly more than preorder
        confirm = record._action_confirm()
        # when _action_confirm should return nothing then state to confirmed
        self.assertEqual(record.state, 'confirmed')

    @users('purchase_user')
    def test_005_confirm_not_editable(self):
        record = self._get_last_preorder()
        with self.assertRaises(exceptions.UserError):
            # when edit should fail
            record.write({'name':'XXXX'})
            record.write({'preorder_line_ids':[(5,0)]}) #not alloweed to edit

    @users('purchase_user')
    def test_006_create_po_without_define_purchase_line(self):
        record = self._get_last_preorder()
        with self.assertRaises(exceptions.UserError):
            record._action_create_po()
        self.assertEqual(record.purchases_count, 0)
    
    @users('purchase_user')
    def test_007_define_purchase_line(self):
        record = self._get_last_preorder()
        

        preorder_lines = record.preorder_line_ids
        # search products->each qty will 1000
        limit_product = 1
        products = self.env['product.product'].search([('categ_id','=',preorder_lines.product_category_id.id)], limit=limit_product)
        with self.env.cr.savepoint():
            self.setupPreorderPurchaseLine(record, preorder_lines, products)
                
        self.assertEqual(len(record.preorder_purchase_line_ids),len(products))
        self.assertEqual(sum(record.preorder_purchase_line_ids.mapped('product_qty')), len(products)*1000)

        check_filled = record.preorder_purchase_line_ids.product_category_id.id \
            and record.preorder_purchase_line_ids.preorder_line_id.id \
                and record.expected_date
        
        self.assertTrue(check_filled)
        # record.write({'purchase_line_ids':[]})

    @users('purchase_user')
    def test_008_create_po(self):
        # creating po
        record = self._get_last_preorder()
        record._action_create_po()
        self.assertEqual(record.state,'partially')
        # write all product to this category
        preorder_lines = record.preorder_line_ids
        
        self.env['product.product'].search([('categ_id','!=',preorder_lines.product_category_id.id)], limit=9) \
            .with_user(SUPERUSER_ID).write({'categ_id':preorder_lines.product_category_id.id})
        
        limit_product = 4
        products = self.env['product.product'].search([('categ_id','=',preorder_lines.product_category_id.id)], limit=limit_product)

        self.setupPreorderPurchaseLine(record, preorder_lines, products)
        self.assertEqual(record.state, 'partially')
        self.assertEqual(sum(record.preorder_purchase_line_ids.mapped('product_qty')), (len(products)+1)*1000)
        self.assertEqual(sum(record.preorder_line_ids.mapped('ordered_qty')), 1000) # ensure still 1000 cause new po not created yet

        # create 2nd po
        record._action_create_po()
        record.preorder_line_ids._compute_ordered_qty()
        self.assertTrue(all(record.preorder_purchase_line_ids.mapped(lambda r:r.purchase_line_id.id))) # ensure all item related to po line
        self.assertEqual(sum(record.preorder_line_ids.mapped('ordered_qty')), 5000) # ensure ordered qty 5000 after po created

        # ensure done
        self.assertEqual(sum(record.preorder_line_ids.mapped('preorder_qty')), sum(record.preorder_line_ids.mapped('ordered_qty')))
        self.assertEqual(record.state, 'done')
        _logger.info("Preorder test DONE!!!")
        
         