{
    'name': 'Write Tracker',
    'summary': """
        Write Tracker
        Tracking writting on ORM""",
    'version': '0.0.1',
    'category': 'technical',
    'author': 'Portcities',
    'description': """
        Tracking all change in models fields
    """,
    'depends': [
        'base',
    ],
    'data': [
        
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    # 'post_init_hook':post_init.
}