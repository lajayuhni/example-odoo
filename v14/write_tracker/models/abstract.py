from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class WriteTracker(models.AbstractModel):
    _name = 'write.tracker'
    _description = 'Write Tracker'

    def _get_fields_string(self,obj,fields):
        description = obj.fields_get([fields])
        description = description and description.get(fields, {})
        description = description and description.get('string', '') or ''
        return description
        

    def _message_fields_many(self,str_ids,obj,vals):
        info = {
            0: _('Created New Line '+str_ids),
            1: _('Updating %s: '+str_ids),
            2: _('Removed Line %s: '+str_ids),
            3: _('Removed Line %s: '+str_ids),
            4: _("Add existing"),
            6: _('many2many'),
        }
        
        body=''
        for val in vals:
            # skipp 4
            if self._context.get('no_create_track') and val[0]==0:
                continue
            if val[0] in [4]:
                continue
            if val and info.get(val[0], False):
                if val[2]:
                    for value in val[2]:
                        if val[0] ==  0:
                            body += info[0]
                        elif val[0] == 1:
                            body += info[1] % ("<b><i>%s</i></b>" % (obj.browse(val[1]).display_name,))
                        
                        try:
                            if type(val)==list and val[0]==0 and len(val)==3:
                                if type(val[2])==dict:
                                    for k,v in val[2].items():
                                        body += "<li>%s:%s</li>" % (k,v)
                            else:
                                if obj._fields[value].type == 'many2one':
                                    body += self._get_message_many2one(obj.browse(val[1]),val[2],value)
                                else:
                                    body += self._get_message(obj.browse(val[1]),val[2],value)    
                        except Exception as e:
                            
                                body += "%s" % (str(val[2]))
                        
                else:
                    body += info[2] + obj.browse(val[1]).display_name
        return body
    
    def _get_message_many2one(self,obj,vals,value):
        body=''
        old_value = getattr(obj,value).display_name or ''
        new_value = getattr(obj,value).browse(vals[value]).display_name or ''
        body += self._get_fields_string(obj,value) +' : ' + old_value +' → '+ new_value
        return body
    
    def _get_message(self,obj,vals,value):
        body=''   
        try:
            old_value = getattr(obj,value) or ''
            new_value = vals[value] or ''
            body += self._get_fields_string(obj,value) +' : ' + str(old_value) +' → '+ str(new_value)
        except Exception as e:
            
            if type(value)==dict:
                for k,v in value.items():
                    body += "%s:%s" % (k,str(v))
                
        return body

    def write(self, vals):
        for rec in self:
            list_changes = []
            for value in vals:
                field = self._fields[value]
                rel = field.comodel_name
                if field.type == 'many2one':
                    # rel = self.env['ir.model.fields'].search([('model_id','=','product.pricelist'),('name','=',value)]).relation
                    
                    # list_changes += [self._message_fields_many(self._get_fields_string(self,value),self.env[rec._name],vals[value])]
                    list_changes += [rec._get_message_many2one(rec,vals,value)]
                elif field.type == 'one2many':
                    list_changes += [self._message_fields_many(self._get_fields_string(self,value),self.env[rel],vals[value])]
                else:
                    list_changes += [rec._get_message(rec, vals, value)]

            # [change a->b, change x->z]
            # should be:
            # change a->b</li><li>change x->z
            if list_changes:
                list_changes = "<ul><li>"+"</li><li>".join(list_changes)+"</li></ul>"
                rec._message_log(body=list_changes)
        return super().write(vals)
                