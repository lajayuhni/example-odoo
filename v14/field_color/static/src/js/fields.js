odoo.define('field_color.widget', function (require) {
    "use strict";
    
    const { Component } = owl;
    const { useState } = owl.hooks;
    const BasicFieldsOwl = require('web.basic_fields_owl');
    const AbstractFieldOwl = require('web.AbstractFieldOwl');
    const field_registry = require('web.field_registry_owl');

    // class ColorAttr extends Component { }
    // ColorAttr.props = ["color"];
    // ColorAttr.template = 'field_color.color_attr_template';
    class Fieldcolor extends AbstractFieldOwl {
        constructor(...args) {
            super(...args);
            
        }
    }
    Fieldcolor.template = 'field_color.widget_template';
    // Fieldcolor.components = { ColorAttr }
    // Fieldcolor.props = ["color"];
    field_registry.add('field_color', Fieldcolor);
    return Fieldcolor;
    
})