{
    'name': 'Fields Color',
    'summary': """
        Fields Color
        """,
    'version': '0.0.1',
    'category': 'web',
    'author': 'La Jayuhni Yarsyah',
    'description': """
        Coloring basic field Char/Text/Float/Integer
    """,
    'depends': [
        'web',
    ],
    'data': [
        'views/assets.xml',
    ],
    'qweb':[
        'static/src/xml/template.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    # 'post_init_hook':post_init.
}