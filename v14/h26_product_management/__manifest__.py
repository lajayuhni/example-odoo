{
    'name': 'Dedoles product management',
    'version': '14.0.0.0.1',
    "sequence": 1,
    'category': 'Inventory',
    'summary': 'This module handles all customizations related to the product management process. These customizations were designed for specific client - Dedoles.',
    'website': 'https://www.26house.com',
    'author': '26HOUSE',
    'company': '26HOUSE',
    'description': """
Product Management
==================
For more information contact info@26house.com or visit 26house.com
Pre viac informácií kontaktujte info@26house.com alebo navštívte 26house.com
    """,
    'depends': [
        'base',
        'product',
        'stock',
        'contacts',
        'sale_management',
        'account_accountant',
        'purchase',
        'website_sale',
        'base_vat',
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/product_design_views.xml',
        'views/menu_product_design_views.xml',
        'views/product_design_strings_views.xml',
        'views/product_template_views.xml',
        'views/product_form_view.xml',
        'views/res_partner_views.xml',
        'views/res_partner_product_dimensions.xml',
        'views/res_partner_packing.xml',
        'views/product_category_view.xml',
        'views/product_product.xml',
        'views/product_sizes.xml',
        'views/product_transfers.xml',
        'views/res_partner_product_transfers.xml',
        'data/product_attribute_data.xml',
        'data/product_sizes_data.xml',
        'data/product_design_strings.xml',
        'views/search_views.xml',
    ],
    'installable': True,
    'auto_install': True,
    'application': False
}
