# **MODULE NAME**

## **Module description**


___
## **Configuration**


### Installing


### Uninstalling
 

___
## **Modifies**

### Models
- `res.example`
- `res.example`
- `res.example`

### Views
- `res.example`
- `res.example`

___
## **Requirements**


___
## **Other info**

|                   |              |   
|-------------------|--------------|
Main contributor/s  | 
Project manager     | 
Odoo versions       | 13, 14
Odoo com / ee       | enterprise
Dependencies        | ```example, h26_example```
