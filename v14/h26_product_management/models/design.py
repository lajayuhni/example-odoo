from odoo import fields, models, _, api, exceptions


class DedolesDesign(models.Model):

    _description = "Product Design"
    _name = "product.design"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(required=True, index=True)
    designer_id = fields.Many2one(
        comodel_name='res.partner',
        string='Designer',
        domain=[('is_designer', '=', True)],
        index=True
    )
    theme_ids = fields.Many2many('product.theme', string='Themes')
    color_ids = fields.Many2many('product.color', string='Colors')
    motive_ids = fields.Many2many('product.motive', string='Motives')
    collection_id = fields.Many2one('product.collection', string='Collection')
    tag_ids = fields.Many2many('product.tag', string='Tags')
    product_ids = fields.One2many('product.template', 'design_id', string='Products')
    product_count = fields.Integer(
        'Number of products',
        compute='_compute_product_count',
        store=False,
        readonly=True
    )
    active = fields.Boolean(
        default=True,
        help="If unchecked, it will allow you to hide the design without removing it."
    )

    _sql_constraints = [
        ('name_unique', 'UNIQUE(name)', "The design name must be unique."),
    ]

    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        if 'name' not in default:
            default['name'] = _("%s (copy)") % (self.name or '')
        return super(DedolesDesign, self).copy(default)

    @api.constrains('designer_id')
    def check_partner_is_designer(self):
        for design in self:
            if design.designer_id and not design.designer_id.is_designer:
                raise exceptions.ValidationError(_("Designer Contact entity must be marked as designer!"))

    @api.depends('product_ids')
    def _compute_product_count(self):
        for design in self:
            design.product_count = len(design.product_ids)

    def action_view_products(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id("stock.product_template_action_product")
        action['domain'] = [('design_id', '=', self.id)]
        action.pop('views', None)
        action['view_mode'] = 'tree,form'
        return action


class DedolesProductTemplate(models.Model):

    _inherit = "product.template"

    design_id = fields.Many2one('product.design', string='Design')
