from odoo import models, fields


class ProductSizes(models.Model):
    _name = "product.sizes"
    _description = "Product sizes"

    #name = fields.Char('Name', index=True, required=True)
    name = fields.Char('Sizes EU', index=True, required=True)
    us = fields.Char('Sizes US', index=True, required=True)
    us_men = fields.Char(index=True)
    us_women = fields.Char(index=True)
    uk = fields.Char('Sizes UK', index=True, required=True)
    cm = fields.Char('cm', index=True)
    men_sock = fields.Char(index=True)
    women_sock = fields.Char(index=True)
