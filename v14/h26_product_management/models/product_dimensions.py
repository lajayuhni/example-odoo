from odoo import api, fields, models, exceptions, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    product_dimensions_line_ids = fields.One2many('product.dimensions', 'supplier', string="Product dimensions")


class ProductDimensions(models.Model):
    _name = "product.dimensions"
    _description = 'Product dimensions'

    supplier = fields.Many2one('res.partner')
    product_category = fields.Many2one('product.category', required=True)
    size = fields.Many2one('product.attribute.value', domain="[('attribute_id','=','Size')]")
    width_cm = fields.Float(string="W (cm)")
    height_cm = fields.Float(string="H (cm)")
    lenght_cm = fields.Float(string="L (cm)")
    width_m = fields.Float(string="W (m)", compute="_compute_cm_to_m")
    height_m = fields.Float(string="H (m)", compute="_compute_cm_to_m")
    lenght_m = fields.Float(string="L (m)", compute="_compute_cm_to_m")
    width_inch = fields.Float(string="W (inch)", compute="_compute_cm_to_inch")
    height_inch = fields.Float(string="H (inch)", compute="_compute_cm_to_inch")
    lenght_inch = fields.Float(string="L (inch)", compute="_compute_cm_to_inch")
    packed_size_width_cm = fields.Float(string="Pack (cm) -w")
    packed_size_height_cm = fields.Float(string="Pack (cm) -h")
    packed_size_depth_cm = fields.Float(string="Pack (cm) -d")
    packed_size_width_m = fields.Float(string="Pack (m) -w", compute="_compute_cm_to_m")
    packed_size_height_m = fields.Float(string="Pack (m) -h", compute="_compute_cm_to_m")
    packed_size_depth_m = fields.Float(string="Pack (m) -d", compute="_compute_cm_to_m")
    pcs_per_box = fields.Integer()
    volume_cm3 = fields.Float(string="Volume (cm3)")
    volume_m3 = fields.Float(string="Volume (m3)", compute="_compute_cm3_to_m3")
    weight_g = fields.Float(string="Weight (g)")
    weight_kg = fields.Float(string="Weight (kg)", compute="_compute_g_to_kg")

    @api.depends('width_cm', 'height_cm', 'lenght_cm', 'packed_size_width_cm',
                 'packed_size_height_cm', 'packed_size_depth_cm')
    def _compute_cm_to_m(self):
        for record in self:
            record.update({
                'width_m': record.width_cm/100,
                'height_m': record.height_cm/100,
                'lenght_m': record.lenght_cm/100,
                'packed_size_width_m': record.packed_size_width_cm/100,
                'packed_size_height_m': record.packed_size_height_cm/100,
                'packed_size_depth_m': record.packed_size_depth_cm/100
            })

    @api.depends('width_cm', 'height_cm', 'lenght_cm')
    def _compute_cm_to_inch(self):
        for record in self:
            record.update({
                'width_inch': record.width_cm/2.54,
                'height_inch': record.height_cm/2.54,
                'lenght_inch': record.lenght_cm/2.54
            })

    @api.depends('volume_cm3')
    def _compute_cm3_to_m3(self):
        for record in self:
            record.volume_m3 = record.volume_cm3/1000000

    @api.depends('weight_g')
    def _compute_g_to_kg(self):
        for record in self:
            record.weight_kg = record.weight_g/1000

    @api.constrains('width_cm', 'height_cm', 'lenght_cm', 'packed_size_width_cm', 'packed_size_height_cm',
                    'packed_size_depth_cm', 'pcs_per_box', 'volume_cm3', 'weight_g')
    def check_not_negative(self):
        for record in self:
            fields_to_check = [
                record.width_cm,
                record.height_cm,
                record.lenght_cm,
                record.packed_size_width_cm,
                record.packed_size_height_cm,
                record.packed_size_depth_cm,
                record.pcs_per_box,
                record.volume_cm3,
                record.weight_g
                ]
            if any(x < 0 for x in fields_to_check):
                raise exceptions.ValidationError(_("Dimension values must be equal or greater than 0."))

    @api.constrains('width_cm', 'height_cm', 'lenght_cm', 'packed_size_width_cm',
                    'packed_size_height_cm', 'packed_size_depth_cm')
    def check_packed_less(self):
        for record in self:
            if (record.width_cm < record.packed_size_width_cm
                    or record.height_cm < record.packed_size_height_cm
                    or record.lenght_cm < record.packed_size_depth_cm):
                raise exceptions.ValidationError(_("Product dimensions can't be lower than packed dimension."))
