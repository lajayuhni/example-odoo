import logging

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class HScode(models.Model):
    _name = "product.hs_code"
    _description = 'Product HS Code'
    name = fields.Char('Display name', compute='_compute_hs_code')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_hs_code(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductCategory(models.Model):
    _inherit = "product.category"
    additional_unit_of_measure = fields.Selection([
        ('CTM', 'c/k - karáty (1 metrický karát = 2 x 10-4 kg)'),
        ('NCL', 'ce/el  - počet buniek'),
        ('CCT', 'ct/l - prepravná kapacita/nosnosť v tonách'),
        ('GRM', 'g - gram'),
        ('GFI', 'gi F/S - gram štiepiteľných izotopov'),
        ('KNS', 'kg H2O2 - kilogram peroxidu vodíka'),
        ('KPO', 'kg K2O - kilogram oxidu draselného'),
        ('KPH', 'kg KOH - kilogram hydroxidu draselného'),
        ('KMA', 'kg met.am. - kilogram metylamínu'),
        ('KNI', 'kg N - kilogram dusíku'),
        ('KSH', 'kg NaOH - kilogram hydroxidu sodného'),
        ('KGM', 'kg/net eda - kilogram čistej hmotnosti bez nálevu'),
        ('KPP', 'kg P2O5 - kilogram oxidu fosforečného '),
        ('KSD', 'kg 90 % sdt - kilogram látky 90% suchej'),
        ('KUR', 'kg U - kilogram uránu'),
        ('MWH', '1 000 kWh - 1 000 kilowatthodín'),
        ('LTR', 'l - liter'),
        ('LPA', 'l alc. 100 % - liter čistého (100%) alkoholu'),
        ('MTR', 'm - meter'),
        ('MTK', 'm2 - štvorcový meter'),
        ('MTQ', 'm3 - kubický meter'),
        ('MQM', '1 000 m3 - 1 000 kubických metrov'),
        ('NPR', 'pa - počet párov'),
        ('NAR', 'p/st - počet kusov'),
        ('CEN', '100 p/st - 100 kusov'),
        ('MIL', '1 000 p/st - 1 000 kusov'),
        ('TJO', 'TJ - terajoule (spaľovacie teplo)'),
        ('ZZZ', 'žiadna doplnková jednotka')
    ])
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('unisex', 'Unisex'),
        ('kids', 'Kids'),
    ])
    eco_material = fields.Boolean('ECO-material')
    additional_description = fields.Char(translate=True)
    hs_code = fields.Many2one('product.hs_code', string="HS code")
