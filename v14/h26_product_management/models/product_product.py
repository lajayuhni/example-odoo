import logging
from odoo import models, fields, api
import re

_logger = logging.getLogger(__name__)


class ProductProductSKU(models.Model):
    _inherit = 'product.product'
    default_code = fields.Char('Code (SKU)', index=True)
    eu = fields.Many2one('product.sizes', string="EU", index=True, store=True)
    us = fields.Char('US Man', related="eu.us_men", index=True)
    us_women = fields.Char(related="eu.us_women", index=True)
    uk = fields.Char(index=True, related="eu.uk", store=True)
    cm = fields.Char(index=True, related="eu.cm", store=True)
    men_sock = fields.Char(related="eu.men_sock", index=True, store=True)
    women_sock = fields.Char(related="eu.women_sock", index=True, store=True)
    estimated_transport_cost = fields.Float(string="Estimated transport cost (ex.VAT)", default=0,
                                            digits=(12, 10),
                                            compute="_compute_estimated_transport_cost")
    estimated_tax_cost = fields.Float(default=0, digits=(12, 10),
                                      compute="_compute_estimated_tax_cost")
    estimated_summary_price = fields.Float(string="Estimated summary price (ex.VAT)", default=0,
                                           digits=(12, 10),
                                           compute="_compute_estimated_summary_price")
    planned_delivery_date = fields.Datetime(index=True, default=0)

    @api.depends('categ_id', 'variant_seller_ids')
    def _compute_estimated_transport_cost(self):
        for record in self:
            for categ in record.categ_id:
                if not record.variant_seller_ids:
                    record.estimated_transport_cost = 0
                    break
                for sellers in record.variant_seller_ids:
                    for seller in sellers:
                        transfers = record.env['product.transfers'].search(
                            ['&', ('product_category_id', '=', categ.id),
                             ('supplier_id', '=', seller.name.id)], limit=1)
                        if not transfers:
                            record.estimated_transport_cost = 0
                            break
                        dimensions = record.env['product.dimensions'].search(
                            ['&', ('product_category', '=', categ.id),
                             ('supplier', '=', seller.name.id)], limit=1)
                        if not dimensions:
                            record.estimated_transport_cost = 0
                            break
                        record.update({
                            'estimated_transport_cost': float(
                                (transfers[0]["type_average_cost"] / transfers[0]["type_volume"]) * (
                                        dimensions[0]["volume_cm3"] / 1000000))
                        })
                        _logger.info(">>> _compute_estimated_transport_cost computed")

    @api.depends('categ_id', 'variant_seller_ids')
    def _compute_estimated_tax_cost(self):
        for record in self:
            for categ in record.categ_id:
                if not record.variant_seller_ids:
                    record.estimated_tax_cost = 0
                    break
                for sellers in record.variant_seller_ids:
                    for seller in sellers:
                        transfers = record.env['product.transfers'].search(
                            ['&', ('product_category_id', '=', categ.id),
                             ('supplier_id', '=', seller.name.id)], limit=1)
                        if not transfers:
                            record.estimated_tax_cost = 0
                            break
                        record.update({
                            'estimated_tax_cost': float(
                                seller.price * (transfers[0]["tax_cost"] / 100))
                        })
                        _logger.info(">>> _compute_shipping_average_cost computed")

    @api.depends('categ_id', 'variant_seller_ids')
    def _compute_estimated_summary_price(self):
        for record in self:
            if not record.variant_seller_ids:
                record.estimated_summary_price = 0
                break
            for sellers in record.variant_seller_ids:
                for seller in sellers:
                    record.update({
                        'estimated_summary_price': float(
                            seller.price + record.estimated_tax_cost + \
                            record.estimated_transport_cost)
                    })
                    _logger.info(">>> _compute_estimated_summary_price computed")

    @api.model_create_multi
    def create(self, vals_list):
        products = super(ProductProductSKU, self.with_context(create_product_product=True)).create(
            vals_list)
        for product in products:
            for attribute in product.product_template_attribute_value_ids:
                atts = self.env['product.sizes'].search([('name', '=', attribute.name)], limit=1)
                for att in atts:
                    if attribute.attribute_id.is_sku:
                        product.eu = att.id
                        _logger.info(">>> created " + str(product.name))
                        product.default_code = (
                                str(product.master_code) + "-" +
                                re.sub(r'\W+', '', str(product.eu.name)))
        self.clear_caches()
        return products
