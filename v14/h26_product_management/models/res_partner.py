""" Customize Res Partner """
from odoo import api, fields, models, _, exceptions


class ResPartner(models.Model):
    """ Inherit res partner """

    _inherit = "res.partner"

    is_designer = fields.Boolean('Is a designer?')

    internal_external_designer = fields.Selection(
        [('internal', 'Internal'), ('external', 'External')], string='Internal / External designer')
    designer_contract = fields.Binary()

    type = fields.Selection(selection_add=[('supplier', 'Supplier')])

    @api.constrains('is_designer', 'bank_ids')
    def check_not_negative(self):
        if self.is_designer and not self.bank_ids:
            raise exceptions.ValidationError(_("Designer's IBAN must be set!"))

    partner_certificate = fields.One2many('certificate', 'supplier', string="Cerficates")
    vendor_capacity = fields.One2many('vendor.capacity', 'supplier', string="Monthly capacity")
    product_transfers_ids = fields.One2many('product.transfers', 'supplier_id', string="Product "
                                                                                        "transfers")


    total_capacity = fields.Integer(string='Total Production Capacity (Monthly)', related='vendor_capacity.vendor_total_capacity')

    business_number = fields.Char(string='Business Registration ID')
    tax_id = fields.Char(string='TAX ID')
    vat = fields.Char(string='VAT ID') # standard Odoo field w label Tax ID
    is_vendor = fields.Boolean('Is a Vendor')
    warned_about_vat_id = fields.Boolean('Check if warning was triggered')

    @api.onchange('is_vendor', 'business_number', 'vat', 'tax_id')
    def warning_vendor_creation_without_vat_id(self):
        if not self.warned_about_vat_id and self.is_vendor and not self.tax_id and self.business_number and self.vat:
            self.warned_about_vat_id = True
            return {
                'warning': {
                    'title': _("Warning"),
                    'message': _("You have not inputted VAT ID. The saving of the vendor will proceed, but double check if he really does not have a VAT ID."),
                },
            }

    @api.model
    def create(self, vals):
        vals['warned_about_vat_id'] = True
        return super(ResPartner, self).create(vals)
