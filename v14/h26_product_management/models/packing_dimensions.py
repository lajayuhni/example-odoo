from odoo import api, fields, models, exceptions, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    packing_dimensions_line_ids = fields.One2many('packing.dimensions', 'supplier', string="Packing dimensions")


class ProductDimensions(models.Model):
    _name = "packing.dimensions"
    _description = 'Products packing dimensions'

    supplier = fields.Many2one('res.partner')
    product_category = fields.Many2one('product.category', required=True)
    box_height_cm = fields.Float(string="Box - height (cm)")
    box_width_cm = fields.Float(string="Box - width (cm)")
    box_lenght_cm = fields.Float(string="Box - lenght (cm)")
    box_height_m = fields.Float(string="Box - height (m)", compute="_compute_cm_to_m")
    box_width_m = fields.Float(string="Box - width (m)", compute="_compute_cm_to_m")
    box_lenght_m = fields.Float(string="Box - lenght (m)", compute="_compute_cm_to_m")
    palette_weight_kg = fields.Float(string="Palette weight (kg)")
    palette_weight_g = fields.Float(string="Palette weight (g)", compute="_compute_kg_to_g")
    pcs_per_packing = fields.Integer()
    box_volume_cm3 = fields.Float(string="Box volume (cm3)")
    box_volume_m3 = fields.Float(string="Box volume (m3)", compute="_compute_cm3_to_m3")
    box_weight_g = fields.Float(string="Box weight (g)")
    box_weight_kg = fields.Float(string="Box weight (kg)", compute="_compute_g_to_kg")
    boxes_per_palette = fields.Integer()

    @api.depends('box_width_cm', 'box_height_cm', 'box_lenght_cm')
    def _compute_cm_to_m(self):
        for record in self:
            record.update({
                'box_width_m': record.box_width_cm/100,
                'box_height_m': record.box_height_cm/100,
                'box_lenght_m': record.box_lenght_cm/100
            })

    @api.depends('box_volume_cm3')
    def _compute_cm3_to_m3(self):
        for record in self:
            record.box_volume_m3 = record.box_volume_cm3/1000000

    @api.depends('box_weight_g')
    def _compute_g_to_kg(self):
        for record in self:
            record.box_weight_kg = record.box_weight_g/1000

    @api.depends('palette_weight_kg')
    def _compute_kg_to_g(self):
        for record in self:
            record.palette_weight_g = record.palette_weight_kg*1000

    @api.constrains('box_height_cm', 'box_width_cm', 'box_lenght_cm', 'palette_weight_kg',
                    'pcs_per_packing', 'box_volume_cm3', 'box_weight_g', 'boxes_per_palette')
    def check_not_negative(self):
        for record in self:
            fields_to_check = [
                record.box_height_cm,
                record.box_width_cm,
                record.box_lenght_cm,
                record.palette_weight_kg,
                record.pcs_per_packing,
                record.box_volume_cm3,
                record.box_weight_g,
                record.boxes_per_palette
                ]
            if any(x < 0 for x in fields_to_check):
                raise exceptions.ValidationError(_("Dimension values must be equal or greater than 0."))
