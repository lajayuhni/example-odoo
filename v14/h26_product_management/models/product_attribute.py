from odoo import models, fields


class ProductAttribute(models.Model):
    _inherit = 'product.attribute'
    is_sku = fields.Boolean(default="False")
