# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class ProductSeason(models.Model):
    _name = "product.season"
    _description = 'Product season'

    name = fields.Char()


class ProductPrimaryCategory(models.Model):
    _name = "product.primary_category"
    _description = 'Product primary category'

    name = fields.Char('Display name', compute='_compute_primary_category')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_primary_category(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductSecondaryCategoryMan(models.Model):
    _name = "product.secondary_category_man"
    _description = 'Product secondary category - Men'

    name = fields.Char('Display name', compute='_compute_secondary_category_man')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_secondary_category_man(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductSecondaryCategoryWoman(models.Model):
    _name = "product.secondary_category_woman"
    _description = 'Product secondary category - Woman'

    name = fields.Char('Display name', compute='_compute_secondary_category_woman')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_secondary_category_woman(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductGenderMKT(models.Model):
    _name = "product.gender_mkt"
    _description = 'Product gender MKT'

    name = fields.Char('Display name', compute='_compute_gender_mkt')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_gender_mkt(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductCut(models.Model):
    _name = "product.cut"
    _description = 'Product cut'

    name = fields.Char('Display name', compute='_compute_cut')
    editable_name = fields.Char('Name')
    code = fields.Char()

    @api.model
    def _compute_cut(self):
        for res in self:
            res.name = res.code + ": " + res.editable_name


class ProductShape(models.Model):
    _name = "product.shape"
    _description = 'Product shape'

    name = fields.Char()


class ProductTemplate(models.Model):
    _inherit = "product.template"
    master_code = fields.Char()
    photo_url = fields.Char('Photo URL')
    season = fields.Many2many('product.season')
    limited_edition = fields.Boolean(default=False)
    limited_edition_name = fields.Char()
    primary_category = fields.Many2one('product.primary_category')
    secondary_category_man = fields.Many2one('product.secondary_category_man', string="Secondary category - man")
    secondary_category_woman = fields.Many2one('product.secondary_category_woman', string="Secondary category - woman")
    color = fields.Many2one('product.color')
    motive = fields.Many2one('product.motive')
    gender_mkt = fields.Many2many('product.gender_mkt', string="Gender MKT")
    collection = fields.Many2one('product.collection')
    cut = fields.Many2one('product.cut')
    material = fields.Char()
    product_special_features = fields.Char()
    care = fields.Char()
    gift_box = fields.Boolean(default=False)
    content = fields.Char()
    shape = fields.Many2one('product.shape')
    planned_launch_date = fields.Date()
    launch_date = fields.Date()
    delist_date = fields.Date()
    bullet_description = fields.Char(translate=True)
    received = fields.Boolean()
    temporary_web_sales_stop = fields.Boolean()
    _sql_constraints = [
        ('master_code_unique', 'unique(master_code)', 'Master code has to be unique!'),
        ('name_unique', 'unique(name)', 'Name of the product has to be unique!')]
