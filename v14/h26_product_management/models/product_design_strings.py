from odoo.osv import expression
from odoo import fields, models, api


def _get_sql_constraints(sub_model):
    return [
        ('name_unique', 'UNIQUE(name)', "The %s name must be unique." % sub_model),
        ('code_unique', 'UNIQUE(code)', "The %s code must be unique." % sub_model),
    ]


class DedolesCodeNameModel(models.AbstractModel):

    _name = "product.design.codename"
    _description = "Product Design Name With Code"
    _order = 'name'

    name = fields.Char(required=True, translate=True)
    code = fields.Char(required=True)

    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, f"{record.code} {record.name}"))
        return result

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []

        if operator == 'ilike' and not (name or '').strip():
            domain = []
        else:
            connector = '&' if operator in expression.NEGATIVE_TERM_OPERATORS else '|'
            domain = [connector, ('code', operator, name), ('name', operator, name)]
        return self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)


class DedolesColor(models.Model):

    _name = "product.color"
    _description = "Product Color"
    _inherit = "product.design.codename"
    _sql_constraints = _get_sql_constraints("color")


class DedolesMotive(models.Model):

    _name = "product.motive"
    _description = "Product Motive"
    _inherit = "product.design.codename"
    _sql_constraints = _get_sql_constraints("motive")


class DedolesCollection(models.Model):

    _name = "product.collection"
    _description = "Product Collection"
    _inherit = "product.design.codename"
    _sql_constraints = _get_sql_constraints("collection")


class DedolesTag(models.Model):

    _name = "product.tag"
    _description = "Product Tag"
    _inherit = "product.design.codename"
    _sql_constraints = _get_sql_constraints("tag")


class DedolesTheme(models.Model):

    _name = "product.theme"
    _description = "Product Theme"
    _order = 'name'

    name = fields.Char(required=True, translate=True)

    _sql_constraints = [
        ('name_unique', 'UNIQUE(name)', "The theme name must be unique."),
    ]
