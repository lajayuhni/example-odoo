# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductTransfers(models.Model):
    _name = "product.transfers"
    _description = "Product transfers"

    # name = fields.Char('Name', index=True, required=True)
    name = fields.Char('Product transfer', index=True, readonly=True)
    supplier_id = fields.Many2one('res.partner', string="Supplier", store=True)
    product_category_id = fields.Many2one('product.category', string="Product Category", store=True)
    logistic_type_id = fields.Many2one('product.logistic.type', string="Logistic type", store=True)
    tax_cost = fields.Float(string="Tax cost (%)")
    type_volume = fields.Float(string="Type volume (m3)")
    type_average_cost = fields.Float(string="Type average cost")

    @api.model
    def create(self, vals):
        new_product_transfer = super(ProductTransfers, self).create(vals)
        new_product_transfer.name = f"{new_product_transfer.supplier_id.name}: {new_product_transfer.product_category_id.name} -> {new_product_transfer.logistic_type_id.name}"
        return new_product_transfer

    def write(self, vals):
        if not vals.get('supplier_id'):
            name = self.supplier_id.name
        else:
            suppliers = self.env['res.partner'].search([('id', '=', int(vals.get('supplier_id')))],
                                                       limit=1)
            if suppliers[0]["name"]:
                name = suppliers[0]["name"]
            else:
                name = "_"

        if not vals.get('product_category_id'):
            product_category = self.product_category_id.name
        else:
            categories = self.env['product.category'].search([('id', '=', int(
                vals.get('product_category_id')))], limit=1)
            if categories[0]["name"]:
                product_category = categories[0]["name"]
            else:
                product_category = "_"

        if not vals.get('logistic_type_id'):
            logistic_type = self.logistic_type_id.name
        else:
            types = self.env['product.logistic.type'].search([('id', '=',
                                                               int(vals.get('logistic_type_id')))],
                                                             limit=1)
            if types[0]["name"]:
                logistic_type = types[0]["name"]
            else:
                logistic_type = "_"

        display_name = f"{name}: {product_category} -> {logistic_type}"
        vals.update({
            'name': display_name,
        })
        # raise ValidationError(_(super(c19MomContact, self).write(vals)))
        return super(ProductTransfers, self).write(vals)
