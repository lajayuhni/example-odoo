from odoo import fields, models, api, exceptions, _


class Certificate(models.Model):
    _name = 'certificate'
    _description = 'Supplier Certificate'

    certificate_number = fields.Char('Cerficate Numer')
    supplier = fields.Many2one('res.partner')
    certificate_type = fields.Many2one('product.category')
    certificate_valid = fields.Date()
    certificate_expiration = fields.Date()
    pdf_certificate = fields.Binary('PDF certificate')

    @api.constrains('certificate_valid', 'certificate_expiration')
    def check_expiration_after_validation(self):
        for record in self:
            if record.certificate_valid > record.certificate_expiration:
                raise exceptions.ValidationError(_("Certificate expiration must be later than certificate validity."))
