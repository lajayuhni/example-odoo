from odoo import models, fields


class ProductLogisticType(models.Model):
    _name = "product.logistic.type"
    _description = "Product logistic type"

    #name = fields.Char('Name', index=True, required=True)
    name = fields.Char('Product logistic type', index=True, required=True)
