from odoo import fields, models, api, exceptions, _

class VendorCapacity(models.Model):
    _name = 'vendor.capacity'
    _description = 'Vendor Capacity'

    supplier = fields.Many2one('res.partner')
    product_category = fields.Many2one('product.category')
    capacity = fields.Integer("Monthly capacity")

    vendor_total_capacity = fields.Integer("Total capacity", compute='_compute_count_total_capacity')

    @api.onchange('capacity')
    def check_not_negative(self):
        for record in self:
            if record.capacity < 0:
                raise exceptions.ValidationError(_("Capacity values must be equal or greater than 0."))

    @api.onchange('capacity')
    def _compute_count_total_capacity(self):
        total_sum = 0
        for record in reversed(self):
            total_sum += record.capacity
            record.vendor_total_capacity = total_sum
