from odoo.tests import TransactionCase
from odoo.exceptions import ValidationError


class ModelProductDimensions(TransactionCase):
    def setUp(self):
        super(ModelProductDimensions, self).setUp()

        self.product_dimensions = self.env['product.dimensions']
        self.supplier = self.env['res.partner'].create({'name': 'Test supplier'}).id
        self.category = self.env['product.category'].create({"name": "Test Category"}).id
        self.attribute = self.env['product.attribute'].create({"name": "Size"}).id
        self.size = self.env['product.attribute.value'].create({"name": "Test size", "attribute_id": self.attribute}).id

    def test_model_product_dimensions_exists(self):
        product_dimensions = self.env['product.dimensions']
        fields = list(product_dimensions.fields_get())

        # Check if our fields are in product_dimensions model
        self.assertIn('supplier', fields)
        self.assertIn('product_category', fields)
        self.assertIn('size', fields)
        self.assertIn('width_cm', fields)
        self.assertIn('height_cm', fields)
        self.assertIn('lenght_cm', fields)
        self.assertIn('width_inch', fields)
        self.assertIn('height_inch', fields)
        self.assertIn('lenght_inch', fields)
        self.assertIn('packed_size_width_cm', fields)
        self.assertIn('packed_size_height_cm', fields)
        self.assertIn('packed_size_depth_cm', fields)
        self.assertIn('packed_size_width_m', fields)
        self.assertIn('packed_size_height_m', fields)
        self.assertIn('packed_size_depth_m', fields)
        self.assertIn('pcs_per_box', fields)
        self.assertIn('volume_cm3', fields)
        self.assertIn('volume_m3', fields)
        self.assertIn('weight_g', fields)
        self.assertIn('weight_kg', fields)

    def test_set_values(self):
        # Create new test_product_dimensions
        test_product_dimensions = self.product_dimensions.create({
            'supplier': self.supplier,
            'product_category': self.category,
            'size': self.size,
            'width_cm': 2.54,
            'height_cm': 2.54,
            'lenght_cm': 2.54,
            'packed_size_width_cm': 1,
            'packed_size_height_cm': 1,
            'packed_size_depth_cm': 1,
            'pcs_per_box': 10,
            'volume_cm3': 1000000,
            'weight_g': 1000,
        })

        # Check if the values are stored
        self.assertEqual(test_product_dimensions.supplier.id, self.supplier)
        self.assertEqual(test_product_dimensions.product_category.id, self.category)
        self.assertEqual(test_product_dimensions.size.id, self.size)
        self.assertEqual(test_product_dimensions.width_cm, 2.54)
        self.assertEqual(test_product_dimensions.height_cm, 2.54)
        self.assertEqual(test_product_dimensions.lenght_cm, 2.54)
        self.assertEqual(test_product_dimensions.packed_size_width_cm, 1)
        self.assertEqual(test_product_dimensions.packed_size_height_cm, 1)
        self.assertEqual(test_product_dimensions.packed_size_depth_cm, 1)
        self.assertEqual(test_product_dimensions.pcs_per_box, 10)
        self.assertEqual(test_product_dimensions.volume_cm3, 1000000)
        self.assertEqual(test_product_dimensions.weight_g, 1000)

    def test_computed_values(self):
        # Create new test_product_dimensions
        test_product_dimensions = self.product_dimensions.create({
            'supplier': self.supplier,
            'product_category': self.category,
            'size': self.size,
            'width_cm': 2.54,
            'height_cm': 2.54,
            'lenght_cm': 2.54,
            'packed_size_width_cm': 1,
            'packed_size_height_cm': 1,
            'packed_size_depth_cm': 1,
            'pcs_per_box': 10,
            'volume_cm3': 1000000,
            'weight_g': 1000,
        })

        # Check if the values are correct
        self.assertEqual(test_product_dimensions.width_inch, 1)
        self.assertEqual(test_product_dimensions.height_inch, 1)
        self.assertEqual(test_product_dimensions.lenght_inch, 1)
        self.assertEqual(test_product_dimensions.packed_size_width_m, 0.01)
        self.assertEqual(test_product_dimensions.packed_size_height_m, 0.01)
        self.assertEqual(test_product_dimensions.packed_size_depth_m, 0.01)
        self.assertEqual(test_product_dimensions.volume_m3, 1)
        self.assertEqual(test_product_dimensions.weight_kg, 1)

    def test_check_not_negative(self):
        with self.assertRaises(ValidationError):
            # Create new test_product_dimensions
            self.product_dimensions.create({
                'supplier': self.supplier,
                'product_category': self.category,
                'size': self.size,
                'width_cm': -1,
                'height_cm': 2.54,
                'lenght_cm': 2.54,
                'packed_size_width_cm': 1,
                'packed_size_height_cm': 1,
                'packed_size_depth_cm': 1,
                'pcs_per_box': 10,
                'volume_cm3': 1000000,
                'weight_g': 1000,
            })

    def test_check_packed_less(self):
        with self.assertRaises(ValidationError):
            # Create new test_product_dimensions
            self.product_dimensions.create({
                'supplier': self.supplier,
                'product_category': self.category,
                'size': self.size,
                'width_cm': 2.54,
                'height_cm': 2.54,
                'lenght_cm': 2.54,
                'packed_size_width_cm': 100,
                'packed_size_height_cm': 1,
                'packed_size_depth_cm': 1,
                'pcs_per_box': 10,
                'volume_cm3': 1000000,
                'weight_g': 1000,
            })
