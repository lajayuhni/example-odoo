from odoo.tests import TransactionCase

class ModelResPartner(TransactionCase):
    def setUp(self):
        super(ModelResPartner, self).setUp()

        self.res_partner = self.env['res.partner']

    def test_model_res_partner_exists(self):
        res_partner = self.res_partner
        fields = list(res_partner.fields_get())

        # Check if our fields are in res_partner model
        self.assertIn('business_number', fields)
        self.assertIn('tax_id', fields)
        self.assertIn('is_vendor', fields)
        self.assertIn('warned_about_vat_id', fields)

    def test_set_values(self):
        # Create new test_res_partner
        test_res_partner = self.res_partner.create({
            'name': 'test name',
            'business_number': '47726890',
            'tax_id': '2024069102',
            'vat': 'SK2024069102',
            'is_vendor': True,
        })

        # Check if the values are stored
        self.assertEqual(test_res_partner.name, 'test name')
        self.assertEqual(test_res_partner.business_number, '47726890')
        self.assertEqual(test_res_partner.tax_id, '2024069102')
        self.assertEqual(test_res_partner.vat, 'SK2024069102')
        self.assertEqual(test_res_partner.is_vendor, True)
