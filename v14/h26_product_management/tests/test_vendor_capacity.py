from odoo.tests import TransactionCase
from odoo.tests.common import Form

class ModelCapacity(TransactionCase):
    def setUp(self):
        super(ModelCapacity, self).setUp()

        self.vendor_capacity = self.env['vendor.capacity']
        self.supp = self.env['res.partner'].create({'name': 'Test supplier'})
        self.suppid = self.supp.id
        self.cat = self.env['product.category'].create({
            "name": "Test Category",
        }).id
        self.cat2 = self.env['product.category'].create({
            "name": "Test Category2",
        }).id

    def test_model_vendor_capacity_exists(self):
        res_capacity = self.env['vendor.capacity']
        default_fields = list(res_capacity.fields_get())

        # Check if our fields are in vendor capacity model
        self.assertIn('supplier', default_fields)
        self.assertIn('product_category', default_fields)
        self.assertIn('capacity', default_fields)

    def test_set_value(self):

        # Create new test_capacity
        test_capacity = self.vendor_capacity.create({
            'supplier': self.suppid,
            'product_category': self.cat,
            'capacity': 100,
        })

        # Check if the values are stored
        self.assertEqual(test_capacity.supplier.id, self.suppid)
        self.assertEqual(test_capacity.product_category.id, self.cat)
        self.assertEqual(test_capacity.capacity, 100)

    def h26_ref(self, sub_xml_id):
        return self.env.ref('h26_product_management.' + sub_xml_id)

    def test_onchange_sum(self):
        mgr_user = self.env['res.users'].create({
            'login': 'mgr',
            'name': 'mgr',
            'groups_id': [(6, 0, [self.h26_ref('group_product_manager').id])],
        })   
        
        capacity1 = self.env['vendor.capacity'].create({
            'supplier': self.suppid,
            'product_category': self.cat,
            'capacity': 100,
        })
        capacity2 = self.env['vendor.capacity'].create({
            'supplier': self.suppid,
            'product_category': self.cat2,
            'capacity': 200,
        })
        capacity1 = capacity1.with_user(mgr_user)
        capacity2 = capacity2.with_user(mgr_user)
        form1 = Form(capacity1)
        form2 = Form(capacity2)
        form1.capacity = 200
        form2.capacity = 100

        # Check if the the 2 capacity sum is calculated correctly
        self.assertEqual(self.supp.total_capacity, 300)
        
