from odoo.tests import TransactionCase

class ModelPartner(TransactionCase):
    def setUp(self):
        super(ModelPartner, self).setUp()

        self.partner = self.env['res.partner']
        self.iban = self.env['res.partner.bank'].create({
            'partner_id': '1',
            'acc_number': 'SK8975000000000012345671',
        })

    def test_model_partner_exists(self):
        res_partner = self.env['res.partner']
        default_fields = list(res_partner.fields_get())

        # Check if our fields are in partner model
        self.assertIn('is_designer', default_fields)
        self.assertIn('internal_external_designer', default_fields)
        self.assertIn('designer_contract', default_fields)

    def test_set_value(self):

        # Create new test_designer
        test_designer = self.partner.create({
            'name': 'Patko Fejda',
            'is_designer': True,
            'internal_external_designer': 'internal',
            'bank_ids': self.iban,
            # 'certificate_type': self.cert,
        })

        # Check if the values are stored
        self.assertTrue(test_designer.is_designer)
        self.assertEqual(test_designer.internal_external_designer, 'internal')
