from odoo.tests import TransactionCase


class ModelProductTemplate(TransactionCase):
    def setUp(self):
        super(ModelProductTemplate, self).setUp()

        self.product_template = self.env['product.template']
        self.categ_id = self.env['product.category'].create({'name': 'Test category'}).id
        self.season = self.env['product.season'].create({'name': 'Test season'}).id
        self.primary_category = self.env['product.primary_category'].create({"name": "Test Category"}).id
        self.secondary_category_man = self.env['product.secondary_category_man'].create({"name": "Test Category"}).id
        self.secondary_category_woman = self.env['product.secondary_category_woman'].create({"name": "Test Category"}).id
        self.color = self.env['product.color'].create({"name": "Test color", 'code': 1234999}).id
        self.motive = self.env['product.motive'].create({"name": "Test motive", 'code': 1234999}).id
        self.gender_mkt = self.env['product.gender_mkt'].create({"name": "Test gender_mkt"}).id
        self.collection = self.env['product.collection'].create({"name": "Test collection", 'code': '1234999'}).id
        self.cut = self.env['product.cut'].create({"name": "Test cut"}).id
        self.shape = self.env['product.shape'].create({"name": "Test shape"}).id

    def test_model_product_template_exists(self):
        product_template = self.env['product.template']
        fields = list(product_template.fields_get())

        # Check if our fields are in product model
        self.assertIn('categ_id', fields)
        self.assertIn('master_code', fields)
        self.assertIn('company_id', fields)
        self.assertIn('name', fields)
        self.assertIn('photo_url', fields)
        self.assertIn('season', fields)
        self.assertIn('limited_edition', fields)
        self.assertIn('limited_edition_name', fields)
        self.assertIn('primary_category', fields)
        self.assertIn('secondary_category_man', fields)
        self.assertIn('secondary_category_woman', fields)
        self.assertIn('color', fields)
        self.assertIn('motive', fields)
        self.assertIn('gender_mkt', fields)
        self.assertIn('collection', fields)
        self.assertIn('cut', fields)
        self.assertIn('material', fields)
        self.assertIn('product_special_features', fields)
        self.assertIn('care', fields)
        self.assertIn('gift_box', fields)
        self.assertIn('content', fields)
        self.assertIn('shape', fields)
        self.assertIn('planned_launch_date', fields)
        self.assertIn('launch_date', fields)
        self.assertIn('delist_date', fields)
        self.assertIn('bullet_description', fields)
        self.assertIn('received', fields)

    def test_set_values(self):

        # Create new test_certificate
        test_product = self.product_template.create({
            'categ_id': self.categ_id,
            'master_code': "123456",
            'name': "Test name",
            'photo_url': "https://www.instagram.com/",
            'limited_edition': True,
            'limited_edition_name': "Test name",
            'primary_category': self.primary_category,
            'secondary_category_man': self.secondary_category_man,
            'secondary_category_woman': self.secondary_category_woman,
            'color': self.color,
            'motive': self.motive,
            'gender_mkt': [(6, 0, [self.gender_mkt])],
            'collection': self.collection,
            'cut': self.cut,
            'material': "Test material",
            'product_special_features': "Test feature",
        })

        # Check if the values are stored
        self.assertEqual(test_product.limited_edition, True)
        self.assertEqual(test_product.master_code, "123456")
        self.assertEqual(test_product.name, "Test name")
        self.assertEqual(test_product.photo_url, "https://www.instagram.com/")
        self.assertEqual(test_product.limited_edition_name, "Test name")
        self.assertEqual(test_product.material, "Test material")
        self.assertEqual(test_product.product_special_features, "Test feature")
