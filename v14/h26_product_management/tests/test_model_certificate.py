from odoo.tests import TransactionCase

class ModelCertificate(TransactionCase):
    def setUp(self):
        super(ModelCertificate, self).setUp()

        self.certificate = self.env['certificate']
        self.supp = self.env['res.partner'].create({'name': 'Test supplier'}).id
        self.cert = self.env['product.category'].create({
            "name": "Test Category",
        }).id

    def test_model_certificate_exists(self):
        res_cert = self.env['certificate']
        default_fields = list(res_cert.fields_get())

        # Check if our fields are in certificate model
        self.assertIn('certificate_number', default_fields)
        self.assertIn('supplier', default_fields)
        self.assertIn('certificate_type', default_fields)
        self.assertIn('certificate_valid', default_fields)
        self.assertIn('certificate_expiration', default_fields)
        self.assertIn('pdf_certificate', default_fields)

    def test_set_value(self):

        # Create new test_certificate
        test_certificate = self.certificate.create({
            'certificate_number': "abc123/123",
            'supplier': self.supp,
            'certificate_type': self.cert,
        })

        # Check if the values are stored
        self.assertEqual(test_certificate.certificate_number, "abc123/123")
        self.assertEqual(test_certificate.supplier.id, self.supp)
        self.assertEqual(test_certificate.certificate_type.id, self.cert)
