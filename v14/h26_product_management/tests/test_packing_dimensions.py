from odoo.tests import TransactionCase
from odoo.exceptions import ValidationError


class ModelPackingDimensions(TransactionCase):
    def setUp(self):
        super(ModelPackingDimensions, self).setUp()

        self.packing_dimensions = self.env['packing.dimensions']
        self.supplier = self.env['res.partner'].create({'name': 'Test supplier'}).id
        self.category = self.env['product.category'].create({"name": "Test Category"}).id

    def test_model_packing_dimensions_exists(self):
        packing_dimensions = self.env['packing.dimensions']
        fields = list(packing_dimensions.fields_get())

        # Check if our fields are in packing_dimensions model
        self.assertIn('supplier', fields)
        self.assertIn('product_category', fields)
        self.assertIn('box_height_cm', fields)
        self.assertIn('box_width_cm', fields)
        self.assertIn('box_lenght_cm', fields)
        self.assertIn('box_height_m', fields)
        self.assertIn('box_width_m', fields)
        self.assertIn('box_lenght_m', fields)
        self.assertIn('palette_weight_kg', fields)
        self.assertIn('palette_weight_g', fields)
        self.assertIn('pcs_per_packing', fields)
        self.assertIn('box_volume_cm3', fields)
        self.assertIn('box_volume_m3', fields)
        self.assertIn('box_weight_g', fields)
        self.assertIn('box_weight_kg', fields)
        self.assertIn('boxes_per_palette', fields)

    def test_set_values(self):
        # Create new test_packing_dimensions
        test_packing_dimensions = self.packing_dimensions.create({
            'supplier': self.supplier,
            'product_category': self.category,
            'box_height_cm': 100,
            'box_width_cm': 100,
            'box_lenght_cm': 100,
            'palette_weight_kg': 0.001,
            'pcs_per_packing': 10,
            'box_volume_cm3': 1000000,
            'box_weight_g': 1000,
            'boxes_per_palette': 10,
        })

        # Check if the values are stored
        self.assertEqual(test_packing_dimensions.supplier.id, self.supplier)
        self.assertEqual(test_packing_dimensions.product_category.id, self.category)
        self.assertEqual(test_packing_dimensions.box_height_cm, 100)
        self.assertEqual(test_packing_dimensions.box_width_cm, 100)
        self.assertEqual(test_packing_dimensions.box_lenght_cm, 100)
        self.assertEqual(test_packing_dimensions.palette_weight_kg, 0.001)
        self.assertEqual(test_packing_dimensions.pcs_per_packing, 10)
        self.assertEqual(test_packing_dimensions.box_volume_cm3, 1000000)
        self.assertEqual(test_packing_dimensions.box_weight_g, 1000)
        self.assertEqual(test_packing_dimensions.boxes_per_palette, 10)

    def test_computed_values(self):
        # Create new test_packing_dimensions
        test_packing_dimensions = self.packing_dimensions.create({
            'supplier': self.supplier,
            'product_category': self.category,
            'box_height_cm': 100,
            'box_width_cm': 100,
            'box_lenght_cm': 100,
            'palette_weight_kg': 0.001,
            'pcs_per_packing': 10,
            'box_volume_cm3': 1000000,
            'box_weight_g': 1000,
            'boxes_per_palette': 10,
        })

        # Check if the values are correct
        self.assertEqual(test_packing_dimensions.box_height_m, 1)
        self.assertEqual(test_packing_dimensions.box_width_m, 1)
        self.assertEqual(test_packing_dimensions.box_lenght_m, 1)
        self.assertEqual(test_packing_dimensions.palette_weight_g, 1)
        self.assertEqual(test_packing_dimensions.box_volume_m3, 1)
        self.assertEqual(test_packing_dimensions.box_weight_kg, 1)

    def test_check_not_negative(self):
        with self.assertRaises(ValidationError):
            # Create new test_packing_dimensions
            self.packing_dimensions.create({
                'supplier': self.supplier,
                'product_category': self.category,
                'box_height_cm': -3,
                'box_width_cm': 100,
                'box_lenght_cm': 100,
                'palette_weight_kg': 0.001,
                'pcs_per_packing': 10,
                'box_volume_cm3': 1000000,
                'box_weight_g': 1000,
                'boxes_per_palette': 10,
            })
