from odoo.tests import TransactionCase
from odoo.exceptions import AccessError


class TestProductDesign(TransactionCase):

    def h26_ref(self, sub_xml_id):
        return self.env.ref('h26_product_management.' + sub_xml_id)

    def h26_design_refs(self, sub_model, sub_xml_ids):
        records = self.env['product.' + sub_model]
        for sub_xml_id in sub_xml_ids:
            rec = self.h26_ref(sub_model + sub_xml_id)
            records |= rec
        return records

    def test_design_access_rights(self):
        other_user = self.env['res.users'].create({
            'login': 'o',
            'name': 'o',
            'groups_id': [(6, 0, [self.h26_ref('group_product_other').id])],
        })
        mkt_user = self.env['res.users'].create({
            'login': 'mkt',
            'name': 'mkt',
            'groups_id': [(6, 0, [self.h26_ref('group_product_marketer').id])],
        })
        mgr_user = self.env['res.users'].create({
            'login': 'mgr',
            'name': 'mgr',
            'groups_id': [(6, 0, [self.h26_ref('group_product_manager').id])],
        })
        model = self.env['product.design']
        # positive create
        design = model.with_user(mgr_user).create({'name': 'mgr-create3'})
        # negtive create
        with self.assertRaises(AccessError):
            design.with_user(other_user).create({'name': 'other-create1'})
        with self.assertRaises(AccessError):
            design.with_user(mkt_user).create({'name': 'mkt-create2'})
        # positive read
        model.with_user(mgr_user).read()
        model.with_user(other_user).read()
        model.with_user(mkt_user).read()
        # positive write
        design.with_user(mgr_user).write({'name': 'mgr-write1'})
        design.with_user(mkt_user).write({'name': 'mkt-write2'})
        # negtive write
        with self.assertRaises(AccessError):
            design.with_user(other_user).write({'name': 'other-write3'})
        # negative delete
        with self.assertRaises(AccessError):
            design.with_user(other_user).unlink()
        with self.assertRaises(AccessError):
            design.with_user(mkt_user).unlink()
        # positive delete
        design.with_user(mgr_user).unlink()

    def test_create_from_dict(self):
        # create values for attributes
        themes = self.h26_design_refs('theme', ['001', '002', '003'])
        tags = self.h26_design_refs('tag', ['001', '002', '003'])
        colln = self.h26_ref('colln003')
        motives = self.h26_design_refs('motive', ['001', '002', '003'])
        colors = self.h26_design_refs('color', ['007', '008', '009'])
        # create a designer
        designer = self.env['res.partner'].create({'name': 'vendor2'})
        iban = self.env['res.partner.bank'].create({
            'partner_id': designer.id,
            'acc_number': 'SK8975000000000012345671',
        })
        designer.write({
            'bank_ids': iban,
            'is_designer': True,
            'internal_external_designer': 'internal',
        })
        # create a design record and fill attributes with values
        design = self.env['product.design'].create({
            'name': 'design1',
            'designer_id': designer.id,
            'theme_ids': [(6, 0, themes.ids)],
            'tag_ids': [(6, 0, tags.ids)],
            'collection_id': colln.id,
            'motive_ids': [(6, 0, motives.ids)],
            'color_ids': [(6, 0, colors.ids)],
        })
        # check if the values are stored
        # designer attribute
        self.assertEqual(design.name, "design1")
        self.assertEqual(design.designer_id.name, 'vendor2')
        self.assertEqual(design.designer_id.is_designer, True)
        # themes attribute
        theme_names = sorted(design.theme_ids.mapped('display_name'))
        self.assertEqual(theme_names, ['abstract', 'africa', 'airplane'])
        # tags attribute
        tag_names = sorted(design.tag_ids.mapped('display_name'))
        self.assertEqual(tag_names, ['249986 Povolania', '250422 Sova', '251249 Donuty'])
        # collection attribute
        colln_name = design.collection_id.display_name
        self.assertEqual(colln_name, '236512 Dedoles')
        # motives attribute
        motive_names = sorted(design.motive_ids.mapped('display_name'))
        self.assertEqual(motive_names, ['249162 Hobby', '249163 Sviatky', '249164 Basic'])
        # colors attribute
        color_names = sorted(design.color_ids.mapped('display_name'))
        self.assertEqual(color_names, ['40149 Fialová', '40152 Bordová', '40153 Hnedá'])
