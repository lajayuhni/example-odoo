{
    'name': 'Academic User',
    'summary': """
        Academic User
        INit Base User For Academic System""",
    'version': '0.0.1',
    'category': 'academic',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Academic Base User
    """,
    'depends': [
        'base',
    ],
    'data': [
    	'security/groups.xml',
        'static/assets_loader.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}