{
    'name': 'Student',
    'summary': """
        Student
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Sisformik Student Data
    """,
    'depends': [
        'account',
        'academic_user',
        'academic_major',
        'academic_period'
    ],
    'data': [
        'security/groups.xml',
        'security/account.invoice.xml',
        'data/sequence.xml',
        'data/student.status.csv',
        'security/student.student.xml',
        'security/student.leave.xml',

        'views/student.student/form.xml',
        'views/student.student/tree.xml',
        'views/student.student/kanban.xml',
        'views/student.student/search.xml',
        'views/student.student/action.xml',
        'views/student.student/menu.xml',


        'views/student.leave/form.xml',
        'views/student.leave/tree.xml',
        'views/student.leave/kanban.xml',
        'views/student.leave/search.xml',
        'views/student.leave/action.xml',
        'views/student.leave/menu.xml',


        'views/student.status/form.xml',
        'views/student.status/tree.xml',
        'views/student.status/kanban.xml',
        'views/student.status/search.xml',
        'views/student.status/action.xml',
        'views/student.status/menu.xml',

        'views/student.leave.approval.wizard/form.xml',
        'views/student.leave.approval.wizard/action.xml',

        'views/res.partner/form.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}