# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStatus(models.Model):
	_name = 'student.status'
	_description = 'Student Status'
	_inherit = ['mail.thread', 'mail.activity.mixin']

	code = fields.Char(string="Kode", required=True, help="Kode Status", track_visibility="onchange")
	name = fields.Char(string="Judul/Title/Nama", required=True, help="Judul Status", track_visibility="onchange")
	notes = fields.Text('Catatan', help="Catatan mengenai status ini")


	active = fields.Boolean(string="Aktif", default=True, track_visibility="onchange")

	student_ids = fields.One2many('student.student','status_id', string="Mahasiswa/i")

	color = fields.Selection([
		('primary', 'Primary'),
		('success', 'Success'),
		('danger', 'Danger'),
		('warning', 'Warning'),
		('info', 'Info'),
		('light', 'Light'),
		('dark', 'Dark'),
		],string='Level Warna', help="Data Color Will Shown", default="primary", required=True)


	def name_get(self):
		res = []
		student_status_code_only = self._context.get('student_status_code_only')
		student_status_name_only = self._context.get('student_status_name_only')
		for rec in self:
			name = "[%s] %s" % (rec.code, rec.name, )
			if student_status_code_only:
				# only show code in kanban of student student
				name = rec.code
			if student_status_name_only:
				# only show code in kanban of student student
				name = rec.name
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('code', operator, name), ('name',operator,name)] + args, limit=limit)
		return recs.name_get()