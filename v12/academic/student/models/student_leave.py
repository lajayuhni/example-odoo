# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError,AccessError

import logging
_logger = logging.getLogger(__name__)


class StudentLeaveApprovalWizard(models.TransientModel):
	_name = 'student.leave.approval.wizard'
	_description = 'Wizard Approval Cuti'

	leave_id = fields.Many2one('student.leave', string="Cuti", required=True)

	currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id, ondelete="RESTRICT", onupdate="RESTRICT")
	amount_fee = fields.Monetary(string="Biaya Cuti", required=True)

	action = fields.Selection([('approve',"Setujui"), ('reject','Tolak')], required=True, string="Action")

	notes = fields.Text('Catatan')


	def btn_approve(self):
		self.ensure_one()
		self.leave_id.write(dict(amount_fee=self.amount_fee, notes=self.notes, currency_id=self.currency_id.id))
		self.leave_id.btn_approve()

	def btn_process(self):
		self.ensure_one()
		if self.action=='approve':
			self.btn_approve()
		else:
			self.btn_reject()

	def btn_reject(self):
		self.ensure_one()
		self.leave_id.btn_reject()


class StudentLeave(models.Model):
	_name = 'student.leave'
	_description = 'Cuti Mahasiswa'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nomor", required=True, track_visibility="onchange")

	def _default_student_id(self):
		if self.user_has_groups('academic_user.group_student'):
			student = self.env['student.student'].search([('partner_id','=',self.env.user.partner_id.id)])
			assert student.id!=False, "User teridentifikasi sebagai mahasiswa namun tidak dapat mendapatkan data default mahasiswa!\nHubungi Administrator!"
			return student.id

	student_id = fields.Many2one('student.student', string="Mahasiswa", required=True, track_visibility="onchange", default=_default_student_id)

	invoice_id = fields.Many2one('account.invoice', string="Tagihan")

	academic_year_id = fields.Many2one('academic.year', required=True, track_visibility="onchange", string="Tahun")
	academic_year_period_id = fields.Many2one('academic.year.period', required=True, track_visibility="onchange", string="Periode")

	active = fields.Boolean(string="Aktif", default=True)

	currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id, ondelete="RESTRICT", onupdate="RESTRICT")
	amount_fee = fields.Monetary(string="Biaya Cuti")

	reason = fields.Text(string="Alasan", required=True)
	notes = fields.Text(string="Catatan")


	state = fields.Selection([('draft','Draft'), ('submited','Submited'), ('approved','Approved'), ('done','Done'), ('rejected','Rejected')],
		help="""
			* Draft: User Create and Saving Document
			* Submited: User submited and ask permission to approve
			* Approved: Student Leave Staff approved the request then will generate invoice
			* Done: Invoice Validated
			* Rejected: Request Rejected
		""",
		default="draft")

	def btn_cancel(self):
		self.active = False

	def btn_toggle_active(self):
		self.active = not self.active

	def btn_submit(self):
		self.write({
			'state':'submited'
			})


	def _prepare_invoice_lines(self):
		self.ensure_one()
		return {
			'name': _("Biaya Cuti"),
			'sequence': 1,
			'origin': self.name,
			'price_unit': self.amount_fee,
			'quantity': 1,
			'account_id': self.env.ref('l10n_generic_coa.1_conf_a_sale').id,
		}

	def _fetch_next_seq(self):
		return self.env['ir.sequence'].sudo().next_by_code('seq.student.leave')

	@api.model
	def create(self,vals):
		if not self._context.get('name'):
			# set default name
			vals.update({'name':self._fetch_next_seq()})
		return super(StudentLeave,self).create(vals)

	def open_invoice(self):
		form = self.env.ref('account.invoice_form')
		context = self._context.copy()
		# contex.update({})
		return {
			'name': _('Invoice Cuti %s') % self.name,
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'account.invoice',
			'view_id': form.id,
			'type': 'ir.actions.act_window',
			'context': context,
			'res_id': self.invoice_id.id,
			'target': 'current',
		}

	def _prepare_invoice(self):
		self.ensure_one()
		journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
		if not journal_id:
			raise UserError(_('Please define an accounting sales journal for this company.'))
		# _logger.critical([self.env.user.email, self.env.user.partner_id])
		return {
			'name': self.name or '',
			'origin': self.name,
			'type': 'out_invoice',
			'account_id': self.env.user.partner_id.property_account_receivable_id.id,
			'partner_id': self.student_id.partner_id.id,
			'journal_id': journal_id,
			'currency_id': self.currency_id.id,
			'comment': _('Pengajuan Cuti %s') % self.name, 
			'payment_term_id': False,
			'fiscal_position_id': self.env.user.partner_id.property_account_position_id.id,
			'company_id': self.env.user.company_id.id,
			'user_id': False,
			'invoice_line_ids':[(0,0,self._prepare_invoice_lines())]
		}

	def _create_invoice(self):
		inv = self.env['account.invoice']
		for rec in self:
			inv_data = self.sudo()._prepare_invoice()
			_logger.info(("New Invoice Data", inv_data))
			# using boot for creating invoice
			new_invoice = inv.sudo().create(inv_data)
			rec.write({'invoice_id':new_invoice.id})

	def btn_open_approve_wizard(self):
		context = self._context.copy()
		action = self.env['ir.actions.act_window'].for_xml_id('student', 'action_student_leave_approval_wizard_form')
		context.update(dict(default_leave_id=self.id))
		action.update({'context':context, 'target':'new'})
		return action
			

	def btn_approve(self):
		for rec in self:
			if not rec.amount_fee:
				raise ValidationError(_('Masukan Biaya Cuti!'))
			rec.write({
				'state':'approved',
				})
			rec._create_invoice()
			rec.invoice_id.action_invoice_open()

	def _update_set_student_as_leave(self):
		leave_status = self.env.ref('student.student_status_3')
		for rec in self:
			rec.student_id.write({'status_id':leave_status.id})


	def _validate_invoice(self):
		for rec in self:
			rec.invoice_id.action_invoice_open()
			assert rec.invoice_id.state=='open', "Invoice gagal divalidasi"
			# rec.invoice_id.action_invoice_paid()
			# action = self.env['ir.actions.act_window'].with_context({'active_id':rec.invoice_id.id}).for_xml_id('account', 'action_account_invoice_payment')
			ctx = {'active_model': 'account.invoice', 'active_ids': [rec.invoice_id.id]}

			journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
			register_payments = self.env['account.register.payments'].with_context(ctx).create({
				'payment_date': fields.Date.today(),
				'journal_id': journal_id,
				'payment_method_id': self.env.ref("account.account_payment_method_manual_out").id,
			})
			register_payments.create_payments()
			assert rec.invoice_id.state=='paid', "Gagal Register Payment"

	def btn_done(self):
		if not self.user_has_groups('account.group_account_invoice'):
			raise AccessError(_("Hanya Keuangan yang dapat memvalidasi persetujuan!"))
		self._validate_invoice()
		self.sudo()._update_set_student_as_leave()
		# _logger.critical(self.invoice_id.state)
		self.write({
			'state':'done'
			})
	def btn_reject(self):
		self.write({
			'state':'rejected'
			})

	def btn_draft(self):
		self.write({
			'state':'draft'
			})