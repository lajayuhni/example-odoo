# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
	_inherit = 'res.partner'
	
	mobile2 = fields.Char(string="No HP (2)", required=False, track_visibility="onchange")
	birth_date = fields.Date(string="Tgl Lahir", required=False, track_visibility="onchange")
	birth_place = fields.Many2one('res.country.state', string="Kota Kelahiran", required=False, track_visibility="onchange")
	religion_id = fields.Many2one('master.religion', string="Agama", required=False, track_visibility="onchange")
	certificate1_file = fields.Binary(string="Sertifikat organisasi di sekolah", attachment=True)
	certificate1_file_name = fields.Char('Nama File Sertifikat Organisasi di Sekolah', track_visibility="onchange")
	certificate2_file = fields.Binary(string="Piagam Penghargaan", attachment=True)
	certificate2_file_name = fields.Char('Nama File Piagam Penghargaan', track_visibility="onchange")
	citizen_id_file = fields.Binary(string="Citizen ID File")
	citizen_id_file_name = fields.Char('Citizen ID Origin File Name', track_visibility="onchange", oldname="citizen_id_origin_filename")
	family_register_file = fields.Binary(string="Fammily Card File")
	family_register_file_name = fields.Char('Fammily Card Origin File Name', track_visibility="onchange", oldname="family_register_origin_filename")
	birth_certificate_file = fields.Binary(string="Birth Certificate File")
	birth_certificate_file_name = fields.Char('Birth Certificate Origin File Name', track_visibility="onchange", oldname="birth_certificate_origin_filename")
	father_name = fields.Char('Father Name', required=False, track_visibility="onchange")
	mother_name = fields.Char('Mother Name', required=False, track_visibility="onchange")
	certificate_file = fields.Binary(string="File Ijazah")
	certificate_file_name = fields.Char("Nama File Ijazah")