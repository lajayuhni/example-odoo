# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudent(models.Model):
	_name = 'student.student'
	_description = 'Student'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']
	_inherits = {"res.partner": "partner_id"}



	# name = fields.Char(string="Nama", required=True, track_visibility="onchange", help="Nama Lengkap")
	code = fields.Char(string="NIM", required=True, track_visibility="onchange", help="N.I.M")

	# active = fields.Boolean('Active', default=True, track_visibility="onchange")


	academic_year_id = fields.Many2one('academic.year', required=True, track_visibility="onchange", help="Angkatan Tahun Periode", domain=[('locked','=',True)])
	major_id = fields.Many2one('academic.major', string="Program Studi", required=True, ondelete="restrict", onupdate="restrict")
	status_id = fields.Many2one('student.status', string="Status", required=True, default=lambda self:self.env.ref('student.student_status_1').id)
	status_color = fields.Selection(related="status_id.color", string="Warna Status", readonly=True)
	# user_id = fields.Many2one('res.users', string="User", required=False)
	partner_id = fields.Many2one('res.partner', string="Kontak", auto_join=True, ondelete="cascade", onupdate="cascade", required=True)
	gender = fields.Selection([('male','Pria'), ('female','Wanita')], string="Jenis Kelamin", required=False, track_visibility="onchange", oldname="sex")

	_sql_constraints = [
		('code_unique', 'unique (code)', _('Code Mahasiswa Harus Unik')),
	]

	def name_get(self):
		res = []
		for rec in self:
			name = "[%s] %s" % (rec.code, rec.name, )
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('code', operator, name), ('name',operator,name)] + args, limit=limit)
		return recs.name_get()

	def _create_partner(self):
		partner_data = {
			'name':self.name,
			'ref':self.code,
		}

		partner = self.env['res.partner'].create(partner_data)
		return partner

	# @api.model
	# def create(self, vals):
	# 	# create res.partner when student created
	# 	if vals.get('user_id') and not vals.get('partner_id'):
	# 		# raise ValidationError(_('Jika User di definisikan maka Kontak Harus Diisi'))
	# 		user = self.env['res.users'].sudo().browse(vals.get('user_id'))
	# 		# set default partner id from user_id.partner_id
	# 		vals.update({'partner_id':user.partner_id.id})
	# 	sup = super(StudentStudent, self).create(vals)
	# 	# if sup.partner_id.id==False:
	# 	# 	# create partner if partner not defined
	# 	# 	partner = sup._create_partner()
	# 	# 	sup.write({'partner_id':partner.id})

	# 	return sup

	def action_view_detail_student_student(self):
		action = self.env['ir.actions.act_window'].for_xml_id('student', 'action_view_detail_student_student')
		action.update({'res_id':self.id})
		return action

	def action_open_contact(self):
		action = self.env['ir.actions.act_window'].for_xml_id('student', 'action_view_student_contact_form')
		action.update({'res_id':self.partner_id.id})
		return action