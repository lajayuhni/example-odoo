# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tests import tagged
from odoo.tests import common

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestStudent(common.TransactionCase):
	def setUp(self):
		super(TestStudent, self).setUp()
		# Prepare group
		group_0 = self.env.ref('student.group_student_user')
		group_1 = self.env.ref('student.group_student_staff')

		self.user_0 = self.env['res.users'].create({
			'name': 'Student User Test',
			'login': 'user_0',
			'email': 'studentuser@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_0.id, self.env.ref('base.group_portal').id]), self.env.ref('academic_user.group_student').id]
		})
		_logger.info("Created User0 id:%s" % self.user_0.id)


		self.user_1 = self.env['res.users'].create({
			'name': 'Unit Test 1',
			'login': 'user_1',
			'email': 'untitest@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_1.id, self.env.ref('base.group_user').id])]
		})
		_logger.info("Created User1 id:%s" % self.user_1.id)

		self.main_env = self.env['student.student']


		self.MAJOR = self.env['academic.major'].create({
			'name':'MAJOR 1',
			'description':"Major for testing purpose",
			'seq':1,
			'code':"TEM",
			'academic_title_prefix':'MJA',
			'active':True,
			'degree_id':self.env.ref('academic_degree.degree_d3').id,
			})

		self.ACADEMIC_YEAR = self.env['academic.year'].create({
			'name':'TEST ACADEMIC YEAR',
			'code':'2000',
			'education_fee':3000000,
			'admission_fee':120000,
			'extra_admission_cost':200000,
			'seq':10,
			'active':True
			})
		# _logger.critical(('Setup---'))


		# student status
		self.STUDENT_STATUSES = self.env['student.status'].create([
			dict(
				code="D-A",
				name="DEMO Aktif",
				notes="Student Active",
				active=True
			),
			dict(
				code="DNA",
				name="DEMO-NonAktif",
				notes="Student Not Active",
				active=True
			),
		])

	def test_create_student(self):
		# Bellow, is example of test create with user group that setted up in setUp()
		# _logger.critical("Create student")
		new_val = {
			'name':'TEST STUDENT',
			'code':'1900001-TEST',
			'major_id':self.MAJOR.id,
			'academic_year_id':self.ACADEMIC_YEAR.id,
			# 'user_id':self.user_0.id,
			'partner_id':self.user_0.partner_id.id,
			'status_id':self.STUDENT_STATUSES[0].id,

		}


		temp_obj = self.main_env.sudo(self.user_1).new(new_val)
		new_obj_val = temp_obj._convert_to_write({name: temp_obj[name] for name in temp_obj._cache})

		student = self.main_env.sudo(self.user_1).create(new_obj_val)
		self.STUDENT = student
		self.assertEqual(student.user_ids[0].email!=False, True)
		_logger.info(('Test Email 1',student.user_ids[0].email))


		# testing is object created
		self.assertEqual(student.id!=False,True)

		# testing is has partner_id
		self.assertEqual(student.partner_id.id!=False,True)
		# self.user_0.write({'partner_id':student.partner_id.id})

		# self.user_0.partner_id.write({'email':self.user_0.email})
		self.assertEqual(student.user_ids[0].id!=False, True)
		_logger.info(('Test Email 2',student.user_ids[0].email))
		self.assertEqual(student.user_ids[0].email!=False, True)
		self.assertEqual(student.user_ids[0].partner_id.email!=False, True)

	def test_user_cant_create_student(self):

		new_val = {
			'name':'TEST STUDENT',
			'code':'1900001',
			'major_id':self.MAJOR.id,
			'academic_year_id':self.ACADEMIC_YEAR.id
		}


		temp_obj = self.main_env.sudo(self.user_0).new(new_val)
		new_obj_val = temp_obj._convert_to_write({name: temp_obj[name] for name in temp_obj._cache})

		# must raise AccessError
		with self.assertRaises(AccessError):
			newobj = self.main_env.sudo(self.user_0).create(new_obj_val)