# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tests import tagged
from odoo.tests import common

from . import test_student

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestStudentLeave(test_student.TestStudent):
	def setUp(self):
		super(TestStudentLeave, self).setUp()
		self.ACADEMIC_YEAR.btn_lock()
		self.assertEqual(self.ACADEMIC_YEAR.locked==True, True)

		self.ACADEMIC_YEAR_PERIOD = self.ACADEMIC_YEAR.academic_year_period_ids[0]

		group_billing = self.env.ref('account.group_account_invoice')
		
		self.user_billing = self.env['res.users'].create({
			'name': 'BillingAcademic',
			'login': 'user_billing',
			'email': 'usr_test_billing@example.com',
			'signature': '--\nUnitTestingbilling',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_billing.id, self.env.ref('base.group_user').id])]
		})

	def test_student_leave_request(self):
		# Bellow, is example of test create with user group that setted up in setUp()
		# _logger.critical("Starting testing student leave request")
		self.test_create_student()
		# _logger.critical("After create student")
		new_val = {
			'student_id': self.STUDENT.id,
			'academic_year_id': self.ACADEMIC_YEAR_PERIOD.academic_year_id.id,
			'academic_year_period_id':self.ACADEMIC_YEAR_PERIOD.id,
			'reason': "Bentrok dengan kerja",
		}


		temp_obj = self.env['student.leave'].sudo(self.user_0).new(new_val)
		new_obj_val = temp_obj._convert_to_write({name: temp_obj[name] for name in temp_obj._cache})

		new_leave = self.env['student.leave'].sudo(self.user_0).create(new_obj_val)
		self.leave = new_leave

		# testing is object created
		self.assertEqual(new_leave.id!=False,True)

		# must be draft
		self.assertEqual(self.leave.state, 'draft')

		# submitting
		self.leave.sudo(self.user_0).btn_submit()
		self.assertEqual(self.leave.state, 'submited')

	def test_student_leave_reject(self):
		self.test_student_leave_request()
		self.leave.sudo(self.user_1).btn_reject()
		self.assertEqual(self.leave.state, 'rejected')

	def test_student_leave_cancel(self):
		self.test_student_leave_request()
		self.leave.sudo(self.user_0).btn_cancel()
		self.assertEqual(self.leave.active, False)

	def test_student_leave_approve_done(self):
		self.test_student_leave_request()
		# make sure amount_fee = 0
		self.assertEqual(self.leave.amount_fee, 0)

		with self.assertRaises(ValidationError):
			# cant approve direct without form wizard
			self.leave.sudo(self.user_1).btn_approve()

		wizard = self.leave.sudo(self.user_1).btn_open_approve_wizard()
		# _logger.critical(wizard)
		self.assertEqual(wizard.get('type'), 'ir.actions.act_window')
		self.assertEqual(wizard.get('res_model'), 'student.leave.approval.wizard')
		self.assertEqual(wizard.get('res_id'), 0)
		wizard_context = wizard.get('context')
		self.assertEqual(wizard_context.get('default_leave_id')!=False, True)
		

		wizard_values = {
			'leave_id':wizard_context.get('default_leave_id'),
			'amount_fee':2000000,
			'notes':"OK Approved",
			'action':'approve'
		}
		wizobj = self.env['student.leave.approval.wizard'].sudo(self.user_1).create(wizard_values)
		self.assertEqual(wizobj.id!=False, True)
		with self.assertRaises(AccessError):
			wizobj.sudo(self.user_0).btn_approve()

		wizobj.sudo(self.user_1).btn_approve()
		self.assertEqual(self.leave.invoice_id.id!=False, True)
		self.assertEqual(self.leave.state, 'approved')
		self.assertEqual(self.leave.sudo(self.user_0).invoice_id.state, 'draft')

		# try write invoice
		with self.assertRaises(AccessError):
			self.leave.invoice_id.sudo(self.user_0).write({'origin':'test'})

		with self.assertRaises(AccessError):
			# student cant done
			self.leave.sudo(self.user_0).btn_done()
			# staff user cant done
			self.leave.sudo(self.user_1).btn_done()

		self.leave.sudo(self.user_billing).btn_done()
		self.assertEqual(self.leave.invoice_id.state, 'paid')

		self.assertEqual(self.leave.invoice_id.partner_id.id==self.leave.student_id.partner_id.id, True)


