{
	'name': 'Academic Subject',
	'summary': """
		Academic Subject
		""",
	'version': '0.0.1',
	'category': 'sisformik',
	"author": "La Jayuhni Yarsyah",
	'description': """
		Academic Subject
	""",
	'depends': [
		'academic_major',
	],
	'data': [
		'data/sequence.xml',
		
		'views/search.xml',
		'views/kanban.xml',
		'views/tree.xml',
		'views/form.xml',

		'views/academic.major.xml',
		
		'security/group.xml',
		'security/ir.model.access.xml',
		'actions/action.xml',
	],
	'installable': True,
	'auto_install': False,
	'application': True    
}