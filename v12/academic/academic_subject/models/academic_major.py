# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicMajor(models.Model):
	_inherit = 'academic.major'

	subject_ids = fields.One2many('academic.subject', 'major_id', string="Mata Kuliah Terkait")

	def open_subject_ids(self):
		action = self.env['ir.actions.act_window'].for_xml_id('academic_subject', 'action_academic_subject')
		action.update({'domain':[('major_id','=',self.id)]})
		return action