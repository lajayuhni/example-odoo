# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicSubject(models.Model):
	_name = 'academic.subject'
	_description = 'Academic Subject'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange", translate=True)

	unique_no = fields.Char(string="Kode Unik", track_visibility="onchange", required=True)

	code = fields.Char(string="Kode",  required=True, track_visibility="onchange")

	start_on_semester = fields.Integer('Dimulai pada semester', required=True, track_visibility="onchange")
	credit = fields.Integer('SKS', required=True, default=2)
	subject_dependency_ids = fields.Many2many('academic.subject', 'subject_dependency_academic_subject_rel', 'subject_dependency_id', 'academic_subject_id', string="MTK Prasyarat")


	required_subject_id = fields.Many2one('academic.subject', string="MTK Include", track_visibility="onchange")
	required_subject_ids = fields.One2many('academic.subject', 'required_subject_id', string="Dimasukkan kedalam MTK")

	practice = fields.Boolean(string="MTK Praktikum", default=False, track_visibility="onchange")
	major_id = fields.Many2one('academic.major', string="Program Studi", track_visibility="onchange")
	active = fields.Boolean('Active',default=True)


	def _get_next_seq(self):
		return self.env['ir.sequence'].sudo().next_by_code('seq.academic.subject')
	
	@api.model
	def create(self, vals):
		if not vals.get('unique_no'):
			vals.update({'unique_no':self._get_next_seq()})
		sup = super(AcademicSubject, self).create(vals)
		return sup


	def toggle_practice(self):
		self.ensure_one()
		self.practice = not self.practice

	def name_get(self):
		res = []
		for rec in self:
			name = "[%s] %s" % (rec.code, rec.name, )
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('code', operator, name), ('name',operator,name)] + args, limit=limit)
		return recs.name_get()