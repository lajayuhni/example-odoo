# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class ResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'
	
	director_name = fields.Char(string='Nama Direktur')
	subdirector1_name = fields.Char(string='Nama Pudir 1')
	subdirector2_name = fields.Char(string='Nama Pudir 2')
	subdirector3_name = fields.Char(string='Nama Pudir 3')


	@api.multi 
	def set_values(self):
		res = super(ResConfigSettings, self).set_values()
		param = self.env['ir.config_parameter'].sudo()
		param.set_param('student_director.director_name', self.director_name)
		param.set_param('student_director.subdirector1_name', self.subdirector1_name)
		param.set_param('student_director.subdirector2_name', self.subdirector2_name)
		param.set_param('student_director.subdirector3_name', self.subdirector3_name)
		return res

	@api.model
	def get_values(self):
		res = super(ResConfigSettings, self).get_values()
		params = self.env['ir.config_parameter'].sudo()
		res.update(
			director_name=params.get_param('student_director.director_name'),
			subdirector1_name=params.get_param('student_director.subdirector1_name'),
			subdirector2_name=params.get_param('student_director.subdirector2_name'),
			subdirector3_name=params.get_param('student_director.subdirector3_name'),
		)
		return res