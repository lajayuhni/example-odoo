{
    'name': 'Student Director',
    'summary': """
        Student Director
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Student Director
    """,
    'depends': [
        'live_poltekapp','student_final_task'
    ],
    'data': [
        'data/data.xml',
        'templates/student_transcript.xml',
        'templates/student_leave.xml',
        'views/res_config_settings.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}