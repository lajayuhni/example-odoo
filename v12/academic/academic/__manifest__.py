{
    'name': 'Academic Module',
    'summary': """
        Academic Module
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Academic module only to fill all required module on basic function
    """,
    'depends': [
        'academic_activity', 'academic_facility', 'academic_major',
        'academic_time_table_series','academic_subject',
        'student',
    ],
    'data': [
        'views/menu.xml',
        'views/inherited_menus.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}