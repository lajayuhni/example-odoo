# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tests import tagged
from odoo.tests import common

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestStudentStudyLineDirectImport(common.TransactionCase):
	def setUp(self):
		super(TestStudentStudyLineDirectImport, self).setUp()
		# Prepare group
		group_staff = self.env.ref('student_study.group_student_study_staff')

		self.user_staff = self.env['res.users'].create({
			'name': 'Unit Test staff',
			'login': 'user_staff',
			'email': 'user_staff@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_staff.id, self.env.ref('base.group_user').id])]
		})

		self.academic_year_period = self.env.ref('academic_period.academic_year_period_2017_ganjil')

	def test_01_import(self):
		# Bellow, is example of test create with user group that setted up in setUp()

		new_vals = [{
			"student_id": self.env.ref("demo_student.student_170301604").id,
			"subject_id": self.env.ref("demo_lecturer.academic_subject_1050").id,
			"lecturer_id": self.env.ref("demo_lecturer.demo_lecturer11").id,
			"academic_year_period_id": self.academic_year_period.id,
			"score_attendance": 0,
			"score_task": 70,
			"score_mid_exam": 70,
			"score_final_exam": 78,
			"facility_id": self.env.ref("demo_lecturer.facility_31").id,
		},
		{
			"student_id": self.env.ref("demo_student.student_170301604").id,
			"subject_id": self.env.ref("demo_lecturer.academic_subject_1053").id,
			"lecturer_id": self.env.ref("demo_lecturer.demo_lecturer11").id,
			"academic_year_period_id": self.academic_year_period.id,
			"score_attendance": 0,
			"score_task": 70,
			"score_mid_exam": 75,
			"score_final_exam": 78,
			"facility_id": self.env.ref("demo_lecturer.facility_60").id,
		},
		{
			"student_id": self.env.ref("demo_student.student_170301604").id,
			"subject_id": self.env.ref("demo_lecturer.academic_subject_1048").id,
			"lecturer_id": self.env.ref("demo_lecturer.demo_lecturer94").id,
			"academic_year_period_id": self.academic_year_period.id,
			"score_attendance": 0,
			"score_task": 80,
			"score_mid_exam": 60,
			"score_final_exam": 68,
			"facility_id": self.env.ref("demo_lecturer.facility_48").id,
		},
		{
			"student_id": self.env.ref("demo_student.student_170301604").id,
			"subject_id": self.env.ref("demo_lecturer.academic_subject_1065").id,
			"lecturer_id": self.env.ref("demo_lecturer.demo_lecturer89").id,
			"academic_year_period_id": self.academic_year_period.id,
			"score_attendance": 0,
			"score_task": 80,
			"score_mid_exam": 80,
			"score_final_exam": 79,
			"facility_id": self.env.ref("demo_lecturer.facility_34").id,
		}]


		

		newobjs = self.env['student.study.line'].sudo(self.user_staff).create(new_vals)


		# testing is object created
		_logger.critical((newobjs.mapped(lambda r:{'r.id':r.id,'r.student_id':r.student_id.display_name, 'mtk':r.subject_id.display_name, 'lecturer':r.lecturer_id.display_name, 'score_task':r.score_task, 'score_subtotal':r.score_subtotal,'score_grade':r.score_grade})))
		self.assertEqual(len(newobjs)==len(new_vals),True)
		self.assertTrue(all(newobjs.mapped(lambda r:r.id!=False)))

		self.assertTrue(all(newobjs.mapped(lambda r:r.score_grade not in [None,False])))
