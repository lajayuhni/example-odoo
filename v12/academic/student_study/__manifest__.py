{
    'name': 'Student Study',
    'summary': """
        Student Study
        KRS, KST, KHS""",
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        -KRS
        -KHS
        -IPS
    """,
    'depends': [
        'student','lecturer', 'academic',
    ],
    'data': [
        'data/sequence.xml',
    	'security/academic.score.index.xml',
    	'data/academic.score.index.csv',

    	'security/student.study.xml',
    	'security/student.study.line.xml',

    	'views/academic.score.index/action.xml',
    	'views/academic.score.index/kanban.xml',
        'views/academic.score.index/form.xml',

        'views/student.study/search.xml',
        'views/student.study/tree.xml',
        'views/student.study/form.xml',
        'views/student.study/action.xml',
        'views/student.study/menu.xml',


        'views/student.study.line/kanban.xml',
        'views/student.study.line/tree.xml',
        'views/student.study.line/form.xml',
        'views/student.study.line/search.xml',

        'views/student.study.line/action.xml',
        'views/student.study.line/menu.xml',


        'views/student.student/form.xml',

        'security/student.student.xml',
        'security/lecturer.lecturer.xml',

        'views/academic.year.period/form.xml',


        'views/student.subject.study.summary/tree.xml',
        'views/student.subject.study.summary/form.xml',
        'views/student.subject.study.summary/kanban.xml',
        'views/student.subject.study.summary/action.xml',
        'security/student.subject.study.summary.xml',

        'views/student.achievement.index.summary/tree.xml',
        'views/student.achievement.index.summary/form.xml',
        'views/student.achievement.index.summary/kanban.xml',
        'views/student.achievement.index.summary/action.xml',
        'security/student.achievement.index.summary.xml',

        'security/student.achievement.index.semester.xml',
        'views/student.achievement.index.semester/search.xml',
        'views/student.achievement.index.semester/tree.xml',
        'views/student.achievement.index.semester/kanban.xml',
        'views/student.achievement.index.semester/action.xml',


        'views/menus.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}