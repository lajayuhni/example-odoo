# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


def check_range_overlapped(range1: range,range2: range):
	xs = set(range1)
	xs.intersection(range2)
	return xs.intersection(y)

class AcademicScoreIndex(models.Model):
	_name = 'academic.score.index'
	_description = 'Score Index'

	_rec_name = 'score_grade'

	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	score_grade = fields.Char(string="Score Grade", required=True, track_visibility="onchange")
	score_index = fields.Integer(string="Score Index", required=True, size=1, track_visibility="onchange")
	min_score = fields.Integer(string="Nilai Minimum", required=True, track_visibility="onchange")
	max_score = fields.Integer(string="Nilai Maksimal", required=True, track_visibility="onchange")

	active = fields.Boolean('Active', default=True)


	# @api.constrains('min_score','max_score')
	# def constrains_min_max_score(self):
	# 	alls = self.search([
	# 		('active','=',True)
	# 	])
	# 	for rec in self:
	# 		# where range overlapped
			
	# 		all_to_check = alls.filtered(lambda r:r.id!=rec.id)
	# 		for f in all_to_check:
	# 			intersection = check_range_overlapped(range(rec.min_score,rec.max_score+1), range(f.min_score, f.max_score+1))
	# 			if len(intersection):
	# 				raise ValidationError(_('Nilai Min/Max bentrok dengan %s' % (f.display_name,)))


	def name_get(self):
		res = []
		for rec in self:
			name = "%s" % (rec.score_index)
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('score_index', operator, name), ('score_grade',operator, name)] + args, limit=limit)
		return recs.name_get()