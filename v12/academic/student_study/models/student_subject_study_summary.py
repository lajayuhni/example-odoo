# -*- coding: utf-8 -*-
from odoo import models, fields, api, _,tools
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentSubjectStudySummary(models.Model):
	_name = 'student.subject.study.summary'
	_description = 'Student Subject Study Summary'


	# uncomment if using tracking modules
	#_inherit = ['mail.thread', 'mail.activity.mixin']

	_auto = False

	student_study_line_id = fields.Many2one('student.study.line', string="Krs Item", readonly=True)

	student_id = fields.Many2one('student.student', string="Mhs", readonly=True)
	subject_id = fields.Many2one('academic.subject', string="Subject", readonly=True)


	# START RELATED

	student_study_id = fields.Many2one('student.study', string="NO KRS", ondelete="cascade", onupdate="cascade", related="student_study_line_id.student_study_id")
	
	academic_year_period_id = fields.Many2one('academic.year.period',  string="Periode", ondelete="restrict", onupdate="restrict", related="student_study_line_id.academic_year_period_id")

	lecturer_schedule_plan_id = fields.Many2one('lecturer.schedule.plan', string="Ref Jadwal Dosen", required=False, ondelete="restrict", onupdate="restrict", related="student_study_line_id.lecturer_schedule_plan_id")
	lecturer_schedule_plan_line_id = fields.Many2one('lecturer.schedule.plan.line', string="Jadwal Kuliah", ondelete="restrict", onupdate="restrict", related="student_study_line_id.lecturer_schedule_plan_line_id")
	
	
	
	# major_class_id = fields.Many2one('edu.major.class', string="Class", related="lecture_schedule_id.class_code_id", readonly=True, required=False, related="student_study_line_id.major_class_id")
	lecturer_id = fields.Many2one('lecturer.lecturer', string="Lecturer", readonly=True, related="student_study_line_id.lecturer_id")

	credits = fields.Integer("Credits", readonly=True, related="student_study_line_id.credits")


	score_composition_attendance = fields.Integer('Attendance Scoring Comp (%)', readonly=True, default=0, related="student_study_line_id.score_composition_attendance")
	score_composition_task = fields.Integer('Task Scoring Comp (%)', readonly=True, default=20, related="student_study_line_id.score_composition_task")
	score_composition_mid_exams = fields.Integer('Mid Exam Scoring Comp (%)', readonly=True, default=35, related="student_study_line_id.score_composition_mid_exams")
	score_composition_final_exams = fields.Integer('Final Exam Scoring Comp (%)', readonly=True, default=45, related="student_study_line_id.score_composition_final_exams")


	score_attendance =  fields.Integer('Attendance Score', readonly=True, related="student_study_line_id.score_attendance")
	score_task = fields.Integer('Task Score', readonly=True, related="student_study_line_id.score_task")
	score_mid_exam = fields.Integer('Mid Exam Score', readonly=True, related="student_study_line_id.score_mid_exam")
	score_final_exam = fields.Integer('End Exam Score', readonly=True, related="student_study_line_id.score_final_exam")


	score_proportion_attendance =  fields.Integer('Attendance Score', related="student_study_line_id.score_proportion_attendance")
	score_proportion_task = fields.Integer('Task Score', related="student_study_line_id.score_proportion_task")
	score_proportion_mid_exams = fields.Integer('Mid Exam Score', related="student_study_line_id.score_proportion_mid_exams")
	score_proportion_final_exams = fields.Integer('End Exam Score', related="student_study_line_id.score_proportion_final_exams")

	score_subtotal = fields.Integer("Subtotal", related="student_study_line_id.score_subtotal")

	score_grade = fields.Char(string="Grade", related="student_study_line_id.score_grade")
	score_grade_forced = fields.Char(string="Grade(Forced)", help="Only for Syncing purpose only", related="student_study_line_id.score_grade_forced")

	score_index_id = fields.Many2one('academic.score.index', string="Score Index", related="student_study_line_id.score_index_id")

	fix_score_index_id = fields.Many2one('academic.score.index', string="Score Index", related="student_study_line_id.fix_score_index_id")

	ref = fields.Char('Ref', related="student_study_line_id.ref")

	state = fields.Selection([('draft','draft'), ('submited','submited'), ('done','done'), ('canceled','Canceled')], string="State", default="draft", help="""
		* Draft -> When Krs Set
		* Submited -> KRS Submited, in this stage study card score will input
		* Done -> Done
		""", related="student_study_line_id.state")

	
	def _select(self):
		query = """
			SELECT
				MAX(ssl.id) AS id
				,MAX(ssl.id) AS student_study_line_id
				,ssl.student_id
				,ssl.subject_id
				,SUM((asi.score_index*ssl.credits)) as total_score_index
				,SUM(ssl.credits) as total_credit
		"""
		return query
	
	def _from(self):
		query = """
			student_study_line AS ssl
			JOIN academic_score_index AS asi ON asi.id = ssl.fix_score_index_id
			
		"""
		return query
	
	
	@api.model_cr
	def init(self):
		tools.drop_view_if_exists(self.env.cr, self._table)

		# fixed where wild card in live_poltekapp
		self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			FROM ( %s )
			GROUP BY
				ssl.student_id,
				ssl.subject_id
			)""" % (self._table, self._select(), self._from()))