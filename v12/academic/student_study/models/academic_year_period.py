# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError
from datetime import date,datetime
import logging
_logger = logging.getLogger(__name__)


class AcademicYearPeriod(models.Model):
	_inherit = 'academic.year.period'

	is_active_period = fields.Boolean(string="Periode Berjalan", default=False, compute="_compute_period")
	passed_period = fields.Boolean(string="Periode Sudah Berlalu", default=False, compute="_compute_period")

	def _compute_period(self):
		today = date.today()

		for rec in self.sorted('id', reverse=True):
			
			all_starts = rec.academic_schedule_ids.mapped(lambda r:r.start_on)
			all_ends = rec.academic_schedule_ids.mapped(lambda r:r.end_on)

			schedules = rec.academic_schedule_ids.filtered(lambda r:r.state == 'posted' and today>=min(all_starts) and today<=max(all_ends))
			if len(schedules):
				rec.update({
					'is_active_period':True,
					'passed_period':False
					})
			else:

				passed_period = False
				schedule_passed = rec.academic_schedule_ids.mapped(lambda r:r.state in ['posted'] and (today>r.start_on and today>r.end_on))
				if all(schedule_passed):
					passed_period = True
				
				rec.update({
					'is_active_period':False,
					'passed_period':passed_period
					})
			