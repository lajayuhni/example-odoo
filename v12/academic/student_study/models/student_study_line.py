# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudyLine(models.Model):
	_name = 'student.study.line'
	_description = 'Item KRS'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	student_study_id = fields.Many2one('student.study', string="NO KRS", track_visibility="onchange", ondelete="cascade", onupdate="cascade")
	student_id = fields.Many2one('student.student', readonly=True, string="Nim/MHS")
	# academic_year_period_id = fields.Many2one('academic.year.period',  string="Periode", ondelete="restrict", onupdate="restrict", compute="_compute_academic_period", store=True)
	academic_year_period_id = fields.Many2one('academic.year.period',  string="Periode", ondelete="restrict", onupdate="restrict")

	lecturer_schedule_plan_id = fields.Many2one('lecturer.schedule.plan', string="Ref Jadwal Dosen", required=False, ondelete="restrict", onupdate="restrict")
	lecturer_schedule_plan_line_id = fields.Many2one('lecturer.schedule.plan.line', string="Jadwal Kuliah", ondelete="restrict", onupdate="restrict")
	
	subject_id = fields.Many2one('academic.subject', string="Subject", readonly=True, required=True)
	
	# major_class_id = fields.Many2one('edu.major.class', string="Class", related="lecture_schedule_id.class_code_id", readonly=True, required=False)
	lecturer_id = fields.Many2one('lecturer.lecturer', string="Lecturer", readonly=True)

	credits = fields.Integer("Credits", store=True, readonly=True)


	score_composition_attendance = fields.Integer('Attendance Scoring Comp (%)', required=True, default=0, track_visibility="onchange")
	score_composition_task = fields.Integer('Task Scoring Comp (%)', required=True, default=20, track_visibility="onchange")
	score_composition_mid_exams = fields.Integer('Mid Exam Scoring Comp (%)', required=True, default=35, track_visibility="onchange")
	score_composition_final_exams = fields.Integer('Final Exam Scoring Comp (%)', required=True, default=45, track_visibility="onchange")


	score_attendance =  fields.Integer('Attendance Score', size=3, required=True, track_visibility="onchange")
	score_task = fields.Integer('Task Score', size=3, required=True, track_visibility="onchange")
	score_mid_exam = fields.Integer('Mid Exam Score', size=3, required=True, track_visibility="onchange")
	score_final_exam = fields.Integer('End Exam Score', size=3, required=True, track_visibility="onchange")


	score_proportion_attendance =  fields.Integer('Attendance Score', size=3, compute="_compute_score_proportions")
	score_proportion_task = fields.Integer('Task Score', size=3, compute="_compute_score_proportions")
	score_proportion_mid_exams = fields.Integer('Mid Exam Score', size=3, compute="_compute_score_proportions")
	score_proportion_final_exams = fields.Integer('End Exam Score', size=3, compute="_compute_score_proportions")

	score_subtotal = fields.Integer("Subtotal", size=3, compute="_compute_scores", store=True, track_visibility="onchange")

	score_grade = fields.Char(string="Grade", compute="_compute_scores", store=True)
	score_grade_forced = fields.Char(string="Grade(Forced)", help="Only for Syncing purpose only")

	score_index_id = fields.Many2one('academic.score.index', string="Score Index", compute="_compute_scores", store=True)

	fix_score_index_id = fields.Many2one('academic.score.index', string="Score Index(fix)", compute="_compute_fix_score_index", store=True)

	ref = fields.Char('Ref')

	state = fields.Selection([('draft','draft'), ('submited','submited'), ('done','done'), ('canceled','Canceled')], string="State", default="draft", help="""
		* Draft -> When Krs Set
		* Submited -> KRS Submited, in this stage study card score will input
		* Done -> Done
		""", track_visibility="onchange")

	# state = fields.Selection([('draft','Draft'), ('submited','Submited'), ('done','Done')])


	# def name_get(self):
	# 	res = []
	# 	for rec in self:
	# 		name = "%s - %s" % (rec.subject_id.display_name, rec.lecturer_id.display_name, )
	# 		res += [(rec.id, name)]	
	# 	return res
	
	# @api.model
	# def name_search(self, name, args=None, operator='ilike', limit=100):
	# 	args = args or []
	# 	connector = '|'
	# 	recs = self.search([connector, ('subject_id', operator, name), ('name',operator,name)] + args, limit=limit)
	# 	return recs.name_get()


	def _inverse_allowed(self):
		return True


	@api.depends('student_study_id')
	def _compute_academic_period(self):
		query = """
		UPDATE student_study_line 
		SET 
			academic_period_id = (
				SELECT sc.academic_period_id FROM student_study AS sc WHERE sc.id = student_study_line.student_study_id 
			),
			semester_type = (
				SELECT sc.semester_type FROM student_study AS sc WHERE sc.id = student_study_line.student_study_id 
			)
		WHERE id in (%s);
		""" % (",".join(map(str, self.ids)))
		_logger.info('Updating Study Card Line - Academic Period of %s' % self.ids)
		self.env.cr.execute(query)




	"""Compute Score Proportion
	
	Compute Score Proportion
	Proportion = {score_}% * ({lecture_schedule_id.score_composition_}%/100)
	% = fields
	"""
	@api.depends('score_task','score_attendance', 'score_mid_exam', 'score_final_exam', 'score_composition_attendance','score_composition_task','score_composition_mid_exams','score_composition_final_exams')
	def _compute_score_proportions(self):
		for rec in self:
			score_proportion_attendance = rec.score_attendance * (rec.score_composition_attendance/100.0) if rec.score_composition_attendance else 0.0
			score_proportion_task = rec.score_task * (rec.score_composition_task/100.0) if rec.score_composition_task else 0.0
			score_proportion_mid_exams = rec.score_mid_exam * (rec.score_composition_mid_exams/100.0) if rec.score_composition_mid_exams else 0.0
			score_proportion_final_exams = rec.score_final_exam * (rec.score_composition_final_exams/100.0) if rec.score_composition_final_exams else 0.0

			rec.update({
				'score_proportion_attendance':score_proportion_attendance,
				'score_proportion_task':score_proportion_task,
				'score_proportion_mid_exams':score_proportion_mid_exams,
				'score_proportion_final_exams':score_proportion_final_exams,
				})



	def _count_score_grade(self, score_subtotal=0.0):
		all_score_index = self.env['academic.score.index'].search([('active','=',True)])

		# find score index
		found = all_score_index.filtered(lambda r:r.min_score<=score_subtotal and r.max_score>=score_subtotal)

		if len(found):
			grade = found.score_grade
			return grade

	@api.depends('score_grade_forced','score_proportion_attendance','score_proportion_task', 'score_proportion_mid_exams', 'score_proportion_final_exams')
	def _compute_scores(self):
		score_indexes = self.env['academic.score.index'].search([('active','=',True)])
		# mapped_score_index = score_indexes.mapped(lambda r:{r.score_grade:r.score_index})
		for rec in self:
			if rec.score_grade:
				score_subtotal = 0
				score_grade = rec.score_grade
				score_index_id = score_indexes.filtered(lambda r:r.score_grade==score_grade).id
			else:
				tocompute = [
					rec.score_proportion_attendance, 
					rec.score_proportion_task, 
					rec.score_proportion_mid_exams, 
					rec.score_proportion_final_exams
				]
				_logger.info((tocompute, rec.student_id.display_name, rec.subject_id.display_name, rec))
				score_subtotal = round(sum(tocompute),0)

				score_grade = rec._count_score_grade(score_subtotal)
				score_index = score_indexes.filtered(lambda r:r.score_grade==score_grade)
				# _logger.warning(score_index.display_name)
			rec.update({
				'score_subtotal':int(round(score_subtotal,0)),
				'score_grade':score_grade,
				'score_index_id': score_index.id
				})

	@api.depends('score_grade_forced', 'score_grade')
	def _compute_fix_score_index(self):
		score_indexes = self.env['academic.score.index'].search([('active','=',True)])
		for rec in self:
			# _logger.critical(('_compute_fix_score_index', rec.score_grade_forced))
			if rec.score_grade_forced!=False:
				score_index = score_indexes.filtered(lambda r:r.score_grade==rec.score_grade_forced)
				# _logger.critical('Forceddddd')
			else:
				score_index = rec.score_index_id
			# _logger.warning((score_index))

			rec.fix_score_index_id = score_index.id