from . import academic_score_index
from . import student_study
from . import student_study_line

from . import student_student
from . import lecturer_lecturer
from . import academic_year_period


from . import student_achievement_index_semester
from . import student_subject_study_summary
from . import student_achievement_index_summary