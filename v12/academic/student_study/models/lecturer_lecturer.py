# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class LecturerLecturer(models.Model):
	_inherit = 'lecturer.lecturer'

	student_study_line_ids = fields.One2many('student.study.line', 'lecturer_id', string="Nilai")