# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudy(models.Model):
	_name = 'student.study'
	_description = 'KRS'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	def _default_study_card_name(self):
		if not self._context.get('no_default_name'):
			return self.env['ir.sequence'].next_by_code('seq.student_study')

	name = fields.Char(string="No KRS", required=True, track_visibility="onchange", default=_default_study_card_name)
	student_id = fields.Many2one('student.student', string="NIM/Mahasiswa", required=True, ondelete="restrict", onupdate="restrict")

	academic_year_period_id = fields.Many2one('academic.year.period', required=True, string="Periode", track_visibility="onchange", ondelete="restrict", onupdate="restrict")

	line_ids = fields.One2many('student.study.line', 'student_study_id', string="Item KRS")

	allocated_credit = fields.Float(string="Jatah SKS", digits=(1,2), required=True, track_visibility="onchange")

	submit_on = fields.Datetime(string="Posted Pada")
	state = fields.Selection([('draft', 'Draft'), ('done', 'Posted'), ('canceled', 'Canceled')], string="Status", required=True, default="draft")

	total_credits = fields.Integer('Total SKS', compute="_compute_total_credits", store=True)
	# selected_lecture_schedule_ids = fields.Many2many('lecture.schedule', 'study_card_lecture_schedule_rel', 'study_card_id', 'lecture_schedule_id', string="Selected Schedule")

	# @api.depends('student_id')
	# def _compute_user(self):
	# 	for rec in self:
	# 		rec.user_id = rec.student_id.user_id.id

	@api.depends('line_ids.credits','student_id')
	def _compute_total_credits(self):
		for rec in self:
			rec.total_credits = sum(rec.line_ids.mapped("credits"))

	# @api.constrains('student_id','academic_period_id','semester_type')
	# def _constrains_study_card(self):
	# 	for rec in self:
	# 		
	_sql_constraints = [
		('student_id_academic_year_period_id_unique', 'unique (student_id,academic_year_period_id)', 'Duplicate Data with constraint (Student, Academic Period and Semester Type) are not Allowed!'),
	]

	# @api.model
	# def create(self, vals):
	# 	if vals.get('name') and self._context.get('no_default_name'):
			
	# 	return super(StudentStudy, self).create(vals)


	def open_available_schedule(self):
		self.ensure_one()
		action = self.env['ir.actions.act_window'].for_xml_id('module', 'id')
		action.update({'domain':[('id','in',res_ids)]})
		return action