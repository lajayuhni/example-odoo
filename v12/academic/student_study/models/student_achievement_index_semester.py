# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError,ValidationError
from odoo.addons import decimal_precision as dp

import logging
_logger = logging.getLogger(__name__)


class StudentAchievementIndexSemester(models.Model):
	_name = 'student.achievement.index.semester'
	_description = 'Student Achievement Index Semester (IPS)'


	# uncomment if using tracking modules
	# _inherit = ['mail.thread', 'mail.activity.mixin']

	student_id = fields.Many2one('student.student', string="MHS", readonly=True)
	academic_year_period_id = fields.Many2one('academic.year.period', string="Period", readonly=True)
	total_score_index = fields.Integer(string="Total Index", readonly=True)
	total_credit = fields.Integer(string="Total Kredit", readonly=True)

	index_avg = fields.Float(string="IPS", digits=(1,2), readonly=True)

	_auto = False
	
	def _select(self):
		query = """
			SELECT
				mm.id
				,mm.academic_year_period_id
				,mm.student_id
				,mm.total_score_index
				,mm.total_credit
				,(mm.total_score_index::decimal/mm.total_credit::decimal) AS index_avg
		"""
		return query
	
	def _from(self):
		query = """
			SELECT
				CONCAT('IPS',LPAD(ssl.student_id::text, 5, '0'), LPAD(ssl.academic_year_period_id::text, 5, '0')) as id
				,ssl.academic_year_period_id
				,ssl.student_id
				,SUM((asi.score_index*ssl.credits)) as total_score_index
				,SUM(ssl.credits) as total_credit
				
			FROM student_study_line AS ssl
			JOIN academic_score_index AS asi ON asi.id = ssl.fix_score_index_id

			GROUP BY
				ssl.academic_year_period_id
				,ssl.student_id
		"""
		return query
	
	
	@api.model_cr
	def init(self):
		tools.drop_view_if_exists(self.env.cr, self._table)
		self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			FROM ( %s ) AS mm

			
			)""" % (self._table, self._select(), self._from()))