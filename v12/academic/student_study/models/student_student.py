# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudent(models.Model):
	_inherit = 'student.student'

	student_study_line_ids = fields.One2many('student.study.line', 'student_id', string="History Nilai")
	current_semester = fields.Integer(string="Semester", default=0, compute="_compute_period")

	krs_enabled = fields.Boolean(string="Krs Dikunci", default=False)

	@api.multi
	def open_related_student_achievement_index_semester(self):
		action = self.env['ir.actions.act_window'].for_xml_id('student_study', 'action_student_achievement_index_semester')
		action.update({'domain':[('student_id','=', self.id)]})
		return action


	@api.multi
	def open_history_student_subject_study_summary(self):
		action = self.env['ir.actions.act_window'].for_xml_id('student_study', 'action_student_subject_study_summary')
		action.update({'domain':[('student_id','in',self.ids)]})
		return action

	def check_no_overdue_invoice(self):
		for rec in self:
			canceled_invoices = self.invoice_ids.filtered(lambda r:r.state in ['cancel'])
			if not len(self.invoice_ids):
				raise ValidationError("Belum ada invoice %s!" % rec.display_name)
			else:
				not_paid = self.invoice_ids.filtered(lambda r:r.state not in ['open','paid','done','cancel'])
				if len(not_paid):
					raise ValidationError("%s Terdapat Tunggakan yang belum dibayarkan!" % rec.display_name)

	def btn_enabled_krs(self):
		self.check_no_overdue_invoice()
		self.write({'krs_enabled':True})

	def btn_disable_krs(self):
		self.write({'krs_enabled':False})

	@api.depends('academic_year_id')
	def _compute_period(self):
		# fixme
		for rec in self:
			# periods = self.env['academic.year.period'].search([('active','=',True),('academic_year_id','>=',rec.academic_year_id.id)])
			# active_period = periods.filtered(lambda r:r.is_active_period==True)
			# passed_period = periods.filtered(lambda r:r.passed_period==True)
			current_semester = 1

			# if len(passed_period):
			# 	current_semester = len(passed_period)+1

			if '2015' in rec.academic_year_id.name:
				start_semester = 2015
				current_year = 2019
				current_semester = (current_year-start_semester) * 2
			elif '2016' in rec.academic_year_id.name:
				start_semester = 2016
				current_year = 2019
				current_semester = (current_year-start_semester) * 2
			elif '2017' in rec.academic_year_id.name:
				
				start_semester = 2017
				current_year = 2019
				current_semester = (current_year-start_semester) * 2
				_logger.critical((current_year, start_semester))
			elif '2018' in rec.academic_year_id.name:
				start_semester = 2018
				current_year = 2019
				current_semester = (current_year-start_semester) * 2
			_logger.critical(rec.academic_year_id.name)


			rec.update({
				'current_semester':current_semester
				})

	def open_student_study_line_ids(self):
		self.ensure_one()
		action = self.env['ir.actions.act_window'].for_xml_id('student_study', 'action_open_student_study_line_history')
		action.update({'domain':[('student_id','in',self.ids)]})
		return action