# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)

# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError,ValidationError
from odoo.addons import decimal_precision as dp

import logging
_logger = logging.getLogger(__name__)




class StudentAchievementIndexSummary(models.Model):
	_name = 'student.achievement.index.summary'
	_description = 'Student Achievement Index Summary (IPK)'


	# uncomment if using tracking modules
	# _inherit = ['mail.thread', 'mail.activity.mixin']

	student_id = fields.Many2one('student.student', string="MHS", readonly=True)
	
	total_score_index = fields.Integer(string="Sum Index", readonly=True)
	total_credit = fields.Integer(string="Total Kredit", readonly=True)
	index_score = fields.Float(string="IPK", readonly=True, digits=(1,2))

	_auto = False
	
	def _select(self):
		query = """

			SELECT
				mm.student_id as id
				,mm.student_id
				,mm.total_score_index
				,mm.total_credit
				,(mm.total_score_index::decimal/mm.total_credit::decimal) AS index_score

		"""
		return query
	
	def _from(self):
		query = """
				SELECT
					ssts.student_id as id
					,ssts.student_id
					,SUM((asi.score_index*ssl.credits)) as total_score_index
					,SUM(ssl.credits) as total_credit
				FROM student_subject_study_summary AS ssts
				JOIN student_study_line ssl ON ssl.id=ssts.student_study_line_id
				JOIN academic_score_index AS asi ON asi.id = ssl.fix_score_index_id
				GROUP BY
					ssts.student_id
		"""
		return query

	@api.model_cr
	def init(self):
		tools.drop_view_if_exists(self.env.cr, self._table)
		self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			FROM ( %s ) AS mm
			)""" % (self._table, self._select(), self._from()))

	