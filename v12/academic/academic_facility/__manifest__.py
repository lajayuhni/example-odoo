{
	'name': 'Academic Facility',
	'summary': """
		Academic Facility
		""",
	'version': '0.0.1',
	'category': 'sisformik',
	"author": "La Jayuhni Yarsyah",
	'description': """
		Academic Facility
	""",
	'depends': [
		'academic_major',
	],
	'data': [
		'views/search.xml',
		'views/kanban.xml',
		'views/tree.xml',
		'views/form.xml',
		
		'security/group.xml',
		'security/ir.model.access.xml',
		'actions/action.xml',
		# 'data/academic.facility.csv',
		'data/academic.facility.xml',
	],
	'installable': True,
	'auto_install': False,
	'application': True    
}