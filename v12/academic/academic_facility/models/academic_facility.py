# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicFacility(models.Model):
	_name = 'academic.facility'
	_description = 'Academic Facility'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange")

	code = fields.Char(string="Kode",  required=True, track_visibility="onchange")

	capacity = fields.Integer('Kapasitas Ruangan', required=True, track_visibility="onchange")

	

	# class_room = fields.Boolean(string="Regular Class Room", default=False, track_visibility="onchange")
	# lab_room = fields.Boolean(string="Laboratory", default=False, track_visibility="onchange")
	# extra_room = fields.Boolean(string="Extra Room", default=False,  track_visibility="onchange")

	room_type = fields.Selection([('class','Ruangan Kelas'), ('lab','Laboratorium'), ('extra','Lainnya')], string="Tipe Ruangan", track_visibility="onchange", required=True)


	active = fields.Boolean('Active',default=True, track_visibility="onchange")


	def toggle_class_room(self):
		self.ensure_one()
		self.class_room = not self.class_room

	def toggle_lab_room(self):
		self.ensure_one()
		self.lab_room = not self.lab_room

	def toggle_extra_room(self):
		self.ensure_one()
		self.extra_room = not self.extra_room

	def toggle_active(self):
		self.ensure_one()
		self.active = not self.active

	@api.constrains('capacity')
	def constrains_capacity(self):

		if self.capacity < 10:
			raise ValidationError(_("Kapasitas Ruangan Tidak Valid"))
