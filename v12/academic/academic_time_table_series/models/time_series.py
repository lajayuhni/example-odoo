# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class BaseAcademicTimeSeries(models.Model):
	_name = 'base.academic.time.series'
	_description = 'Base Academic Time Series'


	# uncomment if using tracking modules
	#_inherit = ['mail.thread', 'mail.activity.mixin']

	_auto = False
	
	def _select(self):
		query = """
			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-01 00:30:00'::timestamp,
					'2018-10-01 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms

			UNION

			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-02 00:30:00'::timestamp,
					'2018-10-02 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms


			UNION

			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-03 00:30:00'::timestamp,
					'2018-10-03 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms

			UNION

			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-04 00:30:00'::timestamp,
					'2018-10-04 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms

			UNION

			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-05 00:30:00'::timestamp,
					'2018-10-05 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms

			UNION

			SELECT
				mms.minute_series::date as s_date,
				mms.minute_series
			FROM (
				SELECT 
					date_trunc('minute', ms) as minute_series
				FROM generate_series
				(
					'2018-10-06 00:30:00'::timestamp,
					'2018-10-06 20:00:00'::timestamp,
					'5 minute'::interval
				) ms
			) mms
			order by minute_series asc
		"""
		return query
	
	def _from(self):
		query = """
			
		"""
		return query
	
	
	@api.model_cr
	def init(self):
		tools.drop_view_if_exists(self.env.cr, self._table)
		self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			)""" % (self._table, self._select()))

	