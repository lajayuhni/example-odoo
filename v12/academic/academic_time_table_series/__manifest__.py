{
    'name': 'Academic Time Table Series',
    'summary': """
        Academic Time Table Series
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        This module only generate series from pgsql
        For using in other modul
        to joining in other modules
    """,
    'depends': [
        'base','web'
    ],
    'data': [
    	'security/security.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}