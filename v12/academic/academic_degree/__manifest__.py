{
    'name': 'Academic Degree',
    'summary': """
        Academic Degree
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Manage Academic Degree
    """,
    'depends': [
        'base','mail','academic_user'
    ],
    'data': [
        'views/search.xml',
        'views/kanban.xml',
    	'views/tree.xml',
    	'views/form.xml',

    	
    	'security/group.xml',
        'security/ir.model.access.xml',
        'actions/action.xml',


        'data/academic.degree.csv',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}