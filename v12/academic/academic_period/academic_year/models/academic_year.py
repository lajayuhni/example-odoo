# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import AccessError,UserError,ValidationError
import re

import logging
_logger = logging.getLogger(__name__)

def replace_non_alphanum(str_value,replacement='_'):
	return re.sub(r'\W+',replacement, str_value)


class AcademicYear(models.Model):
	_name = 'academic.year'
	_description = 'Academic Year'

	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange", unique=True)
	code = fields.Char(string="Kode/Tahun",  required=True, track_visibility="onchange", unique=True)
	currency_id = fields.Many2one('res.currency', string='Currency', required=True, default=lambda self: self.env.user.company_id.currency_id, ondelete="RESTRICT", onupdate="RESTRICT")
	education_fee = fields.Monetary(string='Education Fee', required=True, track_visibility="onchange")
	admission_fee = fields.Monetary(string='Biaya Pendaftaran', required=True, track_visibility="onchange")
	extra_admission_cost = fields.Monetary(string='Biaya Pendaftaran Extra', required=True, track_visibility="onchange", help="Biaya Extra yang di bebankan kepada pendaftar jika lulus, dan di luar biaya pendaftaran. Misal: Biaya Seragam, Dll")

	seq = fields.Integer(string="Urutan", required=True, track_visibility="onchange")
	active = fields.Boolean('Aktif',default=True)
	academic_year_period_ids = fields.One2many('academic.year.period', 'academic_year_id', string="Periode Terkait")
	academic_year_period_ids_count = fields.Integer('Period Counter', compute="_compute_academic_year_period_ids_count")
	locked = fields.Boolean(string="Terkunci", default=False, track_visibility="onchange")



	def _compute_academic_year_period_ids_count(self):
		for rec in self:
			rec.academic_year_period_ids_count = len(rec.academic_year_period_ids)


	# @api.constrains('locked')
	# def constrains_locked(self):
	# 	for rec in self:
	# 		_logger.critical((rec.id, rec.old))
	# 		if rec.locked==False and not self.user_has_groups('academic_period.group_academic_year_manager'):
	# 			raise AccessError(_("Kamu tidak berhak membuka kunci!"))

	def btn_lock(self):
		self.write({'locked':True})

	def btn_unlock(self):
		# unlock can only do by manager
		if not self.user_has_groups('academic_period.group_academic_year_manager'):
			raise AccessError(_("Kamu tidak berhak membuka kunci!"))
		self.write({'locked':False})


	def toggle_locked(self):
		self.ensure_one()
		self.locked = not self.locked

	def open_academic_year_period_ids(self):
		self.ensure_one()
		action = self.env['ir.actions.act_window'].for_xml_id('academic_period', 'action_academic_year_period')
		action.update({
			'domain':[('id','in',self.academic_year_period_ids.ids)],
			'context':{'default_academic_year_id':self.id},
		})
		return action


	def _prepare_year_period_ids(self, academic_year_period_category):
		return {
			'academic_period_category_id':academic_year_period_category.id,
			'seq':academic_year_period_category.seq,
		}

	def _get_default_academic_year_period_ids(self):
		period_cats = self.env['academic.period.category'].search([('active','=',True)])
		res = []
		for cat in period_cats:
			res.append((0,0,self._prepare_year_period_ids(cat)))

		return res

	@api.model
	def create(self, values):
		values.update({
			'academic_year_period_ids':self._get_default_academic_year_period_ids()
			})
		sup = super(AcademicYear, self).create(values)

		module = 'academic_period'
		force_module = self._context.get('force_module',False)


		if force_module:
			module = force_module
		_logger.warning([module, force_module])
		if sup:
			irmodeldata = self.env['ir.model.data']
			for period_cat in sup.academic_year_period_ids:
				# academic_year = TA.19
				# academic period category = Ganjil
				# will create external id like --> academic_year_period_ta19_ganjil
				irmodeldata_name = 'academic_year_period_%s_%s' % (replace_non_alphanum(period_cat.academic_year_id.code).lower(), replace_non_alphanum(period_cat.academic_period_category_id.code).lower())
				# _logger.info(('Creating ir.model.data %s' % irmodeldata_name))
				irmodeldata.create({
					'module':module,
					'name':irmodeldata_name,
					'model':'academic.year.period',
					'res_id':period_cat.id,
					'noupdate':True,
					})
		return sup