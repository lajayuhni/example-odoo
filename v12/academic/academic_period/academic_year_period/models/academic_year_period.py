# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicYearPeriod(models.Model):
	_name = 'academic.year.period'
	_description = 'Academic Year Period'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']


	academic_year_id = fields.Many2one('academic.year', string="Tahun Akademik", required=True, track_visibility="onchange", ondelete="cascade", onupdate="cascade")
	academic_period_category_id = fields.Many2one('academic.period.category', string="Kategori Periode Akademik",required=True, track_visibility="onchange")

	seq = fields.Integer(string=_("Urutan"), required=True, track_visibility="onchange", help=_("Urutan"))

	# seq = fields.Integer(string="Urutan", required=True, track_visibility="onchange")
	active = fields.Boolean(_("Aktif"), default=True, track_visibility="onchange")

	# _sql_constraints = [
	# 	('year_and_period_unique', 'unique (academic_year_id,academic_period_category_id)', 'Tahun dan Periode sudah terdaftar'),
	# ]

	@api.constrains('academic_year_id','academic_period_category_id')
	def constrains_year_period(self):
		for rec in self:
			# find
			f = self.search([('id','!=',rec.id), ('academic_year_id','=',rec.academic_year_id.id), ('academic_period_category_id','=',rec.academic_period_category_id.id), ('active','=',rec.active)])
			if len(f):
				raise ValidationError(_("Tahun %s dan %s sudah terdaftar (%s)" % (rec.academic_year_id.name, rec.academic_period_category_id.name, "Aktif" if rec.active else 'Tidak Aktif')))


	def name_get(self):
		res = []
		for rec in self:
			name = "[%s] %s" % (rec.academic_year_id.name, rec.academic_period_category_id.code, )
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('academic_year_id.name', operator, name), ('academic_period_category_id.code',operator,name)] + args, limit=limit)
		return recs.name_get()

	def toggle_active(self):
		self.ensure_one()
		self.active = not self.active

	def open_academic_year_period_period_ids(self):
		action = self.env['ir.actions.act_window'].for_xml_id('academic_period', 'action_academic_year_period_period')
		action.update({'domain':[('id','in',self.year_period_ids.ids)]})
		return action