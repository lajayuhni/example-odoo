# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicPeriodCategory(models.Model):
	_name = 'academic.period.category'
	_description = 'Academic Period Category'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange")
	code = fields.Char(string="Kode",  required=True, track_visibility="onchange", unique=True)
	seq = fields.Integer(string="Urutan", required=True, track_visibility="onchange")
	
	active = fields.Boolean('Aktif',default=True)


	# academic_year_period_ids = fields.One2many('academic.year.period', 'academic_period_category_id', string="Periode Terkait")

	def toggle_active(self):
		self.ensure_one()
		self.active = not self.active


	def open_academic_year_period_ids(self):
		action = self.env['ir.actions.act_window'].for_xml_id('academic_period', 'action_academic_year_period')
		action.update({'domain':[('id','in',self.year_period_ids.ids)]})
		return action

	def all_active(self):
		return self.search([('active','=',True)])