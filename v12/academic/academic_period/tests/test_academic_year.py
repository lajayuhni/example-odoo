# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError
from odoo.tests import tagged
from odoo.tests import common

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestAcademicYearPeriod(common.TransactionCase):

	def setUp(self):
		super(TestAcademicYearPeriod, self).setUp()

		group_staff = self.env.ref('academic_period.group_academic_year_staff')

		self.staff = self.env['res.users'].create({
			'name': 'Andrew Staff',
			'login': 'staff',
			'email': 'a.m@example.com',
			'signature': '--\nAndreww',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_staff.id, self.env.ref('base.group_user').id])]
		})
	
	def test_create(self):
		new_val = {
			'code':'TA19',
			'name':"T.A 2019",
			'education_fee':3000000,
			'extra_admission_cost':300000,
			'admission_fee':150000,
			'seq':1,
		}
		# _logger.info('Test Creating academic.year')
		newobj = self.env['academic.year'].sudo(self.staff).create(new_val)
		self.assertEqual(len(newobj)==1,True)

		period_category = self.env['academic.period.category'].all_active()

		# _logger.info('assertEqual academic.year.period should automatic created')
		self.assertEqual(len(newobj.academic_year_period_ids)==len(period_category),True)

		# lock
		newobj.sudo(self.staff).btn_lock()
		self.assertTrue(newobj.locked)

		
		with self.assertRaises(AccessError):
			newobj.sudo(self.staff).btn_unlock()
