{
	'name': 'Academic Period',
	'summary': """
		Academic Subject
		""",
	'version': '0.0.1',
	'category': 'sisformik',
	"author": "La Jayuhni Yarsyah",
	'description': """
		Academic Period
	""",
	'depends': [
		'academic_major',
	],
	'data': [
		'academic_period_category/views/search.xml',
		'academic_period_category/views/kanban.xml',
		'academic_period_category/views/tree.xml',
		'academic_period_category/views/form.xml',
		'academic_period_category/security/group.xml',
		'academic_period_category/security/ir.model.access.xml',
		'academic_period_category/actions/action.xml',
		'academic_period_category/data/academic.period.category.csv',

		'academic_year/views/search.xml',
		'academic_year/views/kanban.xml',
		'academic_year/views/tree.xml',
		'academic_year/views/form.xml',
		'academic_year/security/group.xml',
		'academic_year/security/ir.model.access.xml',
		'academic_year/actions/action.xml',

		'academic_year_period/views/search.xml',
		'academic_year_period/views/kanban.xml',
		'academic_year_period/views/tree.xml',
		'academic_year_period/views/form.xml',
		'academic_year_period/security/group.xml',
		'academic_year_period/security/ir.model.access.xml',
		'academic_year_period/actions/action.xml',
	],
	'installable': True,
	'auto_install': False,
	'application': True    
}