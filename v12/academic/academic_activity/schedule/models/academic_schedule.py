# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicSchedule(models.Model):
	_name = 'academic.schedule'
	_description = 'Academic Schedule'

	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']


	seq = fields.Integer(string="Urutan", required=True, track_visibility="onchange")
	code = fields.Char(string="Kode",  required=True, track_visibility="onchange", unique=True)
	start_on = fields.Date(string="Mulai")
	end_on = fields.Date(string="Berakhir")

	academic_year_period_id = fields.Many2one('academic.year.period', string="Periode Akademik", required=True, track_visibility="onchange", ondelete="cascade", onupdate="cascade")
	academic_activity_id = fields.Many2one('academic.activity', string="Aktifitas", required=True, track_visibility="onchange")

	active = fields.Boolean('Aktif',default=True, track_visibility="onchange")
	state = fields.Selection([('draft','Draft'), ('posted','Posted'),('canceled','Canceled')], string="State", default="draft", required=True, track_visibility="onchange")
	
	_rec_name = 'code'

	@api.constrains('start_on','end_on')
	def constrains_schedule_validate(self):
		for rec in self:
			if rec.end_on < rec.start_on:
				raise ValidationError(_("Tanggal Berakhir tidak diperbolehkan sebelum tanggal mulai!"))


	@api.constrains('academic_year_period_id', 'academic_activity_id', 'state')
	def constrains_unique_activity_schedule(self):
		# find other record matched with self.academic_activity_id and self.academic_year_period_id and state not in ('canceled')
		# to make sure no schedule duplicated
		# _logger.warning(('constrains', self, self.mapped(lambda r:[r.display_name, r.state])))
		for rec in self:
			domain = [
				('academic_activity_id', '=', rec.academic_activity_id.id),
				('academic_year_period_id', '=', rec.academic_year_period_id.id),
				('state','not in', ['canceled']),
				('id','!=', rec.id)
			]

			duplicated = self.search(domain)
			# _logger.info(('Domain--->', domain, duplicated))
			if len(duplicated)>0:
				raise ValidationError(_('Sudah ada Jadwal aktif untuk aktifitas "%s" pada periode "%s".\nKamu dapat mengubah status menjadi "Cancel" jika ingin membuat baru!' % (rec.academic_activity_id.name, rec.academic_year_period_id.display_name)))


	# To "Draft" only from "Canceled"
	# To "Posted" only from "Draft"
	# To "Cancel" only from "Draft"
	def _validate_state_flow(self, new_state):
		
		for rec in self:
			if new_state=='canceled' and rec.state!='posted':
				raise ValidationError(_("Hanya dapat mengcancel data yg berstatus \"Posted\"!"))

			if new_state=='posted' and rec.state!='draft':
				raise ValidationError(_("Hanya dapat memposting data yang berstatus \"Draft\". Ref:(%s, %s)") % (rec.display_name, rec.state))

			# set to draft only from canceled
			if new_state=='draft' and rec.state not in ['canceled']:
				raise ValidationError(_("Hanya data berstatus \"Posted\" / \"Cancel\" dapat menjadi Draft"))
	
	@api.multi
	def write(self, vals):
		new_state = vals.get('state',False)
		
		if new_state:
			self._validate_state_flow(new_state)
		return super(AcademicSchedule, self).write(vals)

	def toggle_active(self):
		self.ensure_one()
		self.active = not self.active	


	@api.onchange('academic_activity_id','academic_year_period_id')
	def onchange_academic_activity_id(self):
		
		default_academic_activity = self.env['academic.activity'].browse(self._context.get('default_academic_activity_id',0))
		default_academic_year_period = self.env['academic.year.period'].browse(self._context.get('default_academic_year_period_id',0))

		ac_year_period = default_academic_year_period if len(default_academic_year_period)==1 else self.academic_year_period_id
		ac_activity = default_academic_activity if len(default_academic_activity)==1 else self.academic_activity_id

		if (self.academic_year_period_id.id or default_academic_year_period.id) and (self.academic_activity_id.id or default_academic_activity.id):
			self.code = "%s/%s/%s" % (ac_year_period.academic_year_id.code, ac_year_period.academic_period_category_id.code, ac_activity.code,)

		if self.academic_activity_id.id or default_academic_activity.id:
			self.seq = ac_activity.seq


	@api.constrains('state', 'start_on', 'end_on')
	def _constrains_state_start_on_end_on(self):
		for rec in self:
			if rec.state=='posted':
				# if posted 
				# must fill start on and end on
				if rec.start_on == False:
					raise ValidationError(_('Kolom Waktu Mulai pada Jadwal Akdemik %s belum didefinisikan!') % (rec.display_name,))

				if rec.end_on == False:
					raise ValidationError(_('Kolom Waktu Berakhir pada Jadwal Akdemik %s belum didefinisikan!') % (rec.display_name,))


	# BTN
	"""set state as "posted"
	"""
	def btn_post(self):
		self.write({'state':'posted'})


	"""set state to "canceled"
	"""
	def btn_cancel(self):
		self.write({'state':'canceled'})

	"""Set state to "draft"
	"""
	def btn_draft(self):
		self.write({'state':'draft'})