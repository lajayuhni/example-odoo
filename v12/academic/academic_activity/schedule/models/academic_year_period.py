# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError,AccessError

import logging
_logger = logging.getLogger(__name__)

from odoo.addons.academic_period.academic_year.models.academic_year import replace_non_alphanum


class AcademicYearPeriod(models.Model):
	_inherit = 'academic.year.period'

	academic_schedule_ids = fields.One2many('academic.schedule', 'academic_year_period_id', string="Kalender Akademik")
	academic_schedule_ids_count = fields.Integer('Schedule Count', compute="_compute_academic_schedule_ids_count")

	state = fields.Selection([('draft','Draft'), ('posted','Posted'), ('canceled','Canceled')], string="State", default="draft", track_visibility="onchange", required=True)

	""" Validation for change state to post
	1. academic_schedule_ids must be not draft
	2. filtered len(academic_schedule_ids) where state == 'posted' exact with academic.activity.filtered(active=True)

	"""
	def posting_validation(self):
		# activities (academic.activity)
		activities = self.env['academic.activity'].search([('active','=',True)])
		if not self.user_has_groups('academic_period.group_academic_year_period_staff'):
			raise AccessError(_("Kamu tidak berhak memposting data!"))

		for rec in self:
			f_activities = activities.filtered(lambda r:rec.academic_period_category_id.id not in r.academic_period_category_exclude_ids.mapped('id'))			
			posted_schedules = rec.academic_schedule_ids.filtered(lambda r:r.state in ['posted'])
			if len(posted_schedules) != len(f_activities):
				not_created_activities = f_activities.filtered(lambda r:r not in posted_schedules.mapped('academic_activity_id'))
				
				msg_not_created_activities = "\n".join(not_created_activities.mapped(lambda r:"-> %s" % (r.display_name,)))
				raise ValidationError(_("Ada aktifitas (aktif) yang belum didefinisikan pada Kalendar Akademik %s:\n%s!" % (rec.display_name, msg_not_created_activities, )))

	def _prepare_year_period_ids(self, academic_year_period_category):
		return {
			'academic_period_category_id':academic_year_period_category.id,
			'seq':academic_year_period_category.seq,
			'academic_schedule_ids':self._prepare_schedules(),
		}

	def _prepare_schedules(self):
		activities = rec.get_required_academic_activities()
		seq = 0
		res = []
		for activity in activities:
			seq += 1
			new_data = self._prepare_schedule(activity, seq)
			res.append((0,0,new_data))

		return res


	def _prepare_schedule(self, activity, seq=1):
		self.ensure_one()
		data = dict(
			seq=seq,
			start_on=False,
			end_on=False,
			academic_year_period_id=self.id,
			academic_activity_id=activity.id,
			active=True,
			state='draft',
		)
		obj = self.env['academic.schedule'].new(data)
		obj.onchange_academic_activity_id()
		new_data = obj._convert_to_write({name: obj[name] for name in obj._cache})
		return new_data


	@api.returns('academic.schedule')
	def _create_activity(self, activity, seq=1):
		self.ensure_one()
		new_data = self._prepare_schedule(activity, seq)
		return self.env['academic.schedule'].create(new_data)


	

	def _create_draft_schedules(self):
		schs = self.env['academic.schedule']
		for rec in self:
			activities = rec.get_required_academic_activities()
			_logger.critical(('create activities->', rec.display_name, activities.mapped(lambda r:r.name)))
			seq = 0
			for activity in activities:
				seq += 1
				new_schedule = self._create_activity(activity, seq)
				if len(new_schedule):
					schs|=new_schedule
		return schs


	@api.model
	def create(self, vals):
		sup = super(AcademicYearPeriod, self).create(vals)
		module = 'academic_activity'
		force_module = self._context.get('force_module',False)
		if force_module:
			module = self._context.get('force_module')

		if sup:
			irmodeldata = self.env['ir.model.data']
			sup._create_draft_schedules()
			for schedule in sup.academic_schedule_ids:

				irmodeldata_name = 'academic_schedule_%s_%s' % (replace_non_alphanum(schedule.academic_year_period_id.display_name).lower(), replace_non_alphanum(schedule.academic_activity_id.code).lower())
				_logger.info(('Creating ir.model.data for academic.schedule (%s) = %s' % (schedule.code, irmodeldata_name)))
				_logger.warning(('______________________', module, irmodeldata_name))
				irmodeldata.create({
					'module':module,
					'name':irmodeldata_name,
					'model':'academic.schedule',
					'res_id':schedule.id,
					'noupdate':True,
					})
		return sup

	def btn_post(self):
		_logger.critical(['IIII', self.academic_schedule_ids.mapped(lambda r:[r.display_name, r.state])])
		self.academic_schedule_ids.btn_post()
		_logger.critical(['PPP', self.academic_schedule_ids.mapped(lambda r:[r.display_name, r.state])])
		self.posting_validation()
		self.write({'state':'posted'})

	""" Validation change state to "Canceled" rules
	"""
	def canceling_validation(self):
		return True

	def btn_cancel(self):
		self.canceling_validation()
		self.write({'state':'canceled'})
		self.academic_schedule_ids.btn_cancel()

	""" Set record as Draft
	ONly manager can set as draft
	"""
	def btn_draft(self):
		# self.posting_validation()
		if not self.user_has_groups('academic_activity.group_academic_schedule_manager'):
			msg = _("Kamu tidak berhak untuk melakukan aksi tersebut!")
			msg_log = "%s by %s(%s)" % (msg, self.env.user.id, self.env.user.login)
			_logger.error(msg_log)
			raise ValidationError(msg)

		self.write({'state':'draft'})


	@api.multi
	def _compute_academic_schedule_ids_count(self):
		for rec in self:
			rec.academic_schedule_ids_count = len(rec.academic_schedule_ids)


	def open_academic_schedule_ids(self):
		self.ensure_one()
		action = self.env['ir.actions.act_window'].for_xml_id('academic_activity', 'action_academic_schedule')
		
		ctx = {}
		ctx.update({'default_academic_year_period_id':self.id})
		action.update({'domain':[('id','in',self.academic_schedule_ids.ids)], 'context':ctx})
		return action


	"""Validate State Flow
	
	Draft->Posted->Canceled->Draft
	"""
	def _validate_state_flow(self, new_state):
		if new_state=='posted' and self.state!='draft':
			raise ValidationError(_("Hanya dapat memposting data yang berstatus \"Draft\""))
		if new_state=='canceled' and self.state not in ('draft','posted'):
			raise ValidationError(_("Hanya dapat mengcancel data yg berstatus \"Draft\"!"))
	
	def write(self, vals):
		new_state = vals.get('state',False)
		if new_state:
			self._validate_state_flow(new_state)
		return super(AcademicYearPeriod, self).write(vals)
	
	@api.returns('academic.activity')
	def get_required_academic_activities(self):
		self.ensure_one()
		academic_activities = self.env['academic.activity'].search([('active','=',True)])
		# find activity who not has exclude this academic_period_category_id
		return academic_activities.filtered(lambda r:self.academic_period_category_id.id not in r.academic_period_category_exclude_ids.mapped('id'))