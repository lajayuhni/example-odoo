# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicActivity(models.Model):
	_name = 'academic.activity'
	_description = 'Academic Activity'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	seq = fields.Integer(string="Urutan", required=True, track_visibility="onchange")
	code = fields.Char(string="Code", required=True, track_visibility="onchange")
	name = fields.Char(string="Nama", required=True, track_visibility="onchange")
	description = fields.Html('Deskripsi')
	
	# academic_year_period_exclude_ids = fields.Many2many('academic.year.period', 'academic_activity_academic_year_period_rel', 'academic_activity_id', 'academic_year_period_id', string="Tidak Berlaku Pada Periode")
	academic_period_category_exclude_ids = fields.Many2many('academic.period.category', 'academic_activity_acaedemic_period_category_rel', 'academic_activity_id', 'acaedemic_period_category_id', string="Tidak Berlaku Pada Kategori")
	active = fields.Boolean('Aktif',default=True)

	schedule_ids = fields.One2many('academic.schedule', 'academic_activity_id', string="Jadwal Terkait")

	def toggle_active(self):
		self.ensure_one()
		self.active = not self.active

	def open_academic_schedule_ids(self):
		"""
		open_academic_schedule_ids open schedules in new window

		:return: schedule ids
		:rtype: dict
		"""
		action = self.env['ir.actions.act_window'].for_xml_id('academic_activity', 'action_academic_schedule')
		action.update({
			'domain':[('academic_activity_id', '=', self.id)],
			'view_mode':'kanban,calendar,tree,form'
			})
		return action