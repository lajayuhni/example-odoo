# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tests import tagged
from odoo.tests import common
from odoo import fields

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestAcademicYearPeriod(common.TransactionCase):

	def setUp(self):
		super(TestAcademicYearPeriod, self).setUp()
		# Prepare group
		group_staff = self.env.ref('academic_period.group_academic_year_period_staff')
		group_manager = self.env.ref('academic_period.group_academic_year_period_manager')


		
		self.user_staff = self.env['res.users'].create({
			'name': 'Unit Test staff',
			'login': 'user_staff',
			'email': 'untitest@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_staff.id, self.env.ref('base.group_user').id])]
		})

		self.user_manager = self.env['res.users'].create({
			'name': 'Main Unit Test Manager',
			'login': 'academic_year_period_manager',
			'email': 'academic_year_period_manager@example.com',
			'signature': '--\nacademic_year_period_manager',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_manager.id, self.env.ref('base.group_user').id])]
		})

		self.other_user = self.env['res.users'].create({
			'name': 'Other User',
			'login': 'other_user',
			'email': 'other@example.com',
			'signature': '--\nOther User',
			'notification_type': 'email',
			'groups_id': [(6, 0, [self.env.ref('base.group_user').id])]
		})

		self.main_env = self.env['academic.year.period']


		# from current default data,, --> defined in "academic_activity/activity/data/academic.activity.csv"
		active_activities = self.env['academic.activity'].search([('active','=',True)])
		self.academic_year = self.env['academic.year'].create({
			'seq':1,
			'name':"Test Year",
			'code':"TA/TEST",
			'education_fee':10000,
			'extra_admission_cost':10000,
			'admission_fee':150000,
			'locked':True,
			})

		self.academic_period_category = self.env['academic.period.category'].create({
			'name':"Test Period",
			'code':"11",
			'seq':1,
			'active':True,
			})

		# self.academic_year_period = self.env['academic.year.period'].create({
		# 	'academic_year_id':self.academic_year.id,
		# 	'academic_period_category_id':self.academic_period_category.id,
		# 	'seq':1,
		# 	'active':True,
		# 	})

	def test_flow(self):

		# creating academic.year.period
		data = self.main_env.sudo(self.user_staff).create({
			'academic_year_id':self.academic_year.id,
			'academic_period_category_id':self.academic_period_category.id,
			'seq':1,
			'active':True,
			})


		with self.assertRaises(AccessError):
			# try post with Other User
			# will raise an error
			data.sudo(self.other_user).btn_post()
