# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tests import tagged
from odoo.tests import common

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestAcademicSchedule(common.TransactionCase):
	def setUp(self):
		super(TestAcademicSchedule, self).setUp()
		# Prepare group
		group_staff = self.env.ref('academic_activity.group_academic_schedule_staff')


		
		self.user_staff = self.env['res.users'].create({
			'name': 'Unit Test staff',
			'login': 'user_staff',
			'email': 'untitest@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_staff.id, self.env.ref('base.group_user').id])]
		})

		self.ac_year_period_staff = self.env['res.users'].create({
			'name': 'Ac Year Period Staff',
			'login': 'ac_year_period_staff',
			'email': 'ac_year_period_staff@example.com',
			'signature': '--\nAc Year Period Staff',
			'notification_type': 'email',
			'groups_id': [(6, 0, [self.env.ref('academic_period.group_academic_year_period_staff').id, self.env.ref('base.group_user').id])]
		})

		self.other_user = self.env['res.users'].create({
			'name': 'Other User',
			'login': 'other_user',
			'email': 'other@example.com',
			'signature': '--\nOther User',
			'notification_type': 'email',
			'groups_id': [(6, 0, [self.env.ref('base.group_user').id])]
		})


		self.main_env = self.env['academic.schedule']


		self.academic_activity = self.env['academic.activity'].create({
			'seq':1,
			'code':'X123',
			'name':"Test Activity",
			'description':"<b>This is description</b>",
			'active':True
			})

		self.academic_year = self.env['academic.year'].create({
			'seq':1,
			'name':"Test Year",
			'code':"TA/TEST",
			'education_fee':10000,
			'extra_admission_cost':10000,
			'admission_fee':150000,
			'locked':True,
			})

		self.academic_period_category = self.env['academic.period.category'].create({
			'name':"Test Period",
			'code':"11",
			'seq':1,
			'active':True,
			})

		self.academic_year_period = self.env['academic.year.period'].create({
			'academic_year_id':self.academic_year.id,
			'academic_period_category_id':self.academic_period_category.id,
			'seq':1,
			'active':True,
			})

	def test_flow(self):
		# Bellow, is example of test create with user group that setted up in setUp()
		new_val = {
			'academic_activity_id':self.academic_activity.id,
			'academic_year_period_id':self.academic_year_period.id,
			'active':True,
			'start_on':'2018-01-01',
			'end_on':'2018-02-01',
		}

		temp_obj = self.main_env.sudo(self.user_staff).new(new_val)
		temp_obj.onchange_academic_activity_id()

		new_obj_val = temp_obj._convert_to_write({name: temp_obj[name] for name in temp_obj._cache})
		_logger.info("Testing Creating with val %s" % new_obj_val)
		newobj = self.main_env.sudo(self.user_staff).create(new_obj_val)

		# testing is object created
		self.assertEqual(newobj.id!=False, True)

		# test posting
		with self.assertRaises(AccessError):
			newobj.sudo(self.other_user.id).btn_post() #WILL raise AccessError


		# posting from academic year post btn_post()
		# should be only can access by group_academic_year_period_staff
		# self.academic_year_period.sudo(self.ac_year_period_staff.id).btn_post()
		newobj.sudo(self.user_staff.id).btn_post()
		self.assertEqual(newobj.state,'posted')

		# ASSERT IF newobj.academic_schedule_ids.state all was 'POSTED'
		# self.assertEqual(all(self.academic_year_period.academic_schedule_ids.mapped(lambda r:r.state=='posted')),True)

		# test post->cancel
		# will raise an error
		with self.assertRaises(ValidationError):
			newobj.sudo(self.user_staff).write({'state':'draft'})
			# newobj.sudo(self.user_staff).btn_post()