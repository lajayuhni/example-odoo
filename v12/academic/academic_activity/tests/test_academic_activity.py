# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import AccessError, UserError
from odoo.tests import tagged
from odoo.tests import common

import logging
_logger = logging.getLogger(__name__)


@tagged('post_install', 'at_install')
class TestAcademicActivity(common.TransactionCase):
	def setUp(self):
		super(TestAcademicActivity, self).setUp()
		# Prepare group
		group_staff = self.env.ref('academic_activity.group_academic_activity_staff')

		self.user_staff = self.env['res.users'].create({
			'name': 'Unit Test staff',
			'login': 'user_staff',
			'email': 'untitest@example.com',
			'signature': '--\nUnitTestingUser',
			'notification_type': 'email',
			'groups_id': [(6, 0, [group_staff.id, self.env.ref('base.group_user').id])]
		})

	def test_creating_object(self):
		# Bellow, is example of test create with user group that setted up in setUp()

		new_val = {
			'seq':1,
			'code':'XXXX',
			'name':"Test Activity",
			'description':"<b>This is description</b>",
			'active':True
		}
		newobj = self.env['academic.activity'].sudo(self.user_staff).create(new_val)

		# testing is object created
		self.assertEqual(newobj.id!=False,True)
