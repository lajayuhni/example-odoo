{
	'name': 'Academic Activity',
	'summary': """
		Academic Activity
		""",
	'version': '0.0.1',
	'category': 'sisformik',
	"author": "La Jayuhni Yarsyah",
	'description': """
		Academic Activity, contains:
		- academic.activity
		- academic.scheudule
	""",
	'depends': [
		'web_timeline','academic_period',
	],
	'data': [
		'activity/views/search.xml',
		'activity/views/kanban.xml',
		'activity/views/tree.xml',
		'activity/views/form.xml',
		'activity/security/group.xml',
		'activity/security/ir.model.access.xml',
		'activity/actions/action.xml',
		'activity/data/academic.activity.csv',

		'schedule/views/search.xml',
		'schedule/views/kanban.xml',
		'schedule/views/tree.xml',
		'schedule/views/form.xml',
		'schedule/views/calendar.xml',
		'schedule/security/group.xml',
		'schedule/security/ir.model.access.xml',
		'schedule/actions/action.xml',
		'schedule/views/academic.year.period.xml',

	],
	'installable': True,
	'auto_install': False,
	'application': True    
}