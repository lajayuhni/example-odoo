{
    'name': 'Major Class Master',
    'summary': """
        Major Class Master
        """,
    'version': '0.0.1',
    'category': 'academic,sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Adding field class refereced for major
    """,
    'depends': [
        'academic','student'
    ],
    'data': [
    	'security/major.class.xml',
    	'action_menus/server.action.xml',

    	'views/major.class.xml',

    	'action_menus/actions.xml',
    	'action_menus/menus.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}