# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)



class MajorClass(models.Model):
	_name = 'academic.major.class'
	_description = "Academic Major Class"

	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange")
	code = fields.Char(string="Kode", required=True, track_visibility="onchange", unique=True)
	major_id = fields.Many2one('academic.major', string="Program Studi", required=True, track_visibility="onchange")
	semester = fields.Integer(string="Semester", required=True)

	active = fields.Boolean('Active', default=True)

	parent_id = fields.Many2one('academic.major.class', string="Parent", help="Digunakan untuk jadwal matakuliah praktikum")
	child_ids = fields.One2many('academic.major.class', 'parent_id', string="Kelas Turunan")


	def fetch_parent_by_code(self):
		for rec in self:
			parent_code = rec.code[0:-1]
			find = self.search([('code','=',parent_code)])
			if len(find)==1:
				rec.parent_id = find.id

	@api.model
	def create(self, values):
		def constrains_child(val):
			parent_id = val.get('parent_id')
			if parent_id:
				# search 
				parent = self.browse(parent_id)
				val.update({
					'name':val.get('code'),
					'major_id':parent.major_id.id,
					'semester':parent.semester,
					'active':parent.active,
					})
			return val

		if type(values)==list:
			new_values = []
			for value in values:
				data = value
				data.update(constrains_child(value))
				new_values.append(data)
		else:
			new_values = constrains_child(values)
		return super(MajorClass, self).create(values)

	def fetch_semester_from_code_nomenclature(self):
		for rec in self:
			splitted = rec.code.split('-')
			semester = splitted[1][0]
			rec.semester = semester


	@api.constrains('semester')
	def constrains_semester(self):
		for rec in self:
			if rec.semester > 0 and rec.semester <=9:
				pass
			else:
				raise ValidationError(_("Input Semester untuk %s tidak valid") % (rec.code or rec.name,))