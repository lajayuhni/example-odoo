{
    'name': 'Student IPK',
    'summary': """
        Student IPK
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Student IPK
    """,
    'depends': [
        'student','krs_approval',
    ],
    'data': [
        'views/student.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}