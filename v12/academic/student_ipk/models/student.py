# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudent(models.Model):
	_inherit = 'student.student'


	student_ipk = fields.Float(compute='_compute_ipk', string='IPK')

	def _compute_ipk(self):
		for rec in self:
			total_credits = score_weight = total_score_weight = 0
			datas = rec.student_score_cumulative_achievement_ids
			if len(datas)>0:
				for score in datas.sorted(lambda r:r.subject_id.code):
					total_credits = total_credits + score.credits
					score_weight = score_weight + score.score_index
					total_score_weight = total_score_weight + (score.credits * score.score_index)
			
			rec.student_ipk = 0

			try:
				rec.student_ipk = round((total_score_weight / total_credits), 2)
			except Exception as e:
				_logger.critical(e)
