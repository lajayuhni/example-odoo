# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudy(models.Model):
	_inherit = 'student.study'


	student_ipk = fields.Float(related='student_id.student_ipk', string='IPK')

