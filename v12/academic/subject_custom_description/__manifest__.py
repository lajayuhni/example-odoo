{
    'name': 'Subject Description',
    'summary': """
        Subject Description
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Subject Description
    """,
    'depends': [
        'academic_subject','app_live','live_poltekapp',
    ],
    'data': [
        'views/academic_subject.xml',
        'views/student_study_line.xml',
        'templates/student_study_result.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}