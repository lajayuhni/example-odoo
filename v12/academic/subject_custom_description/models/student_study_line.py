# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudyLine(models.Model):
	_inherit = 'student.study.line'


	is_custom_description = fields.Boolean(related='subject_id.is_custom_description')
	subject_description = fields.Text(string='Judul')