# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicSubject(models.Model):
	_inherit = 'academic.subject'


	is_custom_description = fields.Boolean(default=False)
