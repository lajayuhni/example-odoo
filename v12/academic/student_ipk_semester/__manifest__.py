{
    'name': 'Student IPK Semester',
    'summary': """
        Student IPK Semester
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Student IPK Semester
    """,
    'depends': [
        'student_study','app_live','live_poltekapp'
    ],
    'data': [
        'views/student_study.xml',
        'views/student_achievement_index_semester.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}