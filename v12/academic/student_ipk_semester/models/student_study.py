# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class StudentStudy(models.Model):
	_inherit = 'student.study'


	ipk_period = fields.Float(string='IPK Semester', digits=(1,2), compute='_compute_ipk_period')


	def _compute_ipk_period(self):
		for rec in self:
			query = """ 
				WITH cte AS (
					SELECT
						x.*
					FROM 
						student_achievement_index_semester as x 
					WHERE x.student_id = %s
					order by x.id,x.major_id,x.student_id,x.academic_year_period_id
				)
				SELECT 
					(ct.total_score_index/ct.total_credit) as ipk_period,
					ct.academic_year_period_id
				FROM 
					(
						SELECT	
							academic_year_period_id,
							sum(total_credit) over (order by id asc rows between unbounded preceding and current row) as total_credit,
							sum(total_score_index) over (order by id asc rows between unbounded preceding and current row) as total_score_index
						FROM cte
					) AS ct
			""" % (rec.student_id.id)

			self.env.cr.execute(query)
			res = self.env.cr.fetchall()

			for value in res:
				ipk_period, period = value
				if rec.academic_year_period_id.id == period:
					rec.ipk_period = ipk_period
			