# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError,ValidationError
from odoo.addons import decimal_precision as dp

import logging
_logger = logging.getLogger(__name__)


class StudentAchievementIndexSemester(models.Model):
	_inherit = 'student.achievement.index.semester'


	ipk_period = fields.Float(string='IPK Semester', digits=(1,2))
	
	@api.model_cr
	def init(self):
		tools.drop_view_if_exists(self.env.cr, self._table)
		self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            WITH cte AS (
                SELECT
                    x.*
                FROM 
                    (
                        %s FROM ( %s ) AS mm
                    ) as x 
                order by x.id,x.major_id,x.student_id,x.academic_year_period_id
            )
            SELECT 
                ct.id,
				ct.academic_year_period_id,
				ct.student_id,
				ct.major_id,
				ct.student_academic_year_id,
				ct.total_score_index,
				ct.total_credit,
				(ct.total_score_index::decimal/ct.total_credit::decimal) AS index_avg,
                (ct.total_score_index_prev/ct.total_credit_prev) as ipk_period
            FROM 
                (
                    SELECT
                        y.*,
                        sum(y.total_credit) over (partition by y.student_id order by y.id asc) as total_credit_prev,
                        sum(y.total_score_index) over (partition by y.student_id order by y.id asc) as total_score_index_prev
                    FROM ( %s FROM ( %s ) AS mm ) as y
                ) AS ct
			)""" % (self._table, self._select(), self._from(), self._select(), self._from()))