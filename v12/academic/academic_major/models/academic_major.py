# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicMajor(models.Model):
	_name = 'academic.major'
	_description = 'Academic Major'


	# uncomment if using tracking modules
	_inherit = ['mail.thread', 'mail.activity.mixin']

	name = fields.Char(string="Nama", required=True, track_visibility="onchange", unique=True)
	description = fields.Html(string="Deskripsi")
	seq = fields.Integer(string="Sequence",  track_visibility="onchange")
	code = fields.Char(string="Major Code", required=True, track_visibility="onchange")
	academic_title_prefix = fields.Char(string="Academic Title Prefix", track_visibility="onchange")
	academic_title_suffix = fields.Char(string="Academic Title Suffix" , track_visibility="onchange")
	
	need_extra_admission_test = fields.Boolean(string="Butuh Ujian Masuk 2", default=False)
	extra_admission_test_name = fields.Char(string="Nama Ujian Masuk 2", default="Test Wawancara")

	active = fields.Boolean(string="Aktif", default=True, track_visibility="onchange")
	
	degree_id = fields.Many2one('academic.degree', string="Degree", required=True, track_visibility="onchange")

	def name_get(self):
		res = []
		display_format = self._context.get('display-format')

		for rec in self:
			if display_format:
				name = display_format.replace("{code}",rec.code).replace("{name}",rec.name)
			else:
				name = "[%s] %s" % (rec.code, rec.name, )
			res += [(rec.id, name)]
		return res
	
	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		connector = '|'
		recs = self.search([connector, ('code', operator, name), ('name',operator,name)] + args, limit=limit)
		return recs.name_get()
