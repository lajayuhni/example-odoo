# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError,ValidationError

import logging
_logger = logging.getLogger(__name__)


class AcademicDegree(models.Model):
	_inherit = 'academic.degree'

	major_ids = fields.One2many('academic.major', 'degree_id', string="Jurusan Terkait")


	def open_major_ids(self):
		action = self.env['ir.actions.act_window'].for_xml_id('academic_major', 'action_academic_major')
		action.update({'domain':[('degree_id','in',self.major_ids.ids)]})
		return action