{
    'name': 'Academic Major',
    'summary': """
        Academic Major
        """,
    'version': '0.0.1',
    'category': 'sisformik',
    "author": "La Jayuhni Yarsyah",
    'description': """
        Manage Major Data
    """,
    'depends': [
        'base','mail','academic_degree'
    ],
    'data': [
        'views/search.xml',
        'views/kanban.xml',
    	'views/tree.xml',
    	'views/form.xml',
    	
    	'security/group.xml',
        'security/ir.model.access.xml',
        'actions/action.xml',

        'views/academic.degree.xml',

        'data/academic.major.csv',
    ],
    'installable': True,
    'auto_install': False,
    'application': True    
}